package com.games.main;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.games.properties.ApplicationProperties;
import com.games.service.PaymentService;

@SpringBootApplication
@EnableAsync
@EnableScheduling
@ComponentScan("com.games")
public class LeagueGamesJarApplication {
	private static final Logger logs = LoggerFactory.getLogger(LeagueGamesJarApplication.class);

	@Autowired
	ApplicationProperties applicationProperties;

	@PostConstruct
	public void init() {
		// Setting Spring Boot SetTimeZone
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public static void main(String[] args) {
		SpringApplication.run(LeagueGamesJarApplication.class, args);
	}

	

}
