package com.games.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import com.games.service.MoneyTransferService;
import com.games.service.PaymentService;
import com.games.util.CommonConstants;

@Controller
public class CommonServiceStartController {

	private static final Logger logs = LoggerFactory.getLogger(CommonServiceStartController.class);

	@Autowired
	PaymentService paymentService;

	@Autowired
	MoneyTransferService moneyTransferService;

	// @Autowired
	// private TokenTournmentProperties tokenTournmentProperties;

	@Scheduled(fixedDelay = 15 * 1000)
	public void paymentsPaytmController() {
		paymentService.processPaymentsByPaytm(false, false, CommonConstants.ALL_DAY_RECORDS);

	}

	@Scheduled(fixedDelay = 10 * 60 * 1000)
	public void paymentsPaytmStatusReverificationController() {
		paymentService.processPaymentsByPaytm(true, false, CommonConstants.CURRENT_DAY_RECORDS);
	}

	@Scheduled(fixedDelay = 15 * 60 * 1000)
	public void paymentsPaytmStatusReverificationPreviousDaysController() {
		paymentService.processPaymentsByPaytm(true, false, CommonConstants.PREVIOUS_DAY_RECORDS);
	}

	@Scheduled(fixedDelay = 5 * 1000)
	public void paymentsPaytmWebHookController() {
		paymentService.processPaymentsByPaytm(false, true, CommonConstants.ALL_DAY_RECORDS);
	}

	@Scheduled(fixedDelay = 15 * 1000)
	public void paymentsCashFreeController() {
		paymentService.processPaymentsByCashFree(false, false, CommonConstants.ALL_DAY_RECORDS);
	}

	@Scheduled(fixedDelay = 10 * 60 * 1000)
	public void paymentsCashFreeStatusReverificationController() {
		paymentService.processPaymentsByCashFree(true, false, CommonConstants.ALL_DAY_RECORDS);
	}

	@Scheduled(fixedDelay = 5 * 1000)
	public void paymentsCashFreeWebHookController() {
		paymentService.processPaymentsByCashFree(false, true, CommonConstants.ALL_DAY_RECORDS);
	}

	@Scheduled(fixedDelay = 20 * 1000)
	public void paytmTransferController() {
		// status check must be first
		// transaction are saving paytm side taking time, first they are giving failed,
		// then they giving success
		moneyTransferService.transferStatuCheckByPaytmService(false);
		moneyTransferService.transferAmountThroughPaytm();
	}

	@Scheduled(fixedDelay = 15 * 60 * 1000)
	public void paytmKycPendingController() {
		moneyTransferService.transferStatuCheckByPaytmService(true);
	}

	@Scheduled(fixedDelay = 30 * 1000)
	public void upiTransferController() {
		moneyTransferService.transferAmountThroughUpi();
	}

	@Scheduled(fixedDelay = 60 * 1000)
	public void upiTransferStatusController() {
		moneyTransferService.transferStatuCheckByCoinTabService();
	}

}
