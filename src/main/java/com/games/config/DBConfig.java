package com.games.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.games.properties.DatabaseProperties;

@Configuration
public class DBConfig {

	@Autowired
	DatabaseProperties databaseProperties;
	
	
	@Bean("primeDB")
	@Primary
	public DataSource primeDB() throws Exception {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(databaseProperties.getSql_driver());
		dataSource.setUrl(databaseProperties.getPrimary_sql_db_url());
		dataSource.setUsername(databaseProperties.getPrimary_sql_db_userName());
		dataSource.setPassword(databaseProperties.getPrimary_sql_db_password());
		return dataSource;
	}
	
	@Bean("primaryDB")
	@Autowired
	public JdbcTemplate primaryDB(@Qualifier(value = "primeDB") DataSource primeDB) {
		return new JdbcTemplate(primeDB);
	}
	
	@Bean("namedPrimaryDB")
	@Autowired
	public NamedParameterJdbcTemplate namedPrimaryDB(@Qualifier(value = "primeDB") DataSource primeDB) {
		return new NamedParameterJdbcTemplate(primeDB);
	}
	
}
