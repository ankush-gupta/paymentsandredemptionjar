package com.games.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import redis.clients.jedis.JedisPoolConfig;

/**
 * @author ashish
 */
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport {

	@Value("${redis.host}")
	private String redisHost;
	@Value("${redis.port}")
	private int redisPort;
	@Value("${redis.password}")
	private String redisPassword;

	@Value("${redis.maxTotal}")
	private Integer maxTotal;

	@Value("${redis.minidle}")
	private Integer minIdle;

	@Value("${redis.maxidle}")
	private Integer maxIdle;

	@Value("${redis.maxWait.time}")
	private long maxWaitTime;

	@Value("${redis.client}")
	private String redisClient;

	@Bean
	public JedisConnectionFactory redisConnectionFactory() {

		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(maxTotal);
		poolConfig.setMinIdle(minIdle);
		poolConfig.setMaxIdle(maxIdle);
		poolConfig.setMaxWaitMillis(maxWaitTime);
		JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory(poolConfig);

		jedisConnectionFactory.setHostName(redisHost);
		jedisConnectionFactory.setPort(redisPort);
		if (redisPassword != null) {
			jedisConnectionFactory.setPassword(redisPassword);
		}
		jedisConnectionFactory.setClientName(redisClient);
		jedisConnectionFactory.setUsePool(true);

		return jedisConnectionFactory;
	}

	@Bean
	public RedisSerializer<String> redisStringSerializer() {
		RedisSerializer<String> stringRedisSerializer = new StringRedisSerializer();
		return stringRedisSerializer;
	}

	@Bean("redisTemplate")
	public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory cf, RedisSerializer redisSerializer) {
		RedisTemplate<String, String> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(cf);
		redisTemplate.setDefaultSerializer(redisSerializer);
		// redisTemplate.setEnableTransactionSupport(true);
		return redisTemplate;
	}

	/*
	 * @Bean public CacheManager cacheManager() { return new
	 * RedisCacheManager(redisTemplate(redisConnectionFactory(),
	 * redisStringSerializer())); }
	 */

}