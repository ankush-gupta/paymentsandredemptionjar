package com.games.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.games.properties.DatabaseProperties;
import com.mongodb.MongoClientURI;

@Configuration
public class MongoConfig {
	
	@Autowired
    DatabaseProperties databaseProperties;
	
	
	 public @Bean(name = "mongoTemplatePrimary")
	    MongoTemplate mongoTemplatePrimary() throws Exception {
	        return new MongoTemplate(
	                new SimpleMongoDbFactory(new MongoClientURI(databaseProperties.getPrimary_mongo_db_url())));
	    }


}
