package com.games.util;

public interface QueryConstants {
	String GET_USER_DETAILS_BY_USER_ID = "SELECT user_id as userId, mobile_no as mobile, email_id as email, android_id, google_ad_id, app_ver as ver,prvs_max_offer_amount as previousMaxOfferAmount,added_date as added_date FROM users WHERE user_id= ? limit 1";

	String GET_TOTAL_PAYMENT_AMOUNT = "SELECT paytm_deposit,paytm_redeemed FROM cash_wallet WHERE user_id=? limit 1";

	String USER_GAME_BALANCE_EARNINGS = "select  game_earned from cash_wallet where user_id=? limit 1 ";

	String USER_PRO_BALANCE_EARNINGS = "select  pro_earned from pro_users_wallet where user_id=? limit 1 ";

	String USER_SPORTS_BALANCE_EARNINGS = "select  game_earned,game_redeemed from sports_wallet where user_id=? limit 1 ";

	String CASHBACK_OFFER_BY_PLAN_AMOUNT_REWARD_MODE = "select id,plan_amount,offer_text,offer_type,payment_type,deposit_wallet_cash,reward_wallet_cash,reward_coins,is_new_user_offer from wallet_cashback_offers where is_active='Y' and plan_amount=? and (reward_mode like ? or reward_mode like ?) limit 1";

	String GET_PAYMENT_MODE_BY_KEY_WORD = "SELECT payment_mode FROM payment_modes where key_word=? limit 1";

}
