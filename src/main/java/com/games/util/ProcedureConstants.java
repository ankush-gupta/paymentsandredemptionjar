package com.games.util;

public interface ProcedureConstants {
	
	String  UPDATE_CASH_WALLET_AND_TRACK_TRANSACTIONS_SP= "update_cash_wallet_and_track_transactions_sp";
	
	
	String USER_ID="_userId";
	String REFERENCE_NO="_referenceNo";
	String GAME_AMOUNT= "_gameAmount";
	String REWARD_AMOUNT= "_rewardAmount";
	String PAYTM_AMOUNT= "_paytmAmount";
	String GAME_COINS= "_gameCoins";
	String REWARD_COINS= "_rewardCoins";
	String TRANS_TYPE= "_transType";
	String TRANS_RESULT= "_result";
	
	
	

}
