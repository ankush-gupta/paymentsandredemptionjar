package com.games.util;

public interface CommonConstants {

	int DEBIT_EVENT = 2;
	int CREDIT_EVENT = 1;

	int CASH_ENTRY_FEE_TYPE = 1;
	int TOKEN_ENTRY_FEE_TYPE = 2;

	String ALL_DAY_RECORDS = "ALL";
	String CURRENT_DAY_RECORDS = "CURRENT";
	String PREVIOUS_DAY_RECORDS = "PREVIOUS";

	String CASH_TRANSACTIONS = "cash_transactions";
	String TOKEN_TRANSACTIONS = "token_transactions";
	String NOTIFICATION_COMMON_COLLECTION = "common_notifications";
	String PRO_USER_MASTER_COLLECTION = "proUserMaster";
	String PRO_USER_HISTORY_COLLECTION = "proUserHistoryMaster";
	String SINGULAR_EVENTS_COLLECTION = "singular_events";

	String PAYMENT_COLLECTION = "paymentMaster";
	String PAYMENT_TYPE_PAYTM = "Paytm";
	String PAYMENT_TYPE_CASHFREE = "CashFree";

	String TRANSFER_COLLECTION = "transferMaster";
	String TRANSFER_TYPE_PAYTM = "Paytm";
	String TRANSFER_TYPE_UPI = "Upi";

	int TXN_INIT_STATUS = 0;
	int TXN_SUCCESS_STATUS = 1;
	int TXN_FAILURE_STATUS = 2;
	int TXN_PENDING_STATUS = 3;

	String CASH_WALLET_CREDIT = "CREDIT";
	String CASH_WALLET_DEBIT = "DEBIT";

	String EVENT_TYPE_ADD_MONEY = "ADD_MONEY";
	String EVENT_TYPE_WITHDRAW_MONEY = "WITHDRAW_MONEY";
	String ADD_MONEY_REQUESTED_TITLE = "Add money requested";
	String ADD_MONEY_SUCCESS_TITLE = "Add money request successful";
	String ADD_MONEY_FAILED_TITLE = "Add money request failed";
	String ADD_MONEY_BONUS = "Bonus for Adding Money";
	String ADD_MONEY_DEPOSIT_CASHBACK = "Cashback bonus";

	String CASH_WITH_DRAWL_TITLE = "Cash withdrawal requested";
	String PRO_CASH_WITH_DRAWL_TITLE = "VIP winning redemption requested";
	String SPORTS_CASH_WITH_DRAWL_TITLE = "Fantasy Wallet - Redemption requested";

	String CASH_WITH_DRAWL_SUCC_TITLE = "Cash withdrawal successful";
	String PRO_CASH_WITH_DRAWL_SUCC_TITLE = "VIP winning withdrawal successful";
	String SPORTS_CASH_WITH_DRAWL_SUCC_TITLE = "Fantasy Wallet - Redemption Successful";
	String CASH_WITH_DRAWL_FAIL_TITLE = "Cash withdrawal failed";
	String PRO_CASH_WITH_DRAWL_FAIL_TITLE = "VIP winning redemption failed";
	String SPORTS_CASH_WITH_DRAWL_FAIL_TITLE = "Fantasy Wallet - Redemption Failed";
	String CASH_WITH_DRAWL_SUB_TITLE_PAYTM = "Paytm";
	String CASH_WITH_DRAWL_SUB_TITLE_UPI = "UPI";
	String CASH_WITH_DRAWL_UPI_SERVICE_FEE_SUCC = "Service fee";
	String PRO_CASH_WITH_DRAWL_UPI_SERVICE_FEE_SUCC = "Service fee";
	String SPORTS_CASH_WITH_DRAWL_UPI_SERVICE_FEE_SUCC = "Service fee";
	String CASH_WITH_DRAWL_UPI_SERVICE_FEE_FAIL = "Service fee";
	String PRO_CASH_WITH_DRAWL_UPI_SERVICE_FEE_FAIL = "Service fee";
	String SPORTS_CASH_WITH_DRAWL_UPI_SERVICE_FEE_FAIL = "Service fee";
	String CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_SUCC = "Service fee";
	String PRO_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_SUCC = "Service fee";
	String SPORTS_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_SUCC = "Service fee";
	String CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_FAIL = "Service fee";
	String PRO_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_FAIL = "Service fee";
	String SPORTS_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_FAIL = "Service fee";

	String CASH_WITH_DRAWL_SERVICE_FEE = "Service fee";

	int TRANSACTION_HIT_COUNT = 10;
	String PRO_WALLET = "PRO";
	String GAME_WALLET = "GAME";
	String SPORTS_WALLET = "SPORTS";
	String HOT_DEAL_NAME = "HOT_DEAL";
	String USER_HOT_DETAILS_COLLECTION = "userHotDealsMaster";

	String PAYTM_PAYMENT_POSTBACK_COLLECTION = "paytmPaymentPostbackMaster";
	String CASHFREE_PAYMENT_POSTBACK_COLLECTION = "cashFreePaymentPostbackMaster";

	String FIRST_GAME_COLLECTION = "first_game_events";
	String NEW_USER_COLLECTION = "new_users";

	String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS";

	String TRANSFER_USER_BALANCE_TRACK_MASTER = "transferUserBalanceTrackMaster";

	String CASHBACK_REDIS_PREFIX = "CASHBACK_";

	int CHAMP_STATE_WINNERS = 4;
	String USER_TOTAL_ADD_MONEY_COLLECTION = "usersTotalAddMoneyMaster";

	String PAYMENT_FRAUD_DAY_COUNT_COLLECTION = "paymentFraudDayCount";

	String PAYMENT_TOTAL_FRAUD_COUNT_COLLECTION = "paymentTotalFraudCount";
}
