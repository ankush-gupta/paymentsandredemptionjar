package com.games.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paytm.pg.merchant.CheckSumServiceHelper;

public class AppCommonUtils {
	private static final Logger logs = LoggerFactory.getLogger(AppCommonUtils.class);

	public static String getPaytmCheckSum(String merchantKey, TreeMap<String, String> paytmParams) {
		try {
			return CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(merchantKey, paytmParams);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getPaytmCheckSum(String merchantKey, String paramap) {
		try {
			return CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(merchantKey, paramap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currdate = dateFormat.format(cal.getTime());
		return currdate;
	}

	public static String getCurrentTime() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(zone);
		String currdate = dateFormat.format(cal.getTime());

		return currdate;
	}

	public static String getUpiTransferUtrNumber(Map<String, Object> mapResponse) {
		List<String> utrList;
		try {
			if (mapResponse != null) {
				if (mapResponse.get("utr") != null) {
					utrList = (ArrayList<String>) mapResponse.get("utr");
					if (utrList.size() > 0) {
						return " | UTR - " + utrList.get(0);
					}
				}
			}

		} catch (Exception e) {
			logs.error("Error occured in getUpiTransferUtrNumber::" + e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public static void main(String args[]) {

		TreeMap<String, String> paytmParams = new TreeMap<String, String>();
		paytmParams.put("MID", "Coolbo75695045425974");
		paytmParams.put("ORDERID", "QGCPM000864339");

		System.out.println("" + AppCommonUtils.getPaytmCheckSum("KtcFiIc9QAOWzsUz", paytmParams));

		Calendar expireCalendar = Calendar.getInstance();

		expireCalendar.add(Calendar.DATE, -4);

		System.out.println("==========" + new SimpleDateFormat("yyyy-MM-dd").format(expireCalendar.getTime()));

	}

}
