package com.games.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import com.paytm.pg.merchant.CheckSumServiceHelper;

public class TransferPaytmUtils {
	private static final Logger LOGS = LoggerFactory.getLogger(TransferPaytmUtils.class);

	private final String MERCHANT_GUID;
	private final String SALES_WALLET_GUID;
	private final String IP_ADDRESS;
	private final String MERCHANT_KEY;
	private final String PAYTM_SALES_TO_USER_CREDIT;
	private final String TRANSACTION_STATUS;

	public TransferPaytmUtils(String merchantKey, String merchantGuid, String salesWalletGuid, String ipaddress,
			String paytmSalesToUserCredit, String transactionStatus) {
		this.MERCHANT_GUID = merchantGuid;
		this.SALES_WALLET_GUID = salesWalletGuid;
		this.IP_ADDRESS = ipaddress;
		this.PAYTM_SALES_TO_USER_CREDIT = paytmSalesToUserCredit;
		this.MERCHANT_KEY = merchantKey;
		this.TRANSACTION_STATUS = transactionStatus;
	}

	String getPaytmSalesToUserCreditRequestBody(String orderNo, String mobileNo, int amount) throws Exception {
		JSONObject mainBody = null;
		JSONObject requestBody = null;
		String isNull = null;
		try {
			mainBody = new JSONObject();
			requestBody = new JSONObject();
			requestBody.put("requestType", isNull);
			requestBody.put("merchantGuid", MERCHANT_GUID);
			requestBody.put("merchantOrderId", orderNo);
			requestBody.put("salesWalletName", isNull);
			requestBody.put("salesWalletGuid", SALES_WALLET_GUID);
			requestBody.put("payeeEmailId", isNull);
			requestBody.put("payeePhoneNumber", mobileNo);
			requestBody.put("payeeSsoId", isNull);
			requestBody.put("appliedToNewUsers", "N");
			requestBody.put("amount", amount + "");
			requestBody.put("currencyCode", "INR");
			requestBody.put("pendingDaysLimit", "0");

			mainBody.put("request", requestBody);
			mainBody.put("metadata", mobileNo + ":" + orderNo + ":" + amount);
			mainBody.put("ipAddress", IP_ADDRESS);
			mainBody.put("platformName", "");
			mainBody.put("operationType", "SALES_TO_USER_CREDIT");

		} catch (Exception e) {
			throw e;
		}

		return mainBody.toString();
	}

	public String paytmTransferRequest(String orderNo, String mobileNo, int amount) {
		HttpURLConnection connection = null;
		URL url = null;
		String paytmRequestBody = "";
		try {

			LOGS.info("Paytm Transfer url::"+PAYTM_SALES_TO_USER_CREDIT);
			url = new URL(PAYTM_SALES_TO_USER_CREDIT);

			paytmRequestBody = getPaytmSalesToUserCreditRequestBody(orderNo, mobileNo, amount);

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");

			String CHECKSUMHASH = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(MERCHANT_KEY,
					paytmRequestBody.toString());

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("mid", MERCHANT_GUID);
			connection.setRequestProperty("checksumhash", CHECKSUMHASH);
			connection.setRequestProperty("Content-Length", Integer.toString(paytmRequestBody.getBytes().length));
			connection.setReadTimeout(20*1000);
			connection.setConnectTimeout(20*1000);
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			
			
			LOGS.info("Paytm Transfer body::"+paytmRequestBody);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(paytmRequestBody);
			wr.close();

			InputStream is;
			try {
				is = connection.getInputStream();
			} catch (Exception e) {
				is = connection.getErrorStream();
			}

			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			int statusCode = connection.getResponseCode();
			StringBuilder response = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			
			LOGS.info("Paytm Transfer response::"+response);
			return response.toString();
		} catch (Exception e) {
			
			LOGS.info("Paytm Error response::"+e.getMessage());
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return "";
	}

	public String paytmTransactionStatus(String orderNo){
		HttpURLConnection connection = null;
		URL url = null;
		String paytmRequestBody = "";
		try {
			LOGS.info("Paytm Transfer status url::"+TRANSACTION_STATUS);
			url = new URL(TRANSACTION_STATUS);

			paytmRequestBody = getPaytmTransactionStatusRequestBody(orderNo);

			

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");

			String CHECKSUMHASH = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(MERCHANT_KEY,
					paytmRequestBody.toString());

		
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("mid", MERCHANT_GUID);

			connection.setRequestProperty("checksumhash", CHECKSUMHASH);
			connection.setRequestProperty("Content-Length", Integer.toString(paytmRequestBody.getBytes().length));
			connection.setReadTimeout(20*1000);
			connection.setConnectTimeout(20*1000);
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			
			
			LOGS.info("Paytm Transfer status body::"+paytmRequestBody);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(paytmRequestBody);
			wr.close();

			InputStream is;
			try {
				is = connection.getInputStream();
			} catch (Exception e) {
				is = connection.getErrorStream();
			}

			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			int statusCode = connection.getResponseCode();
			StringBuilder response = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();

			LOGS.info("Paytm Transfer status response::"+response);
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return "";
	}

	String getPaytmTransactionStatusRequestBody(String orderNo) throws Exception {
		JSONObject mainBody = null;
		JSONObject requestBody = null;
		try {

			mainBody = new JSONObject();
			requestBody = new JSONObject();
			requestBody.put("requestType", "merchanttxnid");
			requestBody.put("txnType", "salestouser");
			requestBody.put("txnId", orderNo);
			requestBody.put("merchantGuid", MERCHANT_GUID);

			mainBody.put("request", requestBody);
			mainBody.put("ipAddress", IP_ADDRESS);
			mainBody.put("platformName", "PayTM");
			mainBody.put("operationType", "CHECK_TXN_STATUS");

		} catch (Exception e) {
			throw e;
		}

		return mainBody.toString();
	}

	public static void main(String args[]) throws Exception {
		

		/*
				transfer.paytm.baseurl=https://trust.paytm.in
					transfer.paytm.salesToUserCredit=/wallet-web/salesToUserCredit
					transfer.paytm.checkStatus=/wallet-web/checkStatus
					transfer.paytm.merchant.mid=COOLBO50148505325203
					transfer.paytm.merchant.guid=39f34409-ba12-422c-b542-fe6ae2acd83a
					transfer.paytm.sales.wallet.guid=0a84069f-b9ca-4d3b-ba1f-8482cc69c831
					transfer.paytm.merchant.key=SQSqjCN8G4#TY0q2
					transfer.paytm.ip.address=127.0.0.1
					transfer.status.check.minutus.delay=5
				*/
				
				
				
			/*	TransferPaytmUtils paytmUtils=new TransferPaytmUtils("SQSqjCN8G4#TY0q2", "39f34409-ba12-422c-b542-fe6ae2acd83a", "0a84069f-b9ca-4d3b-ba1f-8482cc69c831", "127.0.01",
						"https://trust.paytm.in//wallet-web/salesToUserCredit", "https://trust.paytm.in//wallet-web/checkStatus");
				
				paytmUtils.paytmTransferRequest("ABC0013", "9963985863", 1);
				
				//paytmUtils.paytmTransactionStatus("ABC0002");
			
				*/
				JSONObject requestBody = new JSONObject();
				/*
				requestBody.put("subwalletGuid", "59b1ebb5-52d9-11ea-8708-fa163e429e83");
				requestBody.put("orderId", "LF0020");
				requestBody.put("beneficiaryAccount", "913010033466077");
				requestBody.put("beneficiaryIFSC", "UTIB0000030");
				requestBody.put("amount", "100.00");
				requestBody.put("purpose", "OTHERS");
				requestBody.put("transactionType", "NON_CASHBACK");
				requestBody.put("transferMode", "NEFT");
				*/
				
			/**/	requestBody.put("subwalletGuid", "59b1ebb5-52d9-11ea-8708-fa163e429e83");
				requestBody.put("orderId", "LF0021");
				requestBody.put("beneficiaryVPA", "9963985863@paytm");
				
				requestBody.put("amount", "100.00");
				requestBody.put("purpose", "OTHERS");
				requestBody.put("transactionType", "NON_CASHBACK");
				requestBody.put("transferMode", "UPI");
				
				
			
				
				
				
				
				
				
				
				
				System.out.println("requestBody:::"+requestBody);
				
				String CHECKSUMHASH = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum("lgbAaFDql_DZDd@_",
						requestBody.toString());
				
				System.out.println("CHECKSUMHASH:::"+CHECKSUMHASH);
				

			}
}
