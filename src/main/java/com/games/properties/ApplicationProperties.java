package com.games.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties {

	@Value("${mongo.notification.collections}")
	private String mongoNotificationColelctions;

	@Value("${notification.tracking.url}")
	private String notificationTrackingUrl;

	@Value("${reward.wallet.expiry}")
	private int rewardWalletExpiry;

	public String getNotificationTrackingUrl() {
		return notificationTrackingUrl;
	}

	public int getRewardWalletExpiry() {
		return rewardWalletExpiry;
	}

	public void setRewardWalletExpiry(int rewardWalletExpiry) {
		this.rewardWalletExpiry = rewardWalletExpiry;
	}

	public void setNotificationTrackingUrl(String notificationTrackingUrl) {
		this.notificationTrackingUrl = notificationTrackingUrl;
	}

	public String getMongoNotificationColelctions() {
		return mongoNotificationColelctions;
	}

	public void setMongoNotificationColelctions(String mongoNotificationColelctions) {
		this.mongoNotificationColelctions = mongoNotificationColelctions;
	}

}
