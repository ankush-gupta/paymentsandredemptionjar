package com.games.properties;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TokenTournmentProperties {

	@Value("${token.tournment.gameId}")
	private int gameId;

	@Value("${token.tournment.name}")
	private String name;

	@Value("${token.tournment.active}")
	private String tournmentActive;

	public String getTournmentActive() {
		return tournmentActive;
	}

	public void setTournmentActive(String tournmentActive) {
		this.tournmentActive = tournmentActive;
	}

	@Value("${token.tournment.users}")
	private int users;

	@Value("${token.tournment.prizeMoney}")
	private String prizeMoney;

	@Value("${token.tournment.prizeMoneyCash}")
	private int prizeMoneyCash;

	@Value("${token.tournment.prizeMoneyToken}")
	private int prizeMoneyToken;

	@Value("${token.tournment.entryFee}")
	private String entryFee;

	@Value("${token.tournment.entryFeeCash}")
	private int entryFeeCash;

	@Value("${token.tournment.entryFeeToken}")
	private int entryFeeToken;

	@Value("${token.tournment.duration.mints}")
	private int tournmentDurationMints;

	@Value("${token.tournment.percentText}")
	private String percentText;

	@Value("#{'${token.tournment.rank.min}'.split(',')}")
	private List<Integer> minRank;

	@Value("#{'${token.tournment.rank.max}'.split(',')}")
	private List<Integer> maxRank;

	@Value("#{'${token.tournment.rank.prizeMoney}'.split(',')}")
	private List<String> rankPrizeMoney;

	@Value("#{'${token.tournment.rank.prizeMoneyCash}'.split(',')}")
	private List<Integer> rankPrizeMoneyCash;

	@Value("#{'${token.tournment.rank.prizeMoneyToken}'.split(',')}")
	private List<Integer> rankprizeMoneyToken;

	public int getUsers() {
		return users;
	}

	public void setUsers(int users) {
		this.users = users;
	}

	public int getTournmentDurationMints() {
		return tournmentDurationMints;
	}

	public void setTournmentDurationMints(int tournmentDurationMints) {
		this.tournmentDurationMints = tournmentDurationMints;
	}

	public List<Integer> getMinRank() {
		return minRank;
	}

	public List<Integer> getMaxRank() {
		return maxRank;
	}

	public List<String> getRankPrizeMoney() {
		return rankPrizeMoney;
	}

	public List<Integer> getRankPrizeMoneyCash() {
		return rankPrizeMoneyCash;
	}

	public List<Integer> getRankprizeMoneyToken() {
		return rankprizeMoneyToken;
	}

	public void setMinRank(List<Integer> minRank) {
		this.minRank = minRank;
	}

	public void setMaxRank(List<Integer> maxRank) {
		this.maxRank = maxRank;
	}

	public void setRankPrizeMoney(List<String> rankPrizeMoney) {
		this.rankPrizeMoney = rankPrizeMoney;
	}

	public void setRankPrizeMoneyCash(List<Integer> rankPrizeMoneyCash) {
		this.rankPrizeMoneyCash = rankPrizeMoneyCash;
	}

	public void setRankprizeMoneyToken(List<Integer> rankprizeMoneyToken) {
		this.rankprizeMoneyToken = rankprizeMoneyToken;
	}

	public int getGameId() {
		return gameId;
	}

	public String getName() {
		return name;
	}

	public String getPrizeMoney() {
		return prizeMoney;
	}

	public int getPrizeMoneyCash() {
		return prizeMoneyCash;
	}

	public int getPrizeMoneyToken() {
		return prizeMoneyToken;
	}

	public String getEntryFee() {
		return entryFee;
	}

	public int getEntryFeeCash() {
		return entryFeeCash;
	}

	public int getEntryFeeToken() {
		return entryFeeToken;
	}

	public String getPercentText() {
		return percentText;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrizeMoney(String prizeMoney) {
		this.prizeMoney = prizeMoney;
	}

	public void setPrizeMoneyCash(int prizeMoneyCash) {
		this.prizeMoneyCash = prizeMoneyCash;
	}

	public void setPrizeMoneyToken(int prizeMoneyToken) {
		this.prizeMoneyToken = prizeMoneyToken;
	}

	public void setEntryFee(String entryFee) {
		this.entryFee = entryFee;
	}

	public void setEntryFeeCash(int entryFeeCash) {
		this.entryFeeCash = entryFeeCash;
	}

	public void setEntryFeeToken(int entryFeeToken) {
		this.entryFeeToken = entryFeeToken;
	}

	public void setPercentText(String percentText) {
		this.percentText = percentText;
	}

}
