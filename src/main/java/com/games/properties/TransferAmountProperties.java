package com.games.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
public class TransferAmountProperties {

	@Value("${cointab.api.key}")
	private String coinTabApiKey;

	@Value("${transfer.paytm.merchant.mid}")
	private String transferPaytmMerchantMid;

	public String getTransferPaytmMerchantMid() {
		return transferPaytmMerchantMid;
	}

	public void setTransferPaytmMerchantMid(String transferPaytmMerchantMid) {
		this.transferPaytmMerchantMid = transferPaytmMerchantMid;
	}

	@Value("${cointab.api.paymentChannel}")
	private String coinTabPaymentChannel;

	@Value("${cointab.api.baseurl}")
	private String coinTabApiBaseUrl;

	@Value("${cointab.api.verifyBHIMUPIID}")
	private String coinTabVerifyBhimUpiIdUri;

	@Value("${cointab.api.initiatePayment}")
	private String coinTabInitiatePaymentUri;

	@Value("${cointab.api.checkPaymentStatus}")
	private String coinTabApicheckPaymentStatusUri;

	@Value("${transfer.paytm.baseurl}")
	private String transferPaytmBaseUrl;

	@Value("${transfer.paytm.salesToUserCredit}")
	private String transferPaytmSalesToUserCreditUri;

	@Value("${transfer.paytm.checkStatus}")
	private String transferPaytmCheckStatusUri;

	@Value("${transfer.paytm.merchant.guid}")
	private String transferPaytmMerchantGuid;

	@Value("${transfer.paytm.sales.wallet.guid}")
	private String transferPaytmSalesWalletGuid;

	@Value("${transfer.paytm.merchant.key}")
	private String transferPaytmMerchantKey;

	@Value("${transfer.paytm.ip.address}")
	private String transferPaytmIpAddress;

	public String getTransferPaytmSuccNotificTitle() {
		return transferPaytmSuccNotificTitle;
	}

	public String getTransferPaytmSuccNotificMessage() {
		return transferPaytmSuccNotificMessage;
	}

	public String getTransferUpiSuccNotificTitle() {
		return transferUpiSuccNotificTitle;
	}

	public String getTransferUpiSuccNotificMessage() {
		return transferUpiSuccNotificMessage;
	}

	public String getTransferPaytmFailNotificTitle() {
		return transferPaytmFailNotificTitle;
	}

	public String getTransferPaytmFailNotificMessage() {
		return transferPaytmFailNotificMessage;
	}

	public String getTransferUpiFailNotificTitle() {
		return transferUpiFailNotificTitle;
	}

	public String getTransferUpiFailNotificMessage() {
		return transferUpiFailNotificMessage;
	}

	public void setTransferPaytmSuccNotificTitle(String transferPaytmSuccNotificTitle) {
		this.transferPaytmSuccNotificTitle = transferPaytmSuccNotificTitle;
	}

	public void setTransferPaytmSuccNotificMessage(String transferPaytmSuccNotificMessage) {
		this.transferPaytmSuccNotificMessage = transferPaytmSuccNotificMessage;
	}

	public void setTransferUpiSuccNotificTitle(String transferUpiSuccNotificTitle) {
		this.transferUpiSuccNotificTitle = transferUpiSuccNotificTitle;
	}

	public void setTransferUpiSuccNotificMessage(String transferUpiSuccNotificMessage) {
		this.transferUpiSuccNotificMessage = transferUpiSuccNotificMessage;
	}

	public void setTransferPaytmFailNotificTitle(String transferPaytmFailNotificTitle) {
		this.transferPaytmFailNotificTitle = transferPaytmFailNotificTitle;
	}

	public void setTransferPaytmFailNotificMessage(String transferPaytmFailNotificMessage) {
		this.transferPaytmFailNotificMessage = transferPaytmFailNotificMessage;
	}

	public void setTransferUpiFailNotificTitle(String transferUpiFailNotificTitle) {
		this.transferUpiFailNotificTitle = transferUpiFailNotificTitle;
	}

	public void setTransferUpiFailNotificMessage(String transferUpiFailNotificMessage) {
		this.transferUpiFailNotificMessage = transferUpiFailNotificMessage;
	}

	@Value("${transfer.status.check.minutus.delay}")
	private int transferStatusCheckMinutsDelay;

	@Value("${transfer.paytm.succ.notific.title}")
	private String transferPaytmSuccNotificTitle;
	@Value("${transfer.paytm.succ.notific.message}")
	private String transferPaytmSuccNotificMessage;
	@Value("${transfer.upi.succ.notific.title}")
	private String transferUpiSuccNotificTitle;
	@Value("${transfer.upi.succ.notific.message}")
	private String transferUpiSuccNotificMessage;
	@Value("${transfer.paytm.fail.notific.title}")
	private String transferPaytmFailNotificTitle;
	@Value("${transfer.paytm.fail.notific.message}")
	private String transferPaytmFailNotificMessage;
	@Value("${transfer.upi.fail.notific.title}")
	private String transferUpiFailNotificTitle;
	@Value("${transfer.upi.fail.notific.message}")
	private String transferUpiFailNotificMessage;

	@Value("${pro.transfer.paytm.succ.notific.title}")
	private String proTransferPaytmSuccNotificTitle;
	@Value("${pro.transfer.paytm.succ.notific.message}")
	private String proTransferPaytmSuccNotificMessage;
	@Value("${pro.transfer.upi.succ.notific.title}")
	private String proTransferUpiSuccNotificTitle;
	@Value("${pro.transfer.upi.succ.notific.message}")
	private String proTransferUpiSuccNotificMessage;
	@Value("${pro.transfer.paytm.fail.notific.title}")
	private String proTransferPaytmFailNotificTitle;
	@Value("${pro.transfer.paytm.fail.notific.message}")
	private String proTransferPaytmFailNotificMessage;
	@Value("${pro.transfer.upi.fail.notific.title}")
	private String proTransferUpiFailNotificTitle;
	@Value("${pro.transfer.upi.fail.notific.message}")
	private String proTransferUpiFailNotificMessage;
	

	
	public String getSportsTransferUpiFailNotificMessage() {
		return sportsTransferUpiFailNotificMessage;
	}

	public String getSportsTransferUpiFailNotificTitle() {
		return sportsTransferUpiFailNotificTitle;
	}

	public String getSportsTransferPaytmFailNotificMessage() {
		return sportsTransferPaytmFailNotificMessage;
	}

	public String getSportsTransferPaytmFailNotificTitle() {
		return sportsTransferPaytmFailNotificTitle;
	}

	public String getSportsTransferUpiSuccNotificMessage() {
		return sportsTransferUpiSuccNotificMessage;
	}

	public String getSportsTransferUpiSuccNotificTitle() {
		return sportsTransferUpiSuccNotificTitle;
	}

	public String getSportsTransferPaytmSuccNotificMessage() {
		return sportsTransferPaytmSuccNotificMessage;
	}

	public String getSportsTransferPaytmSuccNotificTitle() {
		return sportsTransferPaytmSuccNotificTitle;
	}

	public void setSportsTransferUpiFailNotificMessage(String sportsTransferUpiFailNotificMessage) {
		this.sportsTransferUpiFailNotificMessage = sportsTransferUpiFailNotificMessage;
	}

	public void setSportsTransferUpiFailNotificTitle(String sportsTransferUpiFailNotificTitle) {
		this.sportsTransferUpiFailNotificTitle = sportsTransferUpiFailNotificTitle;
	}

	public void setSportsTransferPaytmFailNotificMessage(String sportsTransferPaytmFailNotificMessage) {
		this.sportsTransferPaytmFailNotificMessage = sportsTransferPaytmFailNotificMessage;
	}

	public void setSportsTransferPaytmFailNotificTitle(String sportsTransferPaytmFailNotificTitle) {
		this.sportsTransferPaytmFailNotificTitle = sportsTransferPaytmFailNotificTitle;
	}

	public void setSportsTransferUpiSuccNotificMessage(String sportsTransferUpiSuccNotificMessage) {
		this.sportsTransferUpiSuccNotificMessage = sportsTransferUpiSuccNotificMessage;
	}

	public void setSportsTransferUpiSuccNotificTitle(String sportsTransferUpiSuccNotificTitle) {
		this.sportsTransferUpiSuccNotificTitle = sportsTransferUpiSuccNotificTitle;
	}

	public void setSportsTransferPaytmSuccNotificMessage(String sportsTransferPaytmSuccNotificMessage) {
		this.sportsTransferPaytmSuccNotificMessage = sportsTransferPaytmSuccNotificMessage;
	}

	public void setSportsTransferPaytmSuccNotificTitle(String sportsTransferPaytmSuccNotificTitle) {
		this.sportsTransferPaytmSuccNotificTitle = sportsTransferPaytmSuccNotificTitle;
	}

	@Value("${sports.transfer.upi.fail.notific.message}")
	private String sportsTransferUpiFailNotificMessage;
	
	
	@Value("${sports.transfer.upi.fail.notific.title}")
	private String sportsTransferUpiFailNotificTitle;
	
	
	@Value("${sports.transfer.paytm.fail.notific.message}")
	private String sportsTransferPaytmFailNotificMessage;
	
	
	@Value("${sports.transfer.paytm.fail.notific.title}")
	private String sportsTransferPaytmFailNotificTitle;
	
	
	@Value("${sports.transfer.upi.succ.notific.message}")
	private String sportsTransferUpiSuccNotificMessage;
	
	
	@Value("${sports.transfer.upi.succ.notific.title}")
	private String sportsTransferUpiSuccNotificTitle;
	
	
	@Value("${sports.transfer.paytm.succ.notific.message}")
	private String sportsTransferPaytmSuccNotificMessage;
	
	
	@Value("${sports.transfer.paytm.succ.notific.title}")
	private String sportsTransferPaytmSuccNotificTitle;
	
	

	public String getProTransferPaytmSuccNotificTitle() {
		return proTransferPaytmSuccNotificTitle;
	}

	public String getProTransferPaytmSuccNotificMessage() {
		return proTransferPaytmSuccNotificMessage;
	}

	public String getProTransferUpiSuccNotificTitle() {
		return proTransferUpiSuccNotificTitle;
	}

	public String getProTransferUpiSuccNotificMessage() {
		return proTransferUpiSuccNotificMessage;
	}

	public String getProTransferPaytmFailNotificTitle() {
		return proTransferPaytmFailNotificTitle;
	}

	public String getProTransferPaytmFailNotificMessage() {
		return proTransferPaytmFailNotificMessage;
	}

	public String getProTransferUpiFailNotificTitle() {
		return proTransferUpiFailNotificTitle;
	}

	public String getProTransferUpiFailNotificMessage() {
		return proTransferUpiFailNotificMessage;
	}

	public void setProTransferPaytmSuccNotificTitle(String proTransferPaytmSuccNotificTitle) {
		this.proTransferPaytmSuccNotificTitle = proTransferPaytmSuccNotificTitle;
	}

	public void setProTransferPaytmSuccNotificMessage(String proTransferPaytmSuccNotificMessage) {
		this.proTransferPaytmSuccNotificMessage = proTransferPaytmSuccNotificMessage;
	}

	public void setProTransferUpiSuccNotificTitle(String proTransferUpiSuccNotificTitle) {
		this.proTransferUpiSuccNotificTitle = proTransferUpiSuccNotificTitle;
	}

	public void setProTransferUpiSuccNotificMessage(String proTransferUpiSuccNotificMessage) {
		this.proTransferUpiSuccNotificMessage = proTransferUpiSuccNotificMessage;
	}

	public void setProTransferPaytmFailNotificTitle(String proTransferPaytmFailNotificTitle) {
		this.proTransferPaytmFailNotificTitle = proTransferPaytmFailNotificTitle;
	}

	public void setProTransferPaytmFailNotificMessage(String proTransferPaytmFailNotificMessage) {
		this.proTransferPaytmFailNotificMessage = proTransferPaytmFailNotificMessage;
	}

	public void setProTransferUpiFailNotificTitle(String proTransferUpiFailNotificTitle) {
		this.proTransferUpiFailNotificTitle = proTransferUpiFailNotificTitle;
	}

	public void setProTransferUpiFailNotificMessage(String proTransferUpiFailNotificMessage) {
		this.proTransferUpiFailNotificMessage = proTransferUpiFailNotificMessage;
	}

	public int getTransferStatusCheckMinutsDelay() {
		return transferStatusCheckMinutsDelay;
	}

	public void setTransferStatusCheckMinutsDelay(int transferStatusCheckMinutsDelay) {
		this.transferStatusCheckMinutsDelay = transferStatusCheckMinutsDelay;
	}

	public String getCoinTabPaymentChannel() {
		return coinTabPaymentChannel;
	}

	public void setCoinTabPaymentChannel(String coinTabPaymentChannel) {
		this.coinTabPaymentChannel = coinTabPaymentChannel;
	}

	public String getTransferPaytmIpAddress() {
		return transferPaytmIpAddress;
	}

	public void setTransferPaytmIpAddress(String transferPaytmIpAddress) {
		this.transferPaytmIpAddress = transferPaytmIpAddress;
	}

	public String getCoinTabApiKey() {
		return coinTabApiKey;
	}

	public String getCoinTabApiBaseUrl() {
		return coinTabApiBaseUrl;
	}

	public String getCoinTabVerifyBhimUpiIdUri() {
		return coinTabVerifyBhimUpiIdUri;
	}

	public String getCoinTabInitiatePaymentUri() {
		return coinTabInitiatePaymentUri;
	}

	public String getCoinTabApicheckPaymentStatusUri() {
		return coinTabApicheckPaymentStatusUri;
	}

	public String getTransferPaytmBaseUrl() {
		return transferPaytmBaseUrl;
	}

	public String getTransferPaytmSalesToUserCreditUri() {
		return transferPaytmSalesToUserCreditUri;
	}

	public String getTransferPaytmCheckStatusUri() {
		return transferPaytmCheckStatusUri;
	}

	public String getTransferPaytmMerchantGuid() {
		return transferPaytmMerchantGuid;
	}

	public String getTransferPaytmSalesWalletGuid() {
		return transferPaytmSalesWalletGuid;
	}

	public String getTransferPaytmMerchantKey() {
		return transferPaytmMerchantKey;
	}

	public void setCoinTabApiKey(String coinTabApiKey) {
		this.coinTabApiKey = coinTabApiKey;
	}

	public void setCoinTabApiBaseUrl(String coinTabApiBaseUrl) {
		this.coinTabApiBaseUrl = coinTabApiBaseUrl;
	}

	public void setCoinTabVerifyBhimUpiIdUri(String coinTabVerifyBhimUpiIdUri) {
		this.coinTabVerifyBhimUpiIdUri = coinTabVerifyBhimUpiIdUri;
	}

	public void setCoinTabInitiatePaymentUri(String coinTabInitiatePaymentUri) {
		this.coinTabInitiatePaymentUri = coinTabInitiatePaymentUri;
	}

	public void setCoinTabApicheckPaymentStatusUri(String coinTabApicheckPaymentStatusUri) {
		this.coinTabApicheckPaymentStatusUri = coinTabApicheckPaymentStatusUri;
	}

	public void setTransferPaytmBaseUrl(String transferPaytmBaseUrl) {
		this.transferPaytmBaseUrl = transferPaytmBaseUrl;
	}

	public void setTransferPaytmSalesToUserCreditUri(String transferPaytmSalesToUserCreditUri) {
		this.transferPaytmSalesToUserCreditUri = transferPaytmSalesToUserCreditUri;
	}

	public void setTransferPaytmCheckStatusUri(String transferPaytmCheckStatusUri) {
		this.transferPaytmCheckStatusUri = transferPaytmCheckStatusUri;
	}

	public void setTransferPaytmMerchantGuid(String transferPaytmMerchantGuid) {
		this.transferPaytmMerchantGuid = transferPaytmMerchantGuid;
	}

	public void setTransferPaytmSalesWalletGuid(String transferPaytmSalesWalletGuid) {
		this.transferPaytmSalesWalletGuid = transferPaytmSalesWalletGuid;
	}

	public void setTransferPaytmMerchantKey(String transferPaytmMerchantKey) {
		this.transferPaytmMerchantKey = transferPaytmMerchantKey;
	}

}
