package com.games.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NotificationProperties {
	
	@Value("${fcm.app.key}")
    private String fcmAppKey;
	
	@Value("${fcm.push.url}")
    private String fcmPushURL;
	
	@Value("${fcm.notifier.api.url}")
    private String fcmNotifierApiURL;
	
	
	public String getFcmAppKey() {
		return fcmAppKey;
	}

	public void setFcmAppKey(String fcmAppKey) {
		this.fcmAppKey = fcmAppKey;
	}

	public String getFcmPushURL() {
		return fcmPushURL;
	}

	public void setFcmPushURL(String fcmPushURL) {
		this.fcmPushURL = fcmPushURL;
	}
	
	public String getFcmNotifierApiURL() {
		return fcmNotifierApiURL;
	}

	public void setFcmNotifierApiURL(String fcmNotifierApiURL) {
		this.fcmNotifierApiURL = fcmNotifierApiURL;
	}

	@Override
	public String toString() {
		return "NotificationProperties [fcmAppKey=" + fcmAppKey + ", fcmPushURL=" + fcmPushURL + ", fcmNotifierApiURL="
				+ fcmNotifierApiURL + "]";
	}	

}
