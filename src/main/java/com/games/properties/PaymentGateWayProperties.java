package com.games.properties;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component

public class PaymentGateWayProperties {

	@Value("#{'${manual.redemption.payment.error.codes}'.split(',')}")
	private List<String> manualRedemptionPaymentErrorCodes;
	
	@Value("${referal.reward.percentage}")
	private int referalRewardPercentage;

	@Value("${referal.reward.max.amount}")
	private int referalRewardMaxAmount;
	
	@Value("${pay.referal.reward.amount}")
	private int payReferalRewardAmount;

	@Value("${referal.reward.min.amount}")
	private int referalRewardMinAmount;

	@Value("${cashback.visible.cond.amount}")
	private int cashbackVisibleCondAmount;

	@Value("${cashback.visible.cond.days.intervals}")
	private int cashbackVisibleCondDaysIntervals;

	
	
	public List<String> getManualRedemptionPaymentErrorCodes() {
		return manualRedemptionPaymentErrorCodes;
	}

	public void setManualRedemptionPaymentErrorCodes(List<String> manualRedemptionPaymentErrorCodes) {
		this.manualRedemptionPaymentErrorCodes = manualRedemptionPaymentErrorCodes;
	}

	public int getPayReferalRewardAmount() {
		return payReferalRewardAmount;
	}

	public void setPayReferalRewardAmount(int payReferalRewardAmount) {
		this.payReferalRewardAmount = payReferalRewardAmount;
	}

	public int getCashbackVisibleCondAmount() {
		return cashbackVisibleCondAmount;
	}

	public void setCashbackVisibleCondAmount(int cashbackVisibleCondAmount) {
		this.cashbackVisibleCondAmount = cashbackVisibleCondAmount;
	}

	public int getCashbackVisibleCondDaysIntervals() {
		return cashbackVisibleCondDaysIntervals;
	}

	public void setCashbackVisibleCondDaysIntervals(int cashbackVisibleCondDaysIntervals) {
		this.cashbackVisibleCondDaysIntervals = cashbackVisibleCondDaysIntervals;
	}

	public int getReferalRewardPercentage() {
		return referalRewardPercentage;
	}

	public void setReferalRewardPercentage(int referalRewardPercentage) {
		this.referalRewardPercentage = referalRewardPercentage;
	}

	public int getReferalRewardMaxAmount() {
		return referalRewardMaxAmount;
	}

	public void setReferalRewardMaxAmount(int referalRewardMaxAmount) {
		this.referalRewardMaxAmount = referalRewardMaxAmount;
	}

	public int getReferalRewardMinAmount() {
		return referalRewardMinAmount;
	}

	public void setReferalRewardMinAmount(int referalRewardMinAmount) {
		this.referalRewardMinAmount = referalRewardMinAmount;
	}

	@Value("${cashfree.checksum.url}")
	private String cashFreeCheckSumUrl;

	@Value("${cashfree.appId}")
	private String cashFreeAppId;

	@Value("${payment.paytm.succ.extra.tag}")
	private String paymentPaytmSuccExtraTag;

	@Value("${payment.cashfree.succ.extra.tag}")
	private String paymentCashfreeSuccExtraTag;

	@Value("${pro.user.payment.eligible}")
	private int proUserPaymentEligible;

	@Value("${pro.user.active.days}")
	private int proUserActiveDays;

	@Value("${tot.payment.succ.amount.for.referal}")
	private int totPaymentSuccAmountForReferal;

	public int getTotPaymentSuccAmountForReferal() {
		return totPaymentSuccAmountForReferal;
	}

	public void setTotPaymentSuccAmountForReferal(int totPaymentSuccAmountForReferal) {
		this.totPaymentSuccAmountForReferal = totPaymentSuccAmountForReferal;
	}

	public int getProUserPaymentEligible() {
		return proUserPaymentEligible;
	}

	public int getProUserActiveDays() {
		return proUserActiveDays;
	}

	public void setProUserPaymentEligible(int proUserPaymentEligible) {
		this.proUserPaymentEligible = proUserPaymentEligible;
	}

	public void setProUserActiveDays(int proUserActiveDays) {
		this.proUserActiveDays = proUserActiveDays;
	}

	public String getPaymentPaytmSuccExtraTag() {
		return paymentPaytmSuccExtraTag;
	}

	public String getPaymentCashfreeSuccExtraTag() {
		return paymentCashfreeSuccExtraTag;
	}

	public void setPaymentPaytmSuccExtraTag(String paymentPaytmSuccExtraTag) {
		this.paymentPaytmSuccExtraTag = paymentPaytmSuccExtraTag;
	}

	public void setPaymentCashfreeSuccExtraTag(String paymentCashfreeSuccExtraTag) {
		this.paymentCashfreeSuccExtraTag = paymentCashfreeSuccExtraTag;
	}

	@Value("${cashfree.secretKey}")
	private String cashFreeSecretKey;

	@Value("${cashfree.api.baseurl}")
	private String cashFreeApiBaseUrl;

	@Value("${cashfree.api.order.status}")
	private String cashFreeApiOrderStatus;

	@Value("${paytm.checksum.url}")
	private String paytmCheckSumUrl;

	@Value("${paytm.api.baseurl}")
	private String paytmApiBaseUrl;

	@Value("${paytm.api.order.status}")
	private String paytmApiOrderStatus;

	@Value("${paytm.api.merchantId}")
	private String paytmMerchantId;

	@Value("${paytm.api.merchantKey}")
	private String paytmMerchantKey;

	@Value("${paytm.api.webSite}")
	private String paytmApiWebSite;

	@Value("${paytm.api.industryTypeId}")
	private String paytmApiIndustryTypeId;

	@Value("${paytm.api.channelId}")
	private String paytmApiChannelId;

	@Value("${paytm.api.order.callBackUrl}")
	private String paytmApiCallBackUrl;

	public String getPaymentPaytmSuccNotificTitle() {
		return paymentPaytmSuccNotificTitle;
	}

	public String getPaymentPaytmSuccNotificMessage() {
		return paymentPaytmSuccNotificMessage;
	}

	public String getPaymentPaytmSuccBonusNotificMessage() {
		return paymentPaytmSuccBonusNotificMessage;
	}

	public String getPaymentPaytmFailNotificTitle() {
		return paymentPaytmFailNotificTitle;
	}

	public String getPaymentPaytmFailNotificMessage() {
		return paymentPaytmFailNotificMessage;
	}

	public void setPaymentPaytmSuccNotificTitle(String paymentPaytmSuccNotificTitle) {
		this.paymentPaytmSuccNotificTitle = paymentPaytmSuccNotificTitle;
	}

	public void setPaymentPaytmSuccNotificMessage(String paymentPaytmSuccNotificMessage) {
		this.paymentPaytmSuccNotificMessage = paymentPaytmSuccNotificMessage;
	}

	public void setPaymentPaytmSuccBonusNotificMessage(String paymentPaytmSuccBonusNotificMessage) {
		this.paymentPaytmSuccBonusNotificMessage = paymentPaytmSuccBonusNotificMessage;
	}

	public void setPaymentPaytmFailNotificTitle(String paymentPaytmFailNotificTitle) {
		this.paymentPaytmFailNotificTitle = paymentPaytmFailNotificTitle;
	}

	public void setPaymentPaytmFailNotificMessage(String paymentPaytmFailNotificMessage) {
		this.paymentPaytmFailNotificMessage = paymentPaytmFailNotificMessage;
	}

	public String getPaymentCashfreeSuccNotificTitle() {
		return paymentCashfreeSuccNotificTitle;
	}

	public String getPaymentCashfreeSuccNotificMessage() {
		return paymentCashfreeSuccNotificMessage;
	}

	public String getPaymentCashfreeSuccBonusNotificMessage() {
		return paymentCashfreeSuccBonusNotificMessage;
	}

	public String getPaymentCashfreeFailNotificTitle() {
		return paymentCashfreeFailNotificTitle;
	}

	public String getPaymentCashfreeFailNotificMessage() {
		return paymentCashfreeFailNotificMessage;
	}

	public void setPaymentCashfreeSuccNotificTitle(String paymentCashfreeSuccNotificTitle) {
		this.paymentCashfreeSuccNotificTitle = paymentCashfreeSuccNotificTitle;
	}

	public void setPaymentCashfreeSuccNotificMessage(String paymentCashfreeSuccNotificMessage) {
		this.paymentCashfreeSuccNotificMessage = paymentCashfreeSuccNotificMessage;
	}

	public void setPaymentCashfreeSuccBonusNotificMessage(String paymentCashfreeSuccBonusNotificMessage) {
		this.paymentCashfreeSuccBonusNotificMessage = paymentCashfreeSuccBonusNotificMessage;
	}

	public void setPaymentCashfreeFailNotificTitle(String paymentCashfreeFailNotificTitle) {
		this.paymentCashfreeFailNotificTitle = paymentCashfreeFailNotificTitle;
	}

	public void setPaymentCashfreeFailNotificMessage(String paymentCashfreeFailNotificMessage) {
		this.paymentCashfreeFailNotificMessage = paymentCashfreeFailNotificMessage;
	}

	@Value("${payment.status.check.minutus.delay}")
	private int paymentStatusCheckMinutusDelay;

	@Value("${payment.paytm.succ.notific.title}")
	private String paymentPaytmSuccNotificTitle;
	@Value("${payment.paytm.succ.notific.message}")
	private String paymentPaytmSuccNotificMessage;
	@Value("${payment.paytm.succ.bonus.notific.message}")
	private String paymentPaytmSuccBonusNotificMessage;

	@Value("${payment.cashfree.succ.notific.title}")
	private String paymentCashfreeSuccNotificTitle;
	@Value("${payment.cashfree.succ.notific.message}")
	private String paymentCashfreeSuccNotificMessage;
	@Value("${payment.cashfree.succ.bonus.notific.message}")
	private String paymentCashfreeSuccBonusNotificMessage;

	@Value("${payment.paytm.fail.notific.title}")
	private String paymentPaytmFailNotificTitle;
	@Value("${payment.paytm.fail.notific.message}")
	private String paymentPaytmFailNotificMessage;

	@Value("${payment.cashfree.fail.notific.title}")
	private String paymentCashfreeFailNotificTitle;
	@Value("${payment.cashfree.fail.notific.message}")
	private String paymentCashfreeFailNotificMessage;

	public int getPaymentStatusCheckMinutusDelay() {
		return paymentStatusCheckMinutusDelay;
	}

	public void setPaymentStatusCheckMinutusDelay(int paymentStatusCheckMinutusDelay) {
		this.paymentStatusCheckMinutusDelay = paymentStatusCheckMinutusDelay;
	}

	public String getPaytmApiCallBackUrl() {
		return paytmApiCallBackUrl;
	}

	public void setPaytmApiCallBackUrl(String paytmApiCallBackUrl) {
		this.paytmApiCallBackUrl = paytmApiCallBackUrl;
	}

	public String getPaytmApiWebSite() {
		return paytmApiWebSite;
	}

	public void setPaytmApiWebSite(String paytmApiWebSite) {
		this.paytmApiWebSite = paytmApiWebSite;
	}

	public String getPaytmApiIndustryTypeId() {
		return paytmApiIndustryTypeId;
	}

	public void setPaytmApiIndustryTypeId(String paytmApiIndustryTypeId) {
		this.paytmApiIndustryTypeId = paytmApiIndustryTypeId;
	}

	public String getPaytmApiChannelId() {
		return paytmApiChannelId;
	}

	public void setPaytmApiChannelId(String paytmApiChannelId) {
		this.paytmApiChannelId = paytmApiChannelId;
	}

	public String getPaytmCheckSumUrl() {
		return paytmCheckSumUrl;
	}

	public void setPaytmCheckSumUrl(String paytmCheckSumUrl) {
		this.paytmCheckSumUrl = paytmCheckSumUrl;
	}

	public String getPaytmApiBaseUrl() {
		return paytmApiBaseUrl;
	}

	public void setPaytmApiBaseUrl(String paytmApiBaseUrl) {
		this.paytmApiBaseUrl = paytmApiBaseUrl;
	}

	public String getPaytmApiOrderStatus() {
		return paytmApiOrderStatus;
	}

	public void setPaytmApiOrderStatus(String paytmApiOrderStatus) {
		this.paytmApiOrderStatus = paytmApiOrderStatus;
	}

	public String getPaytmMerchantId() {
		return paytmMerchantId;
	}

	public void setPaytmMerchantId(String paytmMerchantId) {
		this.paytmMerchantId = paytmMerchantId;
	}

	public String getPaytmMerchantKey() {
		return paytmMerchantKey;
	}

	public void setPaytmMerchantKey(String paytmMerchantKey) {
		this.paytmMerchantKey = paytmMerchantKey;
	}

	public String getCashFreeCheckSumUrl() {
		return cashFreeCheckSumUrl;
	}

	public String getCashFreeApiBaseUrl() {
		return cashFreeApiBaseUrl;
	}

	public void setCashFreeApiBaseUrl(String cashFreeApiBaseUrl) {
		this.cashFreeApiBaseUrl = cashFreeApiBaseUrl;
	}

	public String getCashFreeApiOrderStatus() {
		return cashFreeApiOrderStatus;
	}

	public void setCashFreeApiOrderStatus(String cashFreeApiOrderStatus) {
		this.cashFreeApiOrderStatus = cashFreeApiOrderStatus;
	}

	public void setCashFreeCheckSumUrl(String cashFreeCheckSumUrl) {
		this.cashFreeCheckSumUrl = cashFreeCheckSumUrl;
	}

	public String getCashFreeAppId() {
		return cashFreeAppId;
	}

	public void setCashFreeAppId(String cashFreeAppId) {
		this.cashFreeAppId = cashFreeAppId;
	}

	public String getCashFreeSecretKey() {
		return cashFreeSecretKey;
	}

	public void setCashFreeSecretKey(String cashFreeSecretKey) {
		this.cashFreeSecretKey = cashFreeSecretKey;
	}
}
