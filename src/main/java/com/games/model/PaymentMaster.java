package com.games.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document
public class PaymentMaster {

	@Id
	@Field
	private String id;

	@Field
	private long userId;

	@Field
	private String name;

	@Field
	private int offerId;

	@Field
	private int planAmount;

	@Field
	private int depositWalletCash;

	@Field
	private int rewardWalletCash;

	@Field
	private int dealCoins;
	@Field
	private int dealAmount;

	@Field
	private String dealName;

	@Field
	private String source;

	@Field
	private int cashback_status;

	public int getCashback_status() {
		return cashback_status;
	}

	public void setCashback_status(int cashback_status) {
		this.cashback_status = cashback_status;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getDealCoins() {
		return dealCoins;
	}

	public int getDealAmount() {
		return dealAmount;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealCoins(int dealCoins) {
		this.dealCoins = dealCoins;
	}

	public void setDealAmount(int dealAmount) {
		this.dealAmount = dealAmount;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	@Field
	private int rewardCoins;

	@Field
	private String offerType;

	@Field
	private long crtLongDate;

	@Field
	private String groupDate;

	public String getGroupDate() {
		return groupDate;
	}

	public void setGroupDate(String groupDate) {
		this.groupDate = groupDate;
	}

	@Field
	@CreatedDate
	@DateTimeFormat(iso = ISO.DATE)
	private Date createdDate;

	@Field
	private String paymentType;

	@Field
	private String paymentModes;

	@Field
	private int paymentStatus;

	@Field
	@CreatedDate
	@DateTimeFormat(iso = ISO.DATE)
	private Date modifiedDate;

	@Field
	private int ver;

	@Field
	private long mobile;

	@Field
	private String email;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public long getCrtLongDate() {
		return crtLongDate;
	}

	public void setCrtLongDate(long crtLongDate) {
		this.crtLongDate = crtLongDate;
	}

	public long getMobile() {
		return mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public int getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(int planAmount) {
		this.planAmount = planAmount;
	}

	public int getDepositWalletCash() {
		return depositWalletCash;
	}

	public void setDepositWalletCash(int depositWalletCash) {
		this.depositWalletCash = depositWalletCash;
	}

	public int getRewardWalletCash() {
		return rewardWalletCash;
	}

	public void setRewardWalletCash(int rewardWalletCash) {
		this.rewardWalletCash = rewardWalletCash;
	}

	public int getRewardCoins() {
		return rewardCoins;
	}

	public void setRewardCoins(int rewardCoins) {
		this.rewardCoins = rewardCoins;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(String paymentModes) {
		this.paymentModes = paymentModes;
	}

	public int getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
