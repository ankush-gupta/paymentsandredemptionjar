package com.games.model;

import java.util.Date;
import java.util.StringJoiner;

public class FirstGameDto {
	private long uid;
	private int status;
	private String source;
	private String added_date;
	private String crt_date;
	private int reward;
	private Date expireAt;
	

	public Date getExpireAt() {
		return expireAt;
	}

	public void setExpireAt(Date expireAt) {
		this.expireAt = expireAt;
	}

	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public long getUid() {
		return uid;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAdded_date() {
		return added_date;
	}

	public void setAdded_date(String added_date) {
		this.added_date = added_date;
	}

	public String getCrt_date() {
		return crt_date;
	}

	public void setCrt_date(String crt_date) {
		this.crt_date = crt_date;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", FirstGameDto.class.getSimpleName() + "[", "]").add("uid=" + uid)
				.add("status=" + status).add("added_date='" + added_date + "'").add("crt_date='" + crt_date + "'")
				.toString();
	}
}
