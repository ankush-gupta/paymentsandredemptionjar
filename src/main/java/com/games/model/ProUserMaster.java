package com.games.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class ProUserMaster {

	@Id
	@Field
	private String id;

	@Field
	private long userId;
	@Field
	private String added_date;
	
	
	@Field
	@DateTimeFormat(iso = ISO.DATE)
	private Date record_expire_date;

	public Date getRecord_expire_date() {
		return record_expire_date;
	}

	public void setRecord_expire_date(Date record_expire_date) {
		this.record_expire_date = record_expire_date;
	}

	private String expireDate;

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	@Field
	private int activeDays;

	public int getActiveDays() {
		return activeDays;
	}

	public void setActiveDays(int activeDays) {
		this.activeDays = activeDays;
	}

	public long getUserId() {
		return userId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAdded_date() {
		return added_date;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setAdded_date(String added_date) {
		this.added_date = added_date;
	}

}
