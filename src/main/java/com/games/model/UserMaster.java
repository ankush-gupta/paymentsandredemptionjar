package com.games.model;

import java.util.StringJoiner;

public class UserMaster {

	private long userId;
	private String aid;
	private String gaid;
	private long mobile;
	private int ver;
	private String email;
	private String imei;
	private String lang;
	private String model;
	private String os;
	private String ip;
	private String refCode;
	private int is_active;
	private String group_id;
	private int is_new;
	private int previousMaxOfferAmount;
	private int status;

	private String timestamp;
	private String added_date;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public String getGaid() {
		return gaid;
	}

	public void setGaid(String gaid) {
		this.gaid = gaid;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public int getIs_active() {
		return is_active;
	}

	public void setIs_active(int is_active) {
		this.is_active = is_active;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public int getIs_new() {
		return is_new;
	}

	public void setIs_new(int is_new) {
		this.is_new = is_new;
	}

	public int getPreviousMaxOfferAmount() {
		return previousMaxOfferAmount;
	}

	public void setPreviousMaxOfferAmount(int previousMaxOfferAmount) {
		this.previousMaxOfferAmount = previousMaxOfferAmount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getAdded_date() {
		return added_date;
	}

	public void setAdded_date(String added_date) {
		this.added_date = added_date;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", UserMaster.class.getSimpleName() + "[", "]")
				.add("userId=" + userId)
				.add("aid='" + aid + "'")
				.add("gaid='" + gaid + "'")
				.add("mobile=" + mobile)
				.add("ver=" + ver)
				.add("email='" + email + "'")
				.add("imei='" + imei + "'")
				.add("lang='" + lang + "'")
				.add("model='" + model + "'")
				.add("os='" + os + "'")
				.add("ip='" + ip + "'")
				.add("refCode='" + refCode + "'")
				.add("is_active=" + is_active)
				.add("group_id='" + group_id + "'")
				.add("is_new=" + is_new)
				.add("previousMaxOfferAmount=" + previousMaxOfferAmount)
				.add("status=" + status)
				.add("timestamp='" + timestamp + "'")
				.add("added_date='" + added_date + "'")
				.toString();
	}
}
