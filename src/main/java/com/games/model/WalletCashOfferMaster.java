package com.games.model;

public class WalletCashOfferMaster {

	private int offerId;
	private int planAmount;
	private int depositWalletCash;
	private int rewardWalletCash;
	private int dealAmount;
	private int dealCoins;

	private int rewardCoins;
	private String offerText;
	private String offerType;
	private String paymentType;
	private String paymentModes;
	private String dealName;

	private String isNewUserOffer;

	public String getIsNewUserOffer() {
		return isNewUserOffer;
	}

	public void setIsNewUserOffer(String isNewUserOffer) {
		this.isNewUserOffer = isNewUserOffer;
	}

	public int getDealCoins() {
		return dealCoins;
	}

	public void setDealCoins(int dealCoins) {
		this.dealCoins = dealCoins;
	}

	public int getDealAmount() {
		return dealAmount;
	}

	public void setDealAmount(int dealAmount) {
		this.dealAmount = dealAmount;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	private String isActive;

	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public int getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(int planAmount) {
		this.planAmount = planAmount;
	}

	public int getDepositWalletCash() {
		return depositWalletCash;
	}

	public void setDepositWalletCash(int depositWalletCash) {
		this.depositWalletCash = depositWalletCash;
	}

	public int getRewardWalletCash() {
		return rewardWalletCash;
	}

	public void setRewardWalletCash(int rewardWalletCash) {
		this.rewardWalletCash = rewardWalletCash;
	}

	public int getRewardCoins() {
		return rewardCoins;
	}

	public void setRewardCoins(int rewardCoins) {
		this.rewardCoins = rewardCoins;
	}

	public String getOfferText() {
		return offerText;
	}

	public void setOfferText(String offerText) {
		this.offerText = offerText;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(String paymentModes) {
		this.paymentModes = paymentModes;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

}
