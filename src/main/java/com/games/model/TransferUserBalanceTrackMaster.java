package com.games.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document
public class TransferUserBalanceTrackMaster {

	@Id
	@Field
	private String id;

	@Field
	private String referenceNo;

	@Field
	private String walletType;

	@Field
	private int transferAmount;

	@Field
	private long crtLongDate;

	@Field
	private int transferStatus;

	@Field
	private String transferType;

	@Field
	private String groupDate;

	@Field
	private float totalAmount;

	@Field
	private float serviceCharge;

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public float getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(float serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	@Field
	@CreatedDate
	@DateTimeFormat(iso = ISO.DATE)
	private Date createdDate;

	@Field
	@DateTimeFormat(iso = ISO.DATE)
	private Date modifiedDate;

	@Field
	private String modifiedGroupDate;

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedGroupDate() {
		return modifiedGroupDate;
	}

	public void setModifiedGroupDate(String modifiedGroupDate) {
		this.modifiedGroupDate = modifiedGroupDate;
	}

	@Field
	private double pre_redeem_balance;

	@Field
	private double post_redeem_balance;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getWalletType() {
		return walletType;
	}

	public void setWalletType(String walletType) {
		this.walletType = walletType;
	}

	public int getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(int transferAmount) {
		this.transferAmount = transferAmount;
	}

	public long getCrtLongDate() {
		return crtLongDate;
	}

	public void setCrtLongDate(long crtLongDate) {
		this.crtLongDate = crtLongDate;
	}

	public int getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(int transferStatus) {
		this.transferStatus = transferStatus;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getGroupDate() {
		return groupDate;
	}

	public void setGroupDate(String groupDate) {
		this.groupDate = groupDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public double getPre_redeem_balance() {
		return pre_redeem_balance;
	}

	public void setPre_redeem_balance(double pre_redeem_balance) {
		this.pre_redeem_balance = pre_redeem_balance;
	}

	public double getPost_redeem_balance() {
		return post_redeem_balance;
	}

	public void setPost_redeem_balance(double post_redeem_balance) {
		this.post_redeem_balance = post_redeem_balance;
	}
}
