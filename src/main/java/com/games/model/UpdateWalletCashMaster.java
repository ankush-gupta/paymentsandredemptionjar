package com.games.model;

public class UpdateWalletCashMaster {
	
	private long userId;
	private int gameAmount;
	private int rewardAmount;
	private int paytmAmount;
	private int gameCoins;
	private int rewardCoins;
	private String tType;
	private String referenceNo;
	
	
	public long getUserId() {
		return userId;
	}
	public int getGameAmount() {
		return gameAmount;
	}
	public int getRewardAmount() {
		return rewardAmount;
	}
	public int getPaytmAmount() {
		return paytmAmount;
	}
	public int getGameCoins() {
		return gameCoins;
	}
	public int getRewardCoins() {
		return rewardCoins;
	}
	
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public void setGameAmount(int gameAmount) {
		this.gameAmount = gameAmount;
	}
	public void setRewardAmount(int rewardAmount) {
		this.rewardAmount = rewardAmount;
	}
	public void setPaytmAmount(int paytmAmount) {
		this.paytmAmount = paytmAmount;
	}
	public void setGameCoins(int gameCoins) {
		this.gameCoins = gameCoins;
	}
	public void setRewardCoins(int rewardCoins) {
		this.rewardCoins = rewardCoins;
	}
	
	public String gettType() {
		return tType;
	}
	public void settType(String tType) {
		this.tType = tType;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	

	

}
