package com.games.model;

public class UsersTotalAddMoneyMaster {

	private long userId;
	private int total_plan_amount;
	private String modified_date;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getTotal_plan_amount() {
		return total_plan_amount;
	}

	public void setTotal_plan_amount(int total_plan_amount) {
		this.total_plan_amount = total_plan_amount;
	}


	public String getModified_date() {
		return modified_date;
	}

	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}

}
