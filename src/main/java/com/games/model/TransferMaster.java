package com.games.model;

import java.util.Date;
import java.util.Map;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document
public class TransferMaster {

	@Id
	@Field
	private String id;

	@Field
	private String walletType;

	public String getWalletType() {
		return walletType;
	}

	public void setWalletType(String walletType) {
		this.walletType = walletType;
	}

	@Field
	private String transferType;

	@Field
	private float totalAmount;

	@Field
	private float serviceCharge;

	@Field
	private int transferAmount;

	@Field
	private long mobileNo;

	@Field
	private String upiName;

	@Field
	private String upiId;

	@Field
	private long userId;

	@Field
	private long crtLongDate;

	@Field
	private int transferStatus;

	@Field
	private String groupDate;

	public String getGroupDate() {
		return groupDate;
	}

	public void setGroupDate(String groupDate) {
		this.groupDate = groupDate;
	}

	@Field
	private Map<String, Object> transferResponse;

	@Field
	private int ver;

	@Field
	@CreatedDate
	@DateTimeFormat(iso = ISO.DATE)
	private Date createdDate;

	@Field
	@CreatedDate
	@DateTimeFormat(iso = ISO.DATE)
	private Date modifiedDate;

	public String getId() {
		return id;
	}

	public String getTransferType() {
		return transferType;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public float getServiceCharge() {
		return serviceCharge;
	}

	public int getTransferAmount() {
		return transferAmount;
	}

	public long getMobileNo() {
		return mobileNo;
	}

	public String getUpiName() {
		return upiName;
	}

	public String getUpiId() {
		return upiId;
	}

	public long getUserId() {
		return userId;
	}

	public Map<String, Object> getTransferResponse() {
		return transferResponse;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setServiceCharge(float serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public void setTransferAmount(int transferAmount) {
		this.transferAmount = transferAmount;
	}

	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}

	public void setUpiName(String upiName) {
		this.upiName = upiName;
	}

	public void setUpiId(String upiId) {
		this.upiId = upiId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCrtLongDate() {
		return crtLongDate;
	}

	public int getTransferStatus() {
		return transferStatus;
	}

	public int getVer() {
		return ver;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setCrtLongDate(long crtLongDate) {
		this.crtLongDate = crtLongDate;
	}

	public void setTransferStatus(int transferStatus) {
		this.transferStatus = transferStatus;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setTransferResponse(Map<String, Object> transferResponse) {
		this.transferResponse = transferResponse;
	}

}
