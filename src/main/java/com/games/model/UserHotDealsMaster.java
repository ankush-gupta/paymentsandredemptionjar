package com.games.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document
public class UserHotDealsMaster {

	@Id
	@Field
	private String id;

	@Field
	private long userId;

	@Field
	private int hotId;
	
	@Field
	private int expireMinuts;
	
	public int getExpireMinuts() {
		return expireMinuts;
	}


	public void setExpireMinuts(int expireMinuts) {
		this.expireMinuts = expireMinuts;
	}

	@Field
	private int dealNo;

	public int getDealNo() {
		return dealNo;
	}


	public void setDealNo(int dealNo) {
		this.dealNo = dealNo;
	}

	@Field
	private int planAmount;

	@Field
	private int depositWalletAmount;
	
	@Field
	private int dealAmount;

	
	
	@Field
	private int dealCoins;

	@Field
	private String added_date;

	@Field
	@DateTimeFormat(iso = ISO.DATE)
	private Date record_expire_date;

	@Field
	private long crtLongDate;

	public String getId() {
		return id;
	}

	
	public int getHotId() {
		return hotId;
	}

	public int getPlanAmount() {
		return planAmount;
	}

	public int getDepositWalletAmount() {
		return depositWalletAmount;
	}

	

	public String getAdded_date() {
		return added_date;
	}

	public Date getRecord_expire_date() {
		return record_expire_date;
	}

	public long getCrtLongDate() {
		return crtLongDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public long getUserId() {
		return userId;
	}


	public void setUserId(long userId) {
		this.userId = userId;
	}


	public void setHotId(int hotId) {
		this.hotId = hotId;
	}

	public void setPlanAmount(int planAmount) {
		this.planAmount = planAmount;
	}

	public void setDepositWalletAmount(int depositWalletAmount) {
		this.depositWalletAmount = depositWalletAmount;
	}

	

	public int getDealAmount() {
		return dealAmount;
	}


	public int getDealCoins() {
		return dealCoins;
	}


	public void setDealAmount(int dealAmount) {
		this.dealAmount = dealAmount;
	}


	public void setDealCoins(int dealCoins) {
		this.dealCoins = dealCoins;
	}


	public void setAdded_date(String added_date) {
		this.added_date = added_date;
	}

	public void setRecord_expire_date(Date record_expire_date) {
		this.record_expire_date = record_expire_date;
	}

	public void setCrtLongDate(long crtLongDate) {
		this.crtLongDate = crtLongDate;
	}

}
