package com.games.model;

public class EmergencyTournmentMaster {

	private int id;
	private int gameId;
	private int entreeFeeType;
	private int formatId;

	public int getId() {
		return id;
	}

	public int getGameId() {
		return gameId;
	}

	public int getEntreeFeeType() {
		return entreeFeeType;
	}

	public int getFormatId() {
		return formatId;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public void setEntreeFeeType(int entreeFeeType) {
		this.entreeFeeType = entreeFeeType;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

}
