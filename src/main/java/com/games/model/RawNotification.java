package com.games.model;

public class RawNotification {
	private long userId;
	private int game_id;
	private String body;
	private String title;
	private String landing;
	private String icon;
	private int n_type;
	private int ttl;
	private int tid;
	
	
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public long getUserId() {
		return userId;
	}
	public int getGame_id() {
		return game_id;
	}
	public String getBody() {
		return body;
	}
	public String getTitle() {
		return title;
	}
	public String getLanding() {
		return landing;
	}
	public String getIcon() {
		return icon;
	}
	public int getN_type() {
		return n_type;
	}
	public int getTtl() {
		return ttl;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public void setGame_id(int game_id) {
		this.game_id = game_id;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setLanding(String landing) {
		this.landing = landing;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public void setN_type(int n_type) {
		this.n_type = n_type;
	}
	public void setTtl(int ttl) {
		this.ttl = ttl;
	}
	

}
