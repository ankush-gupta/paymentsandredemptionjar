package com.games.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document
public class ProUserHistoryMaster {

	@Id
	@Field
	private String id;

	@Field
	private long userId;

	@Field
	private int activeDays;

	public int getActiveDays() {
		return activeDays;
	}

	public void setActiveDays(int activeDays) {
		this.activeDays = activeDays;
	}

	@Field
	private String added_date;

	@Field
	private int planAmount;

	@Field
	private long crtLongDate;

	@Field
	private String referenceNo;

	@Field
	private String ttype;

	public int getPlanAmount() {
		return planAmount;
	}

	public long getCrtLongDate() {
		return crtLongDate;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public String getTtype() {
		return ttype;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setPlanAmount(int planAmount) {
		this.planAmount = planAmount;
	}

	public void setCrtLongDate(long crtLongDate) {
		this.crtLongDate = crtLongDate;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public void setTtype(String ttype) {
		this.ttype = ttype;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Field
	@CreatedDate
	@DateTimeFormat(iso = ISO.DATE)
	private Date createdDate;

	public long getUserId() {
		return userId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAdded_date() {
		return added_date;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setAdded_date(String added_date) {
		this.added_date = added_date;
	}

}
