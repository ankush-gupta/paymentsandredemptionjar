package com.games.model;

import java.util.List;
import java.util.Map;

public class FcmJsonMaster {

	private Map<String,Object> data;
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	private String priority;
	private String time_to_live;
	private List<String> registration_ids;
	
	public String getPriority() {
		return priority;
	}
	public String getTime_to_live() {
		return time_to_live;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public void setTime_to_live(String time_to_live) {
		this.time_to_live = time_to_live;
	}
	public void setRegistration_ids(String registration_ids) {
	
}
	public List<String> getRegistration_ids() {
		return registration_ids;
	}
	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}
}
