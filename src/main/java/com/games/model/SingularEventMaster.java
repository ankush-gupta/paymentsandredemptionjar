package com.games.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Document
public class SingularEventMaster {
	@Id
	@Field
	private String id;

	@Field
	private String aid;

	@Field
	private String gaid;

	@Field
	private int ver;

	@Field
	private String loginDate;

	@Field
	private int userId;

	@Field
	private String mobile;

	@Field
	private String email;

	@Field
	private String group_id;

	@Field
	private int is_new;

	@Field
	private int is_new_status;

	@Field
	private String isNewEventProcessed;

	@Field
	private int cagEvent;

	@Field
	private int cagCount;

	@Field
	private int cagStatus;

	@Field
	private String addCagTime;

	@Field
	private String addCagProcessed;

	@Field
	private int conEvent;
	@Field
	private int conCount;
	@Field
	private int conStatus;

	@Field
	private String addConTime;

	@Field
	private String addConProcessed;

	@Field
	private int addLow;
	@Field
	private int addLowAmt;
	@Field
	private int addLowStatus;

	@Field
	private String addLowTime;

	@Field
	private String addLowProcessed;

	@Field
	private int addMid;
	@Field
	private int addMidAmt;

	@Field
	private int addMidStatus;

	@Field
	private String addMidTime;

	@Field
	private String addMidProcessed;

	@Field
	private int addAll;

	@Field
	private int addAllStatus;

	@Field
	private String addAllTime;

	@Field
	private String addAllProcessed;


	public int getIs_new_status() {
		return is_new_status;
	}

	public void setIs_new_status(int is_new_status) {
		this.is_new_status = is_new_status;
	}

	@Field
	private String otpVerifyDate;

	public String getLoginDate() {
		return loginDate;
	}

	public String getOtpVerifyDate() {
		return otpVerifyDate;
	}

	public String getIsNewEventProcessed() {
		return isNewEventProcessed;
	}

	public String getAddCagTime() {
		return addCagTime;
	}

	public String getAddCagProcessed() {
		return addCagProcessed;
	}

	public String getAddConTime() {
		return addConTime;
	}

	public String getAddConProcessed() {
		return addConProcessed;
	}

	public String getAddLowTime() {
		return addLowTime;
	}

	public String getAddLowProcessed() {
		return addLowProcessed;
	}

	public String getAddMidTime() {
		return addMidTime;
	}

	public String getAddMidProcessed() {
		return addMidProcessed;
	}

	public String getAddAllTime() {
		return addAllTime;
	}

	public String getAddAllProcessed() {
		return addAllProcessed;
	}

	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}

	public void setOtpVerifyDate(String otpVerifyDate) {
		this.otpVerifyDate = otpVerifyDate;
	}

	public void setIsNewEventProcessed(String isNewEventProcessed) {
		this.isNewEventProcessed = isNewEventProcessed;
	}

	public void setAddCagTime(String addCagTime) {
		this.addCagTime = addCagTime;
	}

	public void setAddCagProcessed(String addCagProcessed) {
		this.addCagProcessed = addCagProcessed;
	}

	public void setAddConTime(String addConTime) {
		this.addConTime = addConTime;
	}

	public void setAddConProcessed(String addConProcessed) {
		this.addConProcessed = addConProcessed;
	}

	public void setAddLowTime(String addLowTime) {
		this.addLowTime = addLowTime;
	}

	public void setAddLowProcessed(String addLowProcessed) {
		this.addLowProcessed = addLowProcessed;
	}

	public void setAddMidTime(String addMidTime) {
		this.addMidTime = addMidTime;
	}

	public void setAddMidProcessed(String addMidProcessed) {
		this.addMidProcessed = addMidProcessed;
	}

	public void setAddAllTime(String addAllTime) {
		this.addAllTime = addAllTime;
	}

	public void setAddAllProcessed(String addAllProcessed) {
		this.addAllProcessed = addAllProcessed;
	}


	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getId() {
		return id;
	}

	public String getAid() {
		return aid;
	}

	public String getGaid() {
		return gaid;
	}

	public String getEmail() {
		return email;
	}

	public String getGroup_id() {
		return group_id;
	}

	public int getVer() {
		return ver;
	}

	public int getUserId() {
		return userId;
	}

	public int getIs_new() {
		return is_new;
	}

	public int getCagEvent() {
		return cagEvent;
	}

	public int getCagCount() {
		return cagCount;
	}

	public int getCagStatus() {
		return cagStatus;
	}

	public int getConEvent() {
		return conEvent;
	}

	public int getConCount() {
		return conCount;
	}

	public int getConStatus() {
		return conStatus;
	}

	public int getAddLow() {
		return addLow;
	}

	public int getAddLowAmt() {
		return addLowAmt;
	}

	public int getAddLowStatus() {
		return addLowStatus;
	}

	public int getAddMid() {
		return addMid;
	}

	public int getAddMidAmt() {
		return addMidAmt;
	}

	public int getAddMidStatus() {
		return addMidStatus;
	}

	public int getAddAll() {
		return addAll;
	}

	public int getAddAllStatus() {
		return addAllStatus;
	}


	public void setId(String id) {
		this.id = id;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public void setGaid(String gaid) {
		this.gaid = gaid;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setIs_new(int is_new) {
		this.is_new = is_new;
	}

	public void setCagEvent(int cagEvent) {
		this.cagEvent = cagEvent;
	}

	public void setCagCount(int cagCount) {
		this.cagCount = cagCount;
	}

	public void setCagStatus(int cagStatus) {
		this.cagStatus = cagStatus;
	}

	public void setConEvent(int conEvent) {
		this.conEvent = conEvent;
	}

	public void setConCount(int conCount) {
		this.conCount = conCount;
	}

	public void setConStatus(int conStatus) {
		this.conStatus = conStatus;
	}

	public void setAddLow(int addLow) {
		this.addLow = addLow;
	}

	public void setAddLowAmt(int addLowAmt) {
		this.addLowAmt = addLowAmt;
	}

	public void setAddLowStatus(int addLowStatus) {
		this.addLowStatus = addLowStatus;
	}

	public void setAddMid(int addMid) {
		this.addMid = addMid;
	}

	public void setAddMidAmt(int addMidAmt) {
		this.addMidAmt = addMidAmt;
	}

	public void setAddMidStatus(int addMidStatus) {
		this.addMidStatus = addMidStatus;
	}

	public void setAddAll(int addAll) {
		this.addAll = addAll;
	}

	public void setAddAllStatus(int addAllStatus) {
		this.addAllStatus = addAllStatus;
	}


}
