package com.games.model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class TransactionDto {
	private long user_id;
	private long app_ver;
	private long amount;
	private String transaction_title; // event With game name
	private String transaction_subtitle; // tournament name
	private int transaction_type; // debit-2 / credit-1
	private String event_type; // paytm,winning,welcome bonus
	private String created;
	private String added_date;
	private long created_in_millis;
	private long t_id;
	private long g_id;
	private String referenceNo;
	private Date crtDate;

	private String source;

	public Date getCrtDate() {
		return crtDate;
	}

	public void setCrtDate(Date crtDate) {
		this.crtDate = crtDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public long getUser_id() {
		return user_id;
	}

	public long getT_id() {
		return t_id;
	}

	public void setT_id(long t_id) {
		this.t_id = t_id;
	}

	public long getG_id() {
		return g_id;
	}

	public void setG_id(long g_id) {
		this.g_id = g_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getApp_ver() {
		return app_ver;
	}

	public void setApp_ver(long app_ver) {
		this.app_ver = app_ver;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getTransaction_title() {
		return transaction_title;
	}

	public void setTransaction_title(String transaction_title) {
		this.transaction_title = transaction_title;
	}

	public String getTransaction_subtitle() {
		return transaction_subtitle;
	}

	public void setTransaction_subtitle(String transaction_subtitle) {
		this.transaction_subtitle = transaction_subtitle;
	}

	public int getTransaction_type() {
		return transaction_type;
	}

	public void setTransaction_type(int transaction_type) {
		this.transaction_type = transaction_type;
	}

	public String getEvent_type() {
		return event_type;
	}

	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getAdded_date() {
		return added_date;
	}

	public void setAdded_date(String added_date) {
		this.added_date = added_date;
	}

	public long getCreated_in_millis() {
		return created_in_millis;
	}

	public void setCreated_in_millis(long created_in_millis) {
		this.created_in_millis = created_in_millis;
	}

}
