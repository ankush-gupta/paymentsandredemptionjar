package com.games.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class NotificationMaster {

	@Id
	@Field
	private String id;

	@Field
	private Map<String, Object> data_tag;

	@Field
	private long time_to_live;

	public long getTime_to_live() {
		return time_to_live;
	}

	public void setTime_to_live(long time_to_live) {
		this.time_to_live = time_to_live;
	}

	@Field
	private List<String> fcm_tokens;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Object> getData_tag() {
		return data_tag;
	}

	public List<String> getFcm_tokens() {
		return fcm_tokens;
	}

	public void setData_tag(Map<String, Object> data_tag) {
		this.data_tag = data_tag;
	}

	public void setFcm_tokens(List<String> fcm_tokens) {
		this.fcm_tokens = fcm_tokens;
	}

}
