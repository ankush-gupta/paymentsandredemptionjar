package com.games.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.games.model.RewardTxn;
import com.games.properties.ApplicationProperties;
import com.games.util.AppCommonUtils;

@Service
public class RewardTxnServices {

	private static final Logger logs = LoggerFactory.getLogger(RewardTxnServices.class);

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Autowired
	private ApplicationProperties applicationProperties;

	public void insertIntoRewardTxn(long user_id, int reward, int reward_type) {
		try {
			RewardTxn rewardTxn = new RewardTxn();
			rewardTxn.setUser_id(user_id);
			rewardTxn.setReward(reward);
			rewardTxn.setDate(AppCommonUtils.getCurrentDate());
			rewardTxn.setExpireAt(getExpiryTime(applicationProperties.getRewardWalletExpiry())); 
			rewardTxn.setReward_type(reward_type);
			mongoTemplate.insert(rewardTxn, "reward_txn");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Date getExpiryTime(int days) throws Exception {
		TimeZone zone = TimeZone.getTimeZone("IST");
		TimeZone.setDefault(zone);

		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(zone);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		cal.add(Calendar.DATE, days);

		String datestring = dateFormat.format(cal.getTime());
		logs.info("datestring :: " + datestring);
		return dateFormat.parse(datestring);
	}

}
