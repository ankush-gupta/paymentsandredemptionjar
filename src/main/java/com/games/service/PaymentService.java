package com.games.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games.dao.HotDealsDao;
import com.games.dao.MongoCommonDao;
import com.games.dao.PaymentDao;
import com.games.dao.PaymentFactory;
import com.games.dao.PaymentOrderDao;
import com.games.dao.RedisTemplateDao;
import com.games.dao.SingularDao;
import com.games.dao.TransactionsDao;
import com.games.dao.UsersDao;
import com.games.dao.WalletTransactionDao;
import com.games.model.PaymentMaster;
import com.games.model.ProUserHistoryMaster;
import com.games.model.RawNotification;
import com.games.model.TransactionDto;
import com.games.model.UpdateWalletCashMaster;
import com.games.model.UserMaster;
import com.games.model.WalletCashOfferMaster;
import com.games.properties.PaymentGateWayProperties;
import com.games.util.CommonConstants;

@Service
public class PaymentService {
	private static final Logger logs = LoggerFactory.getLogger(PaymentService.class);

	@Autowired
	PaymentFactory paymentFactory;

	@Autowired
	PaymentGateWayProperties paymentGateWayProperties;

	@Autowired
	RedisTemplateDao redisTemplateDao;

	@Autowired
	MongoCommonDao mongoCommonDao;

	@Autowired
	SingularDao singularDao;

	@Autowired
	PaymentOrderDao paymentOrderDao;

	@Autowired
	UsersDao userDao;

	@Autowired
	HotDealsDao hotDealsDao;

	@Autowired
	WalletTransactionDao walletTransactionDao;

	@Autowired
	TransactionsDao transactionsDao;

	@Autowired
	RewardTxnServices rewardTxnServices;

	@Autowired
	UsersDao usersDao;

	/*
	 * statuReverification: false response: is null
	 * 
	 * webhook::true: hits came from the payment gateway
	 */

	public void processPaymentsByPaytm(boolean statusReVerfication, boolean webhook, String date_type) {
		List<PaymentMaster> listPaymentMaster = new ArrayList<PaymentMaster>();
		PaymentDao paymentDao;
		paymentDao = paymentFactory.paymentObjectByType(CommonConstants.PAYMENT_TYPE_PAYTM);

		try {
			if (webhook) {

				listPaymentMaster = paymentDao.getWebHookPayments();

				// to avoid raise condition from the main site
				Thread.sleep(3 * 60 * 1000);

				logs.info("paytm web hook records count::" + listPaymentMaster.size());
			} else {
				listPaymentMaster = paymentDao.getListPendingPayments(statusReVerfication, date_type);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		logs.info("payments paytm size::" + listPaymentMaster.size());
		for (PaymentMaster paymentMaster : listPaymentMaster) {

			try {
				if (statusReVerfication) {
					Thread.sleep(1 * 1000);
				}

				if (paymentOrderDao.getTransactionProcessingState(paymentMaster.getId())) {
					updatePendingPaymentsByPaytm(paymentDao, paymentMaster.getId());
				}

			} catch (Exception e) {
				logs.error("Exception occured in processPaymentsByPaytm sync blocks:::" + e.getMessage());
			}

		}
	}

	private synchronized void updatePendingPaymentsByPaytm(final PaymentDao paymentDao, String orderId) {
		// PaymentDao paymentDao;
		// PaymentMaster paymentMaster;
		Map<String, Object> mapResponse;
		int paymentStatus;
		UpdateWalletCashMaster updateWalletCashMaster;
		TransactionDto transactionDto;
		Calendar calender;
		UserMaster userMaster;
		ProUserHistoryMaster proUserHistoryMaster;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		RawNotification rawNotification;
		PaymentMaster paymentMaster;
		try {

			/*
			 * this is important because three process getting information at a time raise
			 * condition, dual giving money
			 */
			paymentMaster = paymentOrderDao.getPaymentMasterByOrderId(orderId);
			userMaster = userDao.getUserDetailsByUserId(paymentMaster.getUserId());

			if (paymentMaster != null && paymentMaster.getPaymentStatus() == CommonConstants.TXN_PENDING_STATUS) {
				mapResponse = paymentDao.checkPaymentStaus(paymentMaster.getId());

				paymentStatus = paymentDao.updatePaymentRecord(paymentMaster, mapResponse);

				if (paymentStatus == CommonConstants.TXN_SUCCESS_STATUS) {

					if (paymentMaster.getDealName() != null
							&& paymentMaster.getDealName().equals(CommonConstants.HOT_DEAL_NAME)) {
						// check already user got reward previously with this deal,
						if (paymentOrderDao.checkUserDealPaymentSuccess(paymentMaster.getId(),
								paymentMaster.getUserId(), CommonConstants.HOT_DEAL_NAME)) {
							paymentMaster.setDepositWalletCash(paymentMaster.getPlanAmount());
							paymentMaster.setDealName("");
							paymentMaster.setDealAmount(0);
							paymentMaster.setDealCoins(0);
							// update trasaction with theasse fields
							paymentOrderDao.updatePaymentOrder(paymentMaster.getId(),
									paymentMaster.getDepositWalletCash(), "", 0, 0);

							hotDealsDao.removeUserHotDeals(paymentMaster.getUserId());
						}

					} else {

						String userChoosenModeKeyWord = paymentDao.getUserChoosenPaymentMode(mapResponse);
						WalletCashOfferMaster walletCashOfferMaster = paymentOrderDao
								.getWalletCashBackOfferByAmountAndMode(paymentMaster.getPlanAmount(),
										userChoosenModeKeyWord);

						// by default plan amount
						paymentMaster.setDepositWalletCash(paymentMaster.getPlanAmount());
						paymentMaster.setRewardWalletCash(0);
						paymentMaster.setRewardCoins(0);
						paymentMaster.setCashback_status(0);

						// || (paymentMaster.getPlanAmount() < userMaster.getPreviousMaxOfferAmount())
						if (walletCashOfferMaster == null

								|| (userMaster.getPreviousMaxOfferAmount() >= paymentGateWayProperties
										.getCashbackVisibleCondAmount()
										&& paymentOrderDao.checkUserGotCashBackAmount_N_Previous_days(
												paymentMaster.getUserId(),
												paymentGateWayProperties.getCashbackVisibleCondDaysIntervals()))) {
							// do nothing
						} else {
							int upto_cash_back_amount = redisTemplateDao.getCircularListValue(
									CommonConstants.CASHBACK_REDIS_PREFIX + paymentMaster.getPlanAmount());
							logs.info("Cashback Amount for plan amount::{}::upto_cash_back_amount::{}",
									paymentMaster.getPlanAmount(), upto_cash_back_amount);

							if (upto_cash_back_amount > 0) {
								paymentMaster.setDepositWalletCash(walletCashOfferMaster.getDepositWalletCash());
								// paymentMaster.setRewardWalletCash(walletCashOfferMaster.getRewardWalletCash());
								paymentMaster.setRewardWalletCash(0);
								paymentMaster.setRewardCoins(walletCashOfferMaster.getRewardCoins());
								paymentMaster.setCashback_status(1);
							} else {
								// if deposit cash back not given then we are giving reward wallet cash
								paymentMaster.setRewardWalletCash(walletCashOfferMaster.getRewardWalletCash());
								paymentMaster.setRewardCoins(walletCashOfferMaster.getRewardCoins());
								paymentMaster.setCashback_status(1);
							}

						}

						// updating the details
						paymentOrderDao.updatePaymentOrderWithRewardModeDetails(paymentMaster);

					}

					updateWalletCashMaster = new UpdateWalletCashMaster();
					updateWalletCashMaster.setUserId(paymentMaster.getUserId());
					updateWalletCashMaster.setGameAmount(0);
					updateWalletCashMaster
							.setRewardAmount(paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount());
					updateWalletCashMaster.setPaytmAmount(paymentMaster.getDepositWalletCash());
					updateWalletCashMaster.setRewardCoins(paymentMaster.getRewardCoins());
					updateWalletCashMaster.settType(CommonConstants.CASH_WALLET_CREDIT);
					updateWalletCashMaster.setReferenceNo(paymentMaster.getId());

					walletTransactionDao.updateCashWallet(updateWalletCashMaster);

					// maintaing in the users totaal add money
					paymentOrderDao.maintainUserTotalAddMoney(paymentMaster.getUserId(), paymentMaster.getPlanAmount());
					logs.info("MAINTAINING THE USER TOTAL ADD MONEY paytm ::" + paymentMaster.getId());

					if (paymentMaster.getRewardWalletCash() > 0) {
						logs.info("i am saving in reward tx paytm");
						rewardTxnServices.insertIntoRewardTxn(paymentMaster.getUserId(),
								(paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount()), 7);
					}

					transactionDto = new TransactionDto();
					transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_SUCCESS_TITLE);
					transactionDto.setReferenceNo(paymentMaster.getId());
					transactionDto.setAdded_date(sdf.format(new Date()));
					transactionsDao.updateCashTransactions(transactionDto);

					// updating users plan amount
					usersDao.savePreviousMaxOfferAmount((int) paymentMaster.getUserId(), paymentMaster.getPlanAmount());

					if ((paymentMaster.getDepositWalletCash() - paymentMaster.getPlanAmount()) > 0) {
						/*
						 * saving transaction in common table for showing detaial to the history for his
						 * transactions
						 */
						transactionDto = new TransactionDto();
						transactionDto.setUser_id(paymentMaster.getUserId());
						transactionDto.setApp_ver(paymentMaster.getVer());
						transactionDto.setAmount(paymentMaster.getDepositWalletCash() - paymentMaster.getPlanAmount());
						transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_DEPOSIT_CASHBACK);
						transactionDto.setTransaction_subtitle("In Deposit wallet, For " + paymentMaster.getId());
						transactionDto.setReferenceNo(paymentMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto.setCrtDate(new Date());

						if (paymentMaster.getSource() != null) {
							transactionDto.setSource(paymentMaster.getSource());
						}
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);
					}

					if ((paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount()) > 0) {
						/*
						 * saving transaction in common table for showing detaial to the history for his
						 * transactions
						 */
						transactionDto = new TransactionDto();
						transactionDto.setUser_id(paymentMaster.getUserId());
						transactionDto.setApp_ver(paymentMaster.getVer());
						transactionDto.setAmount(paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount());
						transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_BONUS);
						transactionDto.setTransaction_subtitle("In Bonus wallet, For " + paymentMaster.getId());
						transactionDto.setReferenceNo(paymentMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto.setCrtDate(new Date());

						if (paymentMaster.getSource() != null) {
							transactionDto.setSource(paymentMaster.getSource());
						}
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);
					}

					if (paymentMaster.getRewardCoins() > 0) {

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(paymentMaster.getUserId());
						transactionDto.setApp_ver(paymentMaster.getVer());
						transactionDto.setAmount(paymentMaster.getRewardCoins());
						transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_BONUS);
						transactionDto.setTransaction_subtitle(paymentMaster.getId());
						transactionDto.setReferenceNo(paymentMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto.setCrtDate(new Date());
						if (paymentMaster.getSource() != null) {
							transactionDto.setSource(paymentMaster.getSource());
						}

						transactionsDao.insertTransactions(transactionDto, CommonConstants.TOKEN_TRANSACTIONS);

					}

					// saving pro users data
					if (paymentMaster.getPlanAmount() >= paymentGateWayProperties.getProUserPaymentEligible()) {

						calender = Calendar.getInstance();

						proUserHistoryMaster = new ProUserHistoryMaster();

						proUserHistoryMaster.setUserId(paymentMaster.getUserId());
						proUserHistoryMaster.setAdded_date(sdf.format(calender.getTime()));
						proUserHistoryMaster.setCreatedDate(calender.getTime());
						proUserHistoryMaster.setCrtLongDate(calender.getTimeInMillis());
						proUserHistoryMaster.setPlanAmount(paymentMaster.getPlanAmount());
						proUserHistoryMaster.setReferenceNo(paymentMaster.getId());
						proUserHistoryMaster.setTtype("ADD_MONEY");
						proUserHistoryMaster.setActiveDays(paymentGateWayProperties.getProUserActiveDays());

						usersDao.saveProUserDetails(proUserHistoryMaster);
					}

					// sending notificatio message

					rawNotification = new RawNotification();
					rawNotification.setUserId(paymentMaster.getUserId());

					rawNotification.setTitle(paymentGateWayProperties.getPaymentPaytmSuccNotificTitle()
							.replace("~PAYMENT_AMOUNT~", (paymentMaster.getRewardWalletCash()
									+ paymentMaster.getDepositWalletCash() + paymentMaster.getDealAmount()) + ""));
					rawNotification.setBody(paymentGateWayProperties.getPaymentPaytmSuccNotificMessage()
							.replace("~PAYMENT_AMOUNT~",
									(paymentMaster.getRewardWalletCash() + paymentMaster.getDepositWalletCash()
											+ paymentMaster.getDealAmount()) + "")
							.replace("~DEPOSIT_WALLET_AMOUNT~", paymentMaster.getDepositWalletCash() + ""));
					if (paymentMaster.getRewardWalletCash() > 0) {
						rawNotification.setBody(paymentGateWayProperties.getPaymentPaytmSuccNotificMessage()
								.replace("~PAYMENT_AMOUNT~",
										(paymentMaster.getRewardWalletCash() + paymentMaster.getDepositWalletCash()
												+ paymentMaster.getDealAmount()) + "")
								.replace("~DEPOSIT_WALLET_AMOUNT~", paymentMaster.getDepositWalletCash() + "")
								+ paymentGateWayProperties.getPaymentPaytmSuccBonusNotificMessage().replaceAll(
										"~REWARD_WALLET_AMOUNT~", paymentMaster.getRewardWalletCash() + ""));
					}

					// rawNotification.setLanding("wallet");
					rawNotification.setLanding("home");
					if (paymentMaster.getDealAmount() > 0) {
						rawNotification.setLanding("hotdealclosed");
					}
					rawNotification.setIcon("");

					rawNotification.setGame_id(0);
					rawNotification.setN_type(10);
					rawNotification.setTid(0);
					rawNotification.setTtl(28800);
					mongoCommonDao.saveNotificationData(rawNotification,
							CommonConstants.NOTIFICATION_COMMON_COLLECTION);

					/******** sending singular events *****************/

					if (userMaster != null) {
						if (userMaster.getAdded_date() != null
								&& userMaster.getAdded_date().equals(paymentMaster.getGroupDate())) {

							if (paymentMaster.getPlanAmount() >= 10) {
								singularDao.updateSingularEventsStatus(userMaster.getUserId(), "AddMo_New_Mid",
										paymentMaster.getPlanAmount());
							} else {
								singularDao.updateSingularEventsStatus(userMaster.getUserId(), "AddMo_New_Low",
										paymentMaster.getPlanAmount());
							}
						}

						/*
						 * int totalUserPaymentAmount = walletTransactionDao
						 * .getTotalPaymentsAmount(paymentMaster.getUserId()); if
						 * (totalUserPaymentAmount >= paymentGateWayProperties
						 * .getTotPaymentSuccAmountForReferal()) { // if total user payment amount >=10
						 * then need to sent referal success // previiously it was added on first game
						 * play userDao.verifyAndSendReferalSuccessEvent(paymentMaster.getUserId(),
						 * "ADD_MONEY", paymentGateWayProperties.getPayReferalRewardAmount()); }
						 */

						/*
						 * int totalSuccPaymentsCount = paymentOrderDao
						 * .getUserSuccPaymentOrderCount(paymentMaster.getUserId());
						 * 
						 * if (totalSuccPaymentsCount == 1) { // we are reward on first successful
						 * payment amount // previously if total user payment amount >=10 then need to
						 * sent referal // success // previiously it was added on first game play int
						 * reward = (paymentGateWayProperties.getReferalRewardPercentage()
						 * paymentMaster.getPlanAmount()) / 100;
						 * 
						 * if (reward > paymentGateWayProperties.getReferalRewardMaxAmount()) { reward =
						 * paymentGateWayProperties.getReferalRewardMaxAmount(); }
						 * 
						 * if (reward < paymentGateWayProperties.getReferalRewardMinAmount()) { reward =
						 * paymentGateWayProperties.getReferalRewardMinAmount(); }
						 * 
						 * userDao.verifyAndSendReferalSuccessEvent(paymentMaster.getUserId(),
						 * "ADD_MONEY", reward); }
						 */

					}

				} else if (paymentStatus == CommonConstants.TXN_FAILURE_STATUS) {

					transactionDto = new TransactionDto();
					transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_REQUESTED_TITLE);
					transactionDto.setReferenceNo(paymentMaster.getId());
					transactionDto.setAdded_date(sdf.format(new Date()));
					transactionsDao.updateCashTransactions(transactionDto);

					// saving failure transaction keeping additional record
					transactionDto = new TransactionDto();
					transactionDto.setUser_id(paymentMaster.getUserId());
					transactionDto.setApp_ver(paymentMaster.getVer());
					transactionDto.setAmount(paymentMaster.getPlanAmount());
					transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_FAILED_TITLE);
					transactionDto.setTransaction_subtitle("");
					switch (paymentMaster.getPaymentModes()) {
					case "paytm":
						transactionDto.setTransaction_subtitle("Paytm|" + paymentMaster.getId());
						break;
					case "upi":
						transactionDto.setTransaction_subtitle("Upi|" + paymentMaster.getId());
						break;
					case "nb":
						transactionDto.setTransaction_subtitle("Netbanking|" + paymentMaster.getId());
						break;
					case "cc":
						transactionDto.setTransaction_subtitle("Credit Card|" + paymentMaster.getId());
						break;
					case "dc":
						transactionDto.setTransaction_subtitle("Debit Card|" + paymentMaster.getId());
						break;
					case "wallet":
						transactionDto.setTransaction_subtitle("Wallet|" + paymentMaster.getId());
						break;

					default:
						transactionDto.setTransaction_subtitle(paymentMaster.getId() + "");
					}

					transactionDto.setReferenceNo(paymentMaster.getId());
					transactionDto.setTransaction_type(CommonConstants.DEBIT_EVENT);
					transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
					transactionDto.setAdded_date(sdf.format(new Date()));
					transactionDto.setCrtDate(new Date());
					transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

					/*
					 * // track if the response having frauad error code then save the information
					 * 
					 * paymentDao.checkPaymentFraudStatusCodeAndTrackInfo(paymentMaster.getUserId(),
					 * mapResponse,
					 * paymentGateWayProperties.getManualRedemptionPaymentErrorCodes());
					 */
					// sending notificatio message

					rawNotification = new RawNotification();
					rawNotification.setUserId(paymentMaster.getUserId());

					rawNotification.setTitle(paymentGateWayProperties.getPaymentPaytmFailNotificTitle()
							.replace("~PAYMENT_AMOUNT~", paymentMaster.getPlanAmount() + ""));
					rawNotification.setBody(paymentGateWayProperties.getPaymentPaytmFailNotificMessage()
							.replace("~PAYMENT_AMOUNT~", paymentMaster.getPlanAmount() + ""));

					rawNotification.setLanding("add_money");
					rawNotification.setIcon("");

					rawNotification.setGame_id(0);
					rawNotification.setN_type(10);
					rawNotification.setTid(0);
					rawNotification.setTtl(28800);
					mongoCommonDao.saveNotificationData(rawNotification,
							CommonConstants.NOTIFICATION_COMMON_COLLECTION);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void processPaymentsByCashFree(boolean statusReVerfication, boolean webhook, String date_type) {
		List<PaymentMaster> listPaymentMaster = new ArrayList<PaymentMaster>();
		PaymentDao paymentDao;
		paymentDao = paymentFactory.paymentObjectByType(CommonConstants.PAYMENT_TYPE_CASHFREE);

		try {
			if (webhook) {
				listPaymentMaster = paymentDao.getWebHookPayments();
				// to avoid raise condition
				Thread.sleep(2 * 60 * 1000);
			} else {
				listPaymentMaster = paymentDao.getListPendingPayments(statusReVerfication, date_type);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		logs.info("payments cashfree size::" + listPaymentMaster.size());
		for (PaymentMaster paymentMaster : listPaymentMaster) {
			try {
				if (statusReVerfication) {
					Thread.sleep(1 * 1000);
				}

				if (paymentOrderDao.getTransactionProcessingState(paymentMaster.getId())) {
					updatePendingPaymentsByCashFreee(paymentDao, paymentMaster.getId());
				}

			} catch (Exception e) {
				logs.error("Exception occured in processPaymentsByCashFree sync blocks:::" + e.getMessage());
			}

		}
	}

	private synchronized void updatePendingPaymentsByCashFreee(final PaymentDao paymentDao, String orderId) {

		Map<String, Object> mapResponse;
		RawNotification rawNotification;
		TransactionDto transactionDto;
		Calendar calender;
		ProUserHistoryMaster proUserHistoryMaster;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		int paymentStatus;

		UserMaster userMaster;
		UpdateWalletCashMaster updateWalletCashMaster;
		PaymentMaster paymentMaster;
		try {

			/*
			 * this is important because three process getting information at a time raise
			 * condition, dual giving money
			 */
			paymentMaster = paymentOrderDao.getPaymentMasterByOrderId(orderId);
			userMaster = userDao.getUserDetailsByUserId(paymentMaster.getUserId());
			if (paymentMaster != null && paymentMaster.getPaymentStatus() == CommonConstants.TXN_PENDING_STATUS) {
				mapResponse = paymentDao.checkPaymentStaus(paymentMaster.getId());
				paymentStatus = paymentDao.updatePaymentRecord(paymentMaster, mapResponse);

				if (paymentStatus == CommonConstants.TXN_SUCCESS_STATUS) {

					if (paymentMaster.getDealName() != null
							&& paymentMaster.getDealName().equals(CommonConstants.HOT_DEAL_NAME)) {
						// check already user got reward previously with this deal,
						if (paymentOrderDao.checkUserDealPaymentSuccess(paymentMaster.getId(),
								paymentMaster.getUserId(), CommonConstants.HOT_DEAL_NAME)) {

							paymentMaster.setDepositWalletCash(paymentMaster.getPlanAmount());
							paymentMaster.setDealName("");
							paymentMaster.setDealAmount(0);
							paymentMaster.setDealCoins(0);
							// update trasaction with theasse fields
							paymentOrderDao.updatePaymentOrder(paymentMaster.getId(),
									paymentMaster.getDepositWalletCash(), "", 0, 0);
							hotDealsDao.removeUserHotDeals(paymentMaster.getUserId());
						}

					} else {

						String userChoosenModeKeyWord = paymentDao.getUserChoosenPaymentMode(mapResponse);
						WalletCashOfferMaster walletCashOfferMaster = paymentOrderDao
								.getWalletCashBackOfferByAmountAndMode(paymentMaster.getPlanAmount(),
										userChoosenModeKeyWord);

						// by default giving plan amount
						paymentMaster.setDepositWalletCash(paymentMaster.getPlanAmount());
						paymentMaster.setRewardWalletCash(0);
						paymentMaster.setRewardCoins(0);
						paymentMaster.setCashback_status(0);
						// || (paymentMaster.getPlanAmount() < userMaster.getPreviousMaxOfferAmount())
						if (walletCashOfferMaster == null

								|| (userMaster.getPreviousMaxOfferAmount() >= paymentGateWayProperties
										.getCashbackVisibleCondAmount()
										&& paymentOrderDao.checkUserGotCashBackAmount_N_Previous_days(
												paymentMaster.getUserId(),
												paymentGateWayProperties.getCashbackVisibleCondDaysIntervals()))) {
							// do nothing
						} else {

							int upto_cash_back_amount = redisTemplateDao.getCircularListValue(
									CommonConstants.CASHBACK_REDIS_PREFIX + paymentMaster.getPlanAmount());
							logs.info("Cashback Amount for plan amount::{}::upto_cash_back_amount::{}",
									paymentMaster.getPlanAmount(), upto_cash_back_amount);

							if (upto_cash_back_amount > 0) {
								paymentMaster.setDepositWalletCash(walletCashOfferMaster.getDepositWalletCash());
								// paymentMaster.setRewardWalletCash(walletCashOfferMaster.getRewardWalletCash());
								paymentMaster.setRewardWalletCash(0);
								paymentMaster.setRewardCoins(walletCashOfferMaster.getRewardCoins());
								paymentMaster.setCashback_status(1);
							} else {
								// if deposit cash back not given then we are giving reward wallet cash
								paymentMaster.setRewardWalletCash(walletCashOfferMaster.getRewardWalletCash());
								paymentMaster.setRewardCoins(walletCashOfferMaster.getRewardCoins());
								paymentMaster.setCashback_status(1);
							}

						}

						// updating the details
						paymentOrderDao.updatePaymentOrderWithRewardModeDetails(paymentMaster);

					}

					updateWalletCashMaster = new UpdateWalletCashMaster();
					updateWalletCashMaster.setUserId(paymentMaster.getUserId());
					updateWalletCashMaster.setGameAmount(0);
					updateWalletCashMaster
							.setRewardAmount(paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount());
					updateWalletCashMaster.setPaytmAmount(paymentMaster.getDepositWalletCash());
					updateWalletCashMaster.setRewardCoins(paymentMaster.getRewardCoins());
					updateWalletCashMaster.settType(CommonConstants.CASH_WALLET_CREDIT);
					updateWalletCashMaster.setReferenceNo(paymentMaster.getId());

					walletTransactionDao.updateCashWallet(updateWalletCashMaster);
//maintaing in the users totaal add money
					paymentOrderDao.maintainUserTotalAddMoney(paymentMaster.getUserId(), paymentMaster.getPlanAmount());
					logs.info("MAINTAINING THE USER TOTAL ADD MONEY ::" + paymentMaster.getId());

					if (paymentMaster.getRewardWalletCash() > 0) {
						logs.info("i am saving in reward tx upi");
						rewardTxnServices.insertIntoRewardTxn(paymentMaster.getUserId(),
								(paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount()), 7);
					}

					transactionDto = new TransactionDto();
					transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_SUCCESS_TITLE);
					transactionDto.setReferenceNo(paymentMaster.getId());
					transactionDto.setAdded_date(sdf.format(new Date()));
					transactionsDao.updateCashTransactions(transactionDto);

					// updating users plan amount
					usersDao.savePreviousMaxOfferAmount((int) paymentMaster.getUserId(), paymentMaster.getPlanAmount());

					if ((paymentMaster.getDepositWalletCash() - paymentMaster.getPlanAmount()) > 0) {
						/*
						 * saving transaction in common table for showing detaial to the history for his
						 * transactions
						 */
						transactionDto = new TransactionDto();
						transactionDto.setUser_id(paymentMaster.getUserId());
						transactionDto.setApp_ver(paymentMaster.getVer());
						transactionDto.setAmount(paymentMaster.getDepositWalletCash() - paymentMaster.getPlanAmount());
						transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_DEPOSIT_CASHBACK);
						transactionDto.setTransaction_subtitle("In Deposit wallet, For " + paymentMaster.getId());
						transactionDto.setReferenceNo(paymentMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto.setCrtDate(new Date());
						if (paymentMaster.getSource() != null) {
							transactionDto.setSource(paymentMaster.getSource());
						}
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);
					}

					if ((paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount()) > 0) {
						/*
						 * saving transaction in common table for showing detaial to the history for his
						 * transactions
						 */
						transactionDto = new TransactionDto();
						transactionDto.setUser_id(paymentMaster.getUserId());
						transactionDto.setApp_ver(paymentMaster.getVer());
						transactionDto.setAmount(paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount());
						transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_BONUS);
						transactionDto.setTransaction_subtitle("In Bonus wallet, For " + paymentMaster.getId());
						transactionDto.setReferenceNo(paymentMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);
					}

					if (paymentMaster.getRewardCoins() > 0) {

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(paymentMaster.getUserId());
						transactionDto.setApp_ver(paymentMaster.getVer());
						transactionDto.setAmount(paymentMaster.getRewardCoins());
						transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_BONUS);
						transactionDto.setTransaction_subtitle(paymentMaster.getId());
						transactionDto.setReferenceNo(paymentMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.TOKEN_TRANSACTIONS);

					}

					// saving pro users data
					if (paymentMaster.getPlanAmount() >= paymentGateWayProperties.getProUserPaymentEligible()) {

						calender = Calendar.getInstance();

						proUserHistoryMaster = new ProUserHistoryMaster();

						proUserHistoryMaster.setUserId(paymentMaster.getUserId());
						proUserHistoryMaster.setAdded_date(sdf.format(calender.getTime()));
						proUserHistoryMaster.setCreatedDate(calender.getTime());
						proUserHistoryMaster.setCrtLongDate(calender.getTimeInMillis());
						proUserHistoryMaster.setPlanAmount(paymentMaster.getPlanAmount());
						proUserHistoryMaster.setReferenceNo(paymentMaster.getId());
						proUserHistoryMaster.setTtype("ADD_MONEY");
						proUserHistoryMaster.setActiveDays(paymentGateWayProperties.getProUserActiveDays());

						usersDao.saveProUserDetails(proUserHistoryMaster);
					}

					// sending notificatio message

					rawNotification = new RawNotification();
					rawNotification.setUserId(paymentMaster.getUserId());

					rawNotification.setTitle(paymentGateWayProperties.getPaymentCashfreeSuccNotificTitle()
							.replace("~PAYMENT_AMOUNT~", (paymentMaster.getRewardWalletCash()
									+ paymentMaster.getDepositWalletCash() + paymentMaster.getDealAmount()) + ""));
					rawNotification.setBody(paymentGateWayProperties.getPaymentCashfreeSuccNotificMessage()
							.replace("~PAYMENT_AMOUNT~",
									(paymentMaster.getRewardWalletCash() + paymentMaster.getDepositWalletCash()
											+ paymentMaster.getDealAmount()) + "")
							.replace("~DEPOSIT_WALLET_AMOUNT~", paymentMaster.getDepositWalletCash() + ""));
					if (paymentMaster.getRewardWalletCash() > 0) {
						rawNotification.setBody(paymentGateWayProperties.getPaymentCashfreeSuccNotificMessage()
								.replace("~PAYMENT_AMOUNT~",
										(paymentMaster.getRewardWalletCash() + paymentMaster.getDepositWalletCash())
												+ "")
								.replace("~DEPOSIT_WALLET_AMOUNT~", paymentMaster.getDepositWalletCash() + "")
								+ paymentGateWayProperties.getPaymentCashfreeSuccBonusNotificMessage().replaceAll(
										"~REWARD_WALLET_AMOUNT~",
										(paymentMaster.getRewardWalletCash() + paymentMaster.getDealAmount()) + ""));
					}
					// rawNotification.setLanding("wallet");
					rawNotification.setLanding("home");
					if (paymentMaster.getDealAmount() > 0) {
						rawNotification.setLanding("hotdealclosed");
					}
					rawNotification.setIcon("");

					rawNotification.setGame_id(0);
					rawNotification.setN_type(10);
					rawNotification.setTid(0);
					rawNotification.setTtl(28800);
					mongoCommonDao.saveNotificationData(rawNotification,
							CommonConstants.NOTIFICATION_COMMON_COLLECTION);

					/******** sending singular events *****************/

					if (userMaster != null) {
						if (userMaster.getAdded_date() != null
								&& userMaster.getAdded_date().equals(paymentMaster.getGroupDate())) {

							if (paymentMaster.getPlanAmount() >= 10) {
								singularDao.updateSingularEventsStatus(userMaster.getUserId(), "AddMo_New_Mid",
										paymentMaster.getPlanAmount());
							} else {
								singularDao.updateSingularEventsStatus(userMaster.getUserId(), "AddMo_New_Low",
										paymentMaster.getPlanAmount());
							}

						}

						/*
						 * int totalUserPaymentAmount = walletTransactionDao
						 * .getTotalPaymentsAmount(paymentMaster.getUserId()); if
						 * (totalUserPaymentAmount >= paymentGateWayProperties
						 * .getTotPaymentSuccAmountForReferal()) { // if total user payment amount >=10
						 * then need to sent referal success // previiously it was added on first game
						 * play userDao.verifyAndSendReferalSuccessEvent(paymentMaster.getUserId(),
						 * "ADD_MONEY", paymentGateWayProperties.getPayReferalRewardAmount()); }
						 */

						/*
						 * int totalSuccPaymentsCount = paymentOrderDao
						 * .getUserSuccPaymentOrderCount(paymentMaster.getUserId());
						 * 
						 * if (totalSuccPaymentsCount == 1) { // we are reward on first successful
						 * payment amount // previously if total user payment amount >=10 then need to
						 * sent referal // success // previiously it was added on first game play int
						 * reward = (paymentGateWayProperties.getReferalRewardPercentage()
						 * paymentMaster.getPlanAmount()) / 100;
						 * 
						 * if (reward > paymentGateWayProperties.getReferalRewardMaxAmount()) { reward =
						 * paymentGateWayProperties.getReferalRewardMaxAmount(); }
						 * 
						 * if (reward < paymentGateWayProperties.getReferalRewardMinAmount()) { reward =
						 * paymentGateWayProperties.getReferalRewardMinAmount(); }
						 * 
						 * userDao.verifyAndSendReferalSuccessEvent(paymentMaster.getUserId(),
						 * "ADD_MONEY", reward); }
						 */

					}

				} else if (paymentStatus == CommonConstants.TXN_FAILURE_STATUS) {

					transactionDto = new TransactionDto();
					transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_REQUESTED_TITLE);
					transactionDto.setReferenceNo(paymentMaster.getId());
					transactionDto.setAdded_date(sdf.format(new Date()));
					transactionsDao.updateCashTransactions(transactionDto);

					transactionDto = new TransactionDto();
					transactionDto.setUser_id(paymentMaster.getUserId());
					transactionDto.setApp_ver(paymentMaster.getVer());
					transactionDto.setAmount(paymentMaster.getPlanAmount());
					transactionDto.setTransaction_title(CommonConstants.ADD_MONEY_FAILED_TITLE);
					transactionDto.setTransaction_subtitle("");
					switch (paymentMaster.getPaymentModes()) {
					case "paytm":
						transactionDto.setTransaction_subtitle("Paytm|" + paymentMaster.getId());
						break;
					case "upi":
						transactionDto.setTransaction_subtitle("Upi|" + paymentMaster.getId());
						break;
					case "nb":
						transactionDto.setTransaction_subtitle("Netbanking|" + paymentMaster.getId());
						break;
					case "cc":
						transactionDto.setTransaction_subtitle("Credit Card|" + paymentMaster.getId());
						break;
					case "dc":
						transactionDto.setTransaction_subtitle("Debit Card|" + paymentMaster.getId());
						break;
					case "wallet":
						transactionDto.setTransaction_subtitle("Wallet|" + paymentMaster.getId());
						break;

					default:
						transactionDto.setTransaction_subtitle(paymentMaster.getId() + "");
					}
					transactionDto.setReferenceNo(paymentMaster.getId());
					transactionDto.setTransaction_type(CommonConstants.DEBIT_EVENT);
					transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_ADD_MONEY);
					transactionDto.setAdded_date(sdf.format(new Date()));
					transactionDto.setCrtDate(new Date());
					transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

					// sending notificatio message

					rawNotification = new RawNotification();
					rawNotification.setUserId(paymentMaster.getUserId());

					rawNotification.setTitle(paymentGateWayProperties.getPaymentCashfreeFailNotificTitle()
							.replace("~PAYMENT_AMOUNT~", paymentMaster.getPlanAmount() + ""));
					rawNotification.setBody(paymentGateWayProperties.getPaymentCashfreeFailNotificMessage()
							.replace("~PAYMENT_AMOUNT~", paymentMaster.getPlanAmount() + ""));

					rawNotification.setLanding("add_money");
					rawNotification.setIcon("");

					rawNotification.setGame_id(0);
					rawNotification.setN_type(10);
					rawNotification.setTid(0);
					rawNotification.setTtl(28800);
					mongoCommonDao.saveNotificationData(rawNotification,
							CommonConstants.NOTIFICATION_COMMON_COLLECTION);

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
