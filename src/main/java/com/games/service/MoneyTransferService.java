package com.games.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games.dao.MongoCommonDao;
import com.games.dao.TransactionsDao;
import com.games.dao.TransferDao;
import com.games.dao.TransferFactory;
import com.games.dao.WalletTransactionDao;
import com.games.model.RawNotification;
import com.games.model.TransactionDto;
import com.games.model.TransferMaster;
import com.games.model.UpdateWalletCashMaster;
import com.games.properties.TransferAmountProperties;
import com.games.util.AppCommonUtils;
import com.games.util.CommonConstants;

@Service
public class MoneyTransferService {
	private static final Logger logs = LoggerFactory.getLogger(MoneyTransferService.class);

	@Autowired
	TransferFactory transferFactory;

	@Autowired
	WalletTransactionDao walletTransactionDao;

	@Autowired
	TransferAmountProperties transferAmountProperties;

	@Autowired
	MongoCommonDao mongoCommonDao;

	@Autowired
	TransactionsDao transactionsDao;

	public void transferAmountThroughPaytm() {
		// TransferMaster transferMaster;
		TransferDao transferDao;
		try {
			transferDao = transferFactory.TransferObjectByType(CommonConstants.TRANSFER_TYPE_PAYTM);

			List<TransferMaster> listTransferMaster = transferDao.getListInitRecords();

			for (TransferMaster transferMaster : listTransferMaster) {
				transferDao.transferAmount(transferMaster);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void transferStatuCheckByPaytmService(boolean kycTransaction) {
		List<TransferMaster> listTransferMaster;
		TransferDao transferDao;
		transferDao = transferFactory.TransferObjectByType(CommonConstants.TRANSFER_TYPE_PAYTM);

		if (kycTransaction) {
			listTransferMaster = transferDao.getListKycPendingRecords();
		} else {
			listTransferMaster = transferDao.getListPendingRecords();
		}

		for (TransferMaster transferMaster : listTransferMaster) {
			transferStatusCheckByPaytm(transferDao, transferMaster);
		}

	}

	void transferStatusCheckByPaytm(final TransferDao transferDao, final TransferMaster transferMaster) {
		// TransferMaster transferMaster;
		Map<String, Object> mapResponse;
		// TransferDao transferDao;
		int transfertStatus;
		RawNotification rawNotification;
		UpdateWalletCashMaster updateWalletCashMaster;
		TransactionDto transactionDto;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			// transferDao =
			// transferFactory.TransferObjectByType(CommonConstants.TRANSFER_TYPE_PAYTM);

			/*
			 * if (kycTransaction) { transferMaster = transferDao.getKycPendingRecords(); }
			 * else { transferMaster = transferDao.getPendingRecords(); }
			 */

			if (transferMaster != null) {
				mapResponse = transferDao.getTransferStaus(transferMaster);
				transfertStatus = transferDao.updateTransferRecord(transferMaster, mapResponse);

				if (transferMaster.getWalletType() != null
						&& transferMaster.getWalletType().equals(CommonConstants.PRO_WALLET)) {

					// pro wallet
					if (transfertStatus == CommonConstants.TXN_FAILURE_STATUS) {

						// refunding the amount to pro wallet
						walletTransactionDao.refundProWalletAmount(transferMaster.getUserId(),
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));

						// updatating old transaction
						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(transferMaster.getUserId());
						transactionDto.setApp_ver(transferMaster.getVer());
						transactionDto.setAmount(transferMaster.getTransferAmount());
						transactionDto.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_FAIL_TITLE);
						transactionDto.setTransaction_subtitle(
								CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_PAYTM + "|" + transferMaster.getId());
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setUser_id(transferMaster.getUserId());
							transactionDto.setApp_ver(transferMaster.getVer());
							transactionDto.setAmount((int) transferMaster.getServiceCharge());
							transactionDto
									.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_FAIL);
							transactionDto.setTransaction_subtitle(transferMaster.getId());
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
							transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
							transactionDto.setCrtDate(new Date());
							transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getProTransferPaytmFailNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTotalAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getProTransferPaytmFailNotificMessage());

						rawNotification.setLanding("withdraw_money");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_FAILURE_STATUS);

					} else if (transfertStatus == CommonConstants.TXN_SUCCESS_STATUS) {
						// sending notificatio message

						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_SUCC_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto
									.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_SUCC);
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setAdded_date(sdf.format(new Date()));
							transactionsDao.updateCashTransactions(transactionDto);
						}

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getProTransferPaytmSuccNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTransferAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getProTransferPaytmSuccNotificMessage());

						rawNotification.setLanding("wallet");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_SUCCESS_STATUS);

					}
				} else if (transferMaster.getWalletType() != null
						&& transferMaster.getWalletType().equals(CommonConstants.SPORTS_WALLET)) {

					// sports wallet
					if (transfertStatus == CommonConstants.TXN_FAILURE_STATUS) {

						// refunding the amount to sports wallet
						walletTransactionDao.refundSportsWalletAmount(transferMaster.getUserId(),
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));

						// updatating old transaction
						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(transferMaster.getUserId());
						transactionDto.setApp_ver(transferMaster.getVer());
						transactionDto.setAmount(transferMaster.getTransferAmount());
						transactionDto.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_FAIL_TITLE);
						transactionDto.setTransaction_subtitle(
								CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_PAYTM + "|" + transferMaster.getId());
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setUser_id(transferMaster.getUserId());
							transactionDto.setApp_ver(transferMaster.getVer());
							transactionDto.setAmount((int) transferMaster.getServiceCharge());
							transactionDto.setTransaction_title(
									CommonConstants.SPORTS_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_FAIL);
							transactionDto.setTransaction_subtitle(transferMaster.getId());
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
							transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
							transactionDto.setCrtDate(new Date());
							transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getSportsTransferPaytmFailNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTotalAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getSportsTransferPaytmFailNotificMessage());

						rawNotification.setLanding("withdraw_money");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_FAILURE_STATUS);

					} else if (transfertStatus == CommonConstants.TXN_SUCCESS_STATUS) {
						// sending notificatio message

						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_SUCC_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setTransaction_title(
									CommonConstants.SPORTS_CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_SUCC);
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setAdded_date(sdf.format(new Date()));
							transactionsDao.updateCashTransactions(transactionDto);
						}

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getSportsTransferPaytmSuccNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTransferAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getSportsTransferPaytmSuccNotificMessage());

						rawNotification.setLanding("wallet");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_SUCCESS_STATUS);

					}
				} else {
					// normal wallet
					if (transfertStatus == CommonConstants.TXN_FAILURE_STATUS) {

						updateWalletCashMaster = new UpdateWalletCashMaster();
						updateWalletCashMaster.setUserId(transferMaster.getUserId());
						updateWalletCashMaster.setGameAmount(
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));
						updateWalletCashMaster.setRewardAmount(0);
						updateWalletCashMaster.setPaytmAmount(0);
						updateWalletCashMaster.setRewardCoins(0);
						updateWalletCashMaster.settType(CommonConstants.CASH_WALLET_CREDIT);
						updateWalletCashMaster.setReferenceNo(transferMaster.getId());

						walletTransactionDao.updateCashWallet(updateWalletCashMaster);
						walletTransactionDao.decreaseGameRedeemedAmount((int) transferMaster.getUserId(),
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));

						// updatating old transaction
						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(transferMaster.getUserId());
						transactionDto.setApp_ver(transferMaster.getVer());
						transactionDto.setAmount(transferMaster.getTransferAmount());
						transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_FAIL_TITLE);
						transactionDto.setTransaction_subtitle(
								CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_PAYTM + "|" + transferMaster.getId());
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setUser_id(transferMaster.getUserId());
							transactionDto.setApp_ver(transferMaster.getVer());
							transactionDto.setAmount((int) transferMaster.getServiceCharge());
							transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_FAIL);
							transactionDto.setTransaction_subtitle(transferMaster.getId());
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
							transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
							transactionDto.setCrtDate(new Date());
							transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getTransferPaytmFailNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTotalAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getTransferPaytmFailNotificMessage());

						rawNotification.setLanding("withdraw_money");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_FAILURE_STATUS);

					} else if (transfertStatus == CommonConstants.TXN_SUCCESS_STATUS) {
						// sending notificatio message

						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_SUCC_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_PAYTM_SERVICE_FEE_SUCC);
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setAdded_date(sdf.format(new Date()));
							transactionsDao.updateCashTransactions(transactionDto);
						}

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getTransferPaytmSuccNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTransferAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getTransferPaytmSuccNotificMessage());

						rawNotification.setLanding("wallet");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_SUCCESS_STATUS);

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public void transferAmountThroughUpi() {
		// TransferMaster transferMaster;
		TransferDao transferDao;

		try {
			transferDao = transferFactory.TransferObjectByType(CommonConstants.TRANSFER_TYPE_UPI);

			/*
			 * transferMaster = transferDao.getInitRecords(); if (transferMaster != null) {
			 * transferDao.transferAmount(transferMaster); }
			 */

			List<TransferMaster> listTransferMaster = transferDao.getListInitRecords();

			for (TransferMaster transferMaster : listTransferMaster) {
				transferDao.transferAmount(transferMaster);
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void transferStatuCheckByCoinTabService() {
		List<TransferMaster> listTransferMaster;
		TransferDao transferDao;
		try {
			transferDao = transferFactory.TransferObjectByType(CommonConstants.TRANSFER_TYPE_UPI);

			listTransferMaster = transferDao.getListPendingRecords();

			for (TransferMaster transferMaster : listTransferMaster) {
				Thread.currentThread().sleep(2 * 1000);
				transferStatusCheckByCoinTab(transferDao, transferMaster);
			}

		} catch (Exception e) {
			logs.error("Exception occured in transferStatuCheckByCoinTabService::" + e.getMessage());
			e.printStackTrace();
		}
	}

	private void transferStatusCheckByCoinTab(final TransferDao transferDao, final TransferMaster transferMaster) {
		// TransferMaster transferMaster;
		Map<String, Object> mapResponse;
		// TransferDao transferDao;
		int transfertStatus;
		UpdateWalletCashMaster updateWalletCashMaster;
		RawNotification rawNotification;
		TransactionDto transactionDto;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			// transferDao =
			// transferFactory.TransferObjectByType(CommonConstants.TRANSFER_TYPE_UPI);

			// transferMaster = transferDao.getPendingRecords();
			if (transferMaster != null) {
				logs.info("CoinTab Api status check Hit started ::" + transferMaster.getId());
				mapResponse = transferDao.getTransferStaus(transferMaster);
				logs.info("CoinTab Api status check Hit completed ::" + transferMaster.getId());

				transfertStatus = transferDao.updateTransferRecord(transferMaster, mapResponse);

				if (transferMaster.getWalletType() != null
						&& transferMaster.getWalletType().equals(CommonConstants.PRO_WALLET)) {
					// pro wallet
					if (transfertStatus == CommonConstants.TXN_FAILURE_STATUS) {

						// refunding the amount to pro wallet
						walletTransactionDao.refundProWalletAmount(transferMaster.getUserId(),
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));

						// updatating old transaction
						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(transferMaster.getUserId());
						transactionDto.setApp_ver(transferMaster.getVer());
						transactionDto.setAmount(transferMaster.getTransferAmount());
						transactionDto.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_FAIL_TITLE);
						transactionDto.setTransaction_subtitle(
								CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_UPI + "|" + transferMaster.getId());
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setUser_id(transferMaster.getUserId());
							transactionDto.setApp_ver(transferMaster.getVer());
							transactionDto.setAmount((int) transferMaster.getServiceCharge());
							transactionDto
									.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_UPI_SERVICE_FEE_FAIL);
							transactionDto.setTransaction_subtitle(transferMaster.getId());
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
							transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
							transactionDto.setCrtDate(new Date());
							transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getProTransferUpiFailNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTotalAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getProTransferUpiFailNotificMessage());

						rawNotification.setLanding("withdraw_money");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_FAILURE_STATUS);

					} else if (transfertStatus == CommonConstants.TXN_SUCCESS_STATUS) {

						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_SUCC_TITLE);
						transactionDto.setTransaction_subtitle(CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_UPI + "|"
								+ transferMaster.getId() + AppCommonUtils.getUpiTransferUtrNumber(mapResponse));
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto = transactionsDao.updateCashTransactions(transactionDto);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto
									.setTransaction_title(CommonConstants.PRO_CASH_WITH_DRAWL_UPI_SERVICE_FEE_SUCC);
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setAdded_date(sdf.format(new Date()));
							transactionDto = transactionsDao.updateCashTransactions(transactionDto);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getProTransferUpiSuccNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTransferAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getProTransferUpiSuccNotificMessage());

						rawNotification.setLanding("wallet");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_SUCCESS_STATUS);

					}

				} else if (transferMaster.getWalletType() != null
						&& transferMaster.getWalletType().equals(CommonConstants.SPORTS_WALLET)) {
					// sports wallet

					if (transfertStatus == CommonConstants.TXN_FAILURE_STATUS) {

						// refunding the amount to pro wallet
						walletTransactionDao.refundSportsWalletAmount(transferMaster.getUserId(),
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));

						// updatating old transaction
						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(transferMaster.getUserId());
						transactionDto.setApp_ver(transferMaster.getVer());
						transactionDto.setAmount(transferMaster.getTransferAmount());
						transactionDto.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_FAIL_TITLE);
						transactionDto.setTransaction_subtitle(
								CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_UPI + "|" + transferMaster.getId());
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setUser_id(transferMaster.getUserId());
							transactionDto.setApp_ver(transferMaster.getVer());
							transactionDto.setAmount((int) transferMaster.getServiceCharge());
							transactionDto
									.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_UPI_SERVICE_FEE_FAIL);
							transactionDto.setTransaction_subtitle(transferMaster.getId());
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
							transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
							transactionDto.setCrtDate(new Date());
							transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getSportsTransferUpiFailNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTotalAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getSportsTransferUpiFailNotificMessage());

						rawNotification.setLanding("withdraw_money");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_FAILURE_STATUS);

					} else if (transfertStatus == CommonConstants.TXN_SUCCESS_STATUS) {

						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_SUCC_TITLE);

						transactionDto.setTransaction_subtitle(CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_UPI + "|"
								+ transferMaster.getId() + AppCommonUtils.getUpiTransferUtrNumber(mapResponse));
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto = transactionsDao.updateCashTransactions(transactionDto);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto
									.setTransaction_title(CommonConstants.SPORTS_CASH_WITH_DRAWL_UPI_SERVICE_FEE_SUCC);
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setAdded_date(sdf.format(new Date()));
							transactionDto = transactionsDao.updateCashTransactions(transactionDto);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getSportsTransferUpiSuccNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTransferAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getSportsTransferUpiSuccNotificMessage());

						rawNotification.setLanding("wallet");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_SUCCESS_STATUS);

					}

				} else {
					// normal wallet
					if (transfertStatus == CommonConstants.TXN_FAILURE_STATUS) {
						updateWalletCashMaster = new UpdateWalletCashMaster();
						updateWalletCashMaster.setUserId(transferMaster.getUserId());
						updateWalletCashMaster.setGameAmount(
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));
						updateWalletCashMaster.setRewardAmount(0);
						updateWalletCashMaster.setPaytmAmount(0);
						updateWalletCashMaster.setRewardCoins(0);
						updateWalletCashMaster.settType(CommonConstants.CASH_WALLET_CREDIT);
						updateWalletCashMaster.setReferenceNo(transferMaster.getId());

						walletTransactionDao.updateCashWallet(updateWalletCashMaster);
						walletTransactionDao.decreaseGameRedeemedAmount((int) transferMaster.getUserId(),
								(int) (transferMaster.getTransferAmount() + transferMaster.getServiceCharge()));

						// updatating old transaction
						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_TITLE);
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionsDao.updateCashTransactions(transactionDto);

						transactionDto = new TransactionDto();
						transactionDto.setUser_id(transferMaster.getUserId());
						transactionDto.setApp_ver(transferMaster.getVer());
						transactionDto.setAmount(transferMaster.getTransferAmount());
						transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_FAIL_TITLE);
						transactionDto.setTransaction_subtitle(
								CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_UPI + "|" + transferMaster.getId());
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
						transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
						transactionDto.setCrtDate(new Date());
						transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setUser_id(transferMaster.getUserId());
							transactionDto.setApp_ver(transferMaster.getVer());
							transactionDto.setAmount((int) transferMaster.getServiceCharge());
							transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_UPI_SERVICE_FEE_FAIL);
							transactionDto.setTransaction_subtitle(transferMaster.getId());
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setTransaction_type(CommonConstants.CREDIT_EVENT);
							transactionDto.setEvent_type(CommonConstants.EVENT_TYPE_WITHDRAW_MONEY);
							transactionDto.setCrtDate(new Date());
							transactionsDao.insertTransactions(transactionDto, CommonConstants.CASH_TRANSACTIONS);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getTransferUpiFailNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTotalAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getTransferUpiFailNotificMessage());

						rawNotification.setLanding("withdraw_money");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_FAILURE_STATUS);

					} else if (transfertStatus == CommonConstants.TXN_SUCCESS_STATUS) {

						transactionDto = new TransactionDto();
						transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_SUCC_TITLE);

						transactionDto.setTransaction_subtitle(CommonConstants.CASH_WITH_DRAWL_SUB_TITLE_UPI + "|"
								+ transferMaster.getId() + AppCommonUtils.getUpiTransferUtrNumber(mapResponse));
						transactionDto.setReferenceNo(transferMaster.getId());
						transactionDto.setAdded_date(sdf.format(new Date()));
						transactionDto = transactionsDao.updateCashTransactions(transactionDto);

						if (transferMaster.getServiceCharge() > 0) {

							transactionDto = new TransactionDto();
							transactionDto.setTransaction_title(CommonConstants.CASH_WITH_DRAWL_UPI_SERVICE_FEE_SUCC);
							transactionDto.setReferenceNo(transferMaster.getId() + "_1");
							transactionDto.setAdded_date(sdf.format(new Date()));
							transactionDto = transactionsDao.updateCashTransactions(transactionDto);

						}

						// sending notificatio message

						rawNotification = new RawNotification();
						rawNotification.setUserId(transferMaster.getUserId());

						rawNotification.setTitle(transferAmountProperties.getTransferUpiSuccNotificTitle()
								.replace("~TRANSFER_AMOUNT~", (int) transferMaster.getTransferAmount() + ""));
						rawNotification.setBody(transferAmountProperties.getTransferUpiSuccNotificMessage());

						rawNotification.setLanding("wallet");
						rawNotification.setIcon("");

						rawNotification.setGame_id(0);
						rawNotification.setN_type(11);
						rawNotification.setTid(0);
						rawNotification.setTtl(28800);
						mongoCommonDao.saveNotificationData(rawNotification,
								CommonConstants.NOTIFICATION_COMMON_COLLECTION);

						// update user balance track master
						walletTransactionDao.updateUserBalanceTrackMaster(transferMaster.getUserId(),
								transferMaster.getId(), transferMaster.getWalletType(),
								CommonConstants.TXN_SUCCESS_STATUS);

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
