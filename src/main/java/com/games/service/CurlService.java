package com.games.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games.properties.TransferAmountProperties;

@Service
public class CurlService {
	private static final Logger LOGS = LoggerFactory.getLogger(CurlService.class);

	@Autowired
	TransferAmountProperties transferAmountProperties;


	public Map<String, Object> coingTabApiController(String requestUrl, JSONObject jsonObejct) throws Exception {
		ProcessBuilder pb = null;
		String uuid;
		ObjectMapper mapper;
		Map<String, Object> mapResponse;

		try {
			LOGS.info("request url: " + requestUrl);
			LOGS.info("requestbody: " + jsonObejct);
			
			mapper = new ObjectMapper();

			uuid = UUID.randomUUID().toString().toString().replaceAll("-", "");

			pb = new ProcessBuilder("curl", "-X", "POST", "-H", "content-type: application/json", "-H",
					"api_key: " + transferAmountProperties.getCoinTabApiKey(), "-H", "api_uuid: " + uuid, "-d",
					jsonObejct.toString(), requestUrl);

			pb.redirectErrorStream(false);
			Process p = pb.start();

			InputStream is = p.getInputStream();

			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			StringBuilder out = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
				out.append(System.getProperty("line.separator"));
			}

			reader.close();

			mapResponse = mapper.readValue(out.toString(), new TypeReference<Map<String, Object>>() {
			});

			
			LOGS.info("response:: " + mapResponse);

			return mapResponse;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pb = null;
		}

		return null;

	}

}
