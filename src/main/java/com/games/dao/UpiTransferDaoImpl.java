package com.games.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import com.games.model.TransferMaster;
import com.games.properties.TransferAmountProperties;
import com.games.service.CurlService;
import com.games.util.CommonConstants;
import com.mongodb.client.result.UpdateResult;

@Repository
public class UpiTransferDaoImpl implements TransferDao {
	private static final Logger logs = LoggerFactory.getLogger(UpiTransferDaoImpl.class);

	@Autowired
	TransferAmountProperties transferAmountProperties;

	@Autowired
	CurlService curlService;

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Override
	public TransferMaster getInitRecords() {
		Query query;
		List<TransferMaster> listMaster;
		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_UPI),
						Criteria.where("transferStatus").is(CommonConstants.TXN_INIT_STATUS)));
		query.limit(1);

		listMaster = mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

		if (listMaster.size() > 0) {
			return listMaster.get(0);
		}

		return null;

	}

	@Override
	public TransferMaster getPendingRecords() {
		Query query;
		int count;
		List<TransferMaster> listMaster;
		Random random = new Random();

		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_UPI),
						Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS)));

		count = (int) mongoTemplate.count(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

		if (count > 0) {

			query = new Query();
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_UPI),
							Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS)));
			query.limit(1);
			query.skip(new Long(random.nextInt(count)));

			listMaster = mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

			if (listMaster.size() > 0) {
				return listMaster.get(0);
			}

		}

		return null;
	}

	@Override
	public void transferAmount(TransferMaster transferMaster) {
		JSONObject jsonObejct;
		Query query;
		Update update;
		UpdateResult updateResult;

		try {

			jsonObejct = new JSONObject();
			jsonObejct.put("transaction_id", transferMaster.getId());
			jsonObejct.put("beneficiary_name", "");
			if (transferMaster.getUpiName() != null) {
				jsonObejct.put("beneficiary_name", transferMaster.getUpiName());
				if (transferMaster.getUpiName().length() > 40) {
					jsonObejct.put("beneficiary_name", transferMaster.getUpiName().substring(0, 39));
				}

			}

			jsonObejct.put("bhim_upi_id", transferMaster.getUpiId());
			jsonObejct.put("account_number", "");
			jsonObejct.put("account_ifsc", "");
			jsonObejct.put("payment_channel", transferAmountProperties.getCoinTabPaymentChannel());
			jsonObejct.put("amount", transferMaster.getTransferAmount() + "");

			logs.info("CoinTab Api Transfer Hit started ::" + transferMaster.getId());

			curlService.coingTabApiController(transferAmountProperties.getCoinTabApiBaseUrl()
					+ transferAmountProperties.getCoinTabInitiatePaymentUri(), jsonObejct);
			logs.info("CoinTab Api Transfer Hit completed ::" + transferMaster.getId());

			// updating the record first and send hit to the gateway
			query = new Query(Criteria.where("_id").is(transferMaster.getId()));
			update = new Update().set("transferStatus", CommonConstants.TXN_PENDING_STATUS);
			updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public int updateTransferRecord(TransferMaster transferMaster, Map<String, Object> mapResponse) {
		Query query = new Query(Criteria.where("_id").is(transferMaster.getId()));
		Update update;
		UpdateResult updateResult;
		Calendar calender;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		calender = Calendar.getInstance();

		update = new Update();
		update.set("modifiedDate", calender.getTime());
		update.set("modifiedGroupDate", sdf.format(calender.getTime()));

		if (mapResponse != null && mapResponse.get("status") != null) {

			switch (mapResponse.get("status").toString()) {
			// Pending
			case "0":

				update.set("response", mapResponse);
				updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);

				break;
			// Success
			case "1":
				update.set("transferStatus", CommonConstants.TXN_SUCCESS_STATUS).set("response", mapResponse);
				updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);
				if (updateResult.getMatchedCount() > 0) {
					return CommonConstants.TXN_SUCCESS_STATUS;
				}
				break;

			// api Failure due to some other reason
			case "2":

				if (mapResponse.containsKey("error_code")
						&& mapResponse.get("error_code").toString().equals("NOT_FOUND")) {
					update.set("transferStatus", CommonConstants.TXN_FAILURE_STATUS).set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);
					if (updateResult.getMatchedCount() > 0) {
						return CommonConstants.TXN_FAILURE_STATUS;
					}

				} else {
					update.set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);
				}
				break;

			case "3":
				update.set("transferStatus", CommonConstants.TXN_FAILURE_STATUS).set("response", mapResponse);
				updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);
				if (updateResult.getMatchedCount() > 0) {
					return CommonConstants.TXN_FAILURE_STATUS;
				}
				break;

			default:
				System.out.println("not configured any thing coing tab:: " + mapResponse);
				break;
			}

		}

		return CommonConstants.TXN_PENDING_STATUS;

	}

	@Override
	public Map<String, Object> getTransferStaus(TransferMaster transferMaster) {
		JSONObject jsonObejct;
		try {
			jsonObejct = new JSONObject();
			jsonObejct.put("transaction_id", transferMaster.getId());
			return curlService.coingTabApiController(transferAmountProperties.getCoinTabApiBaseUrl()
					+ transferAmountProperties.getCoinTabApicheckPaymentStatusUri(), jsonObejct);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public List<TransferMaster> getListInitRecords() {
		Query query;
		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_UPI),
						Criteria.where("transferStatus").is(CommonConstants.TXN_INIT_STATUS)));
		// query.limit(CommonConstants.TRANSACTION_HIT_COUNT);
		return mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

	}

	@Override
	public List<TransferMaster> getListPendingRecords() {
		Query query;
		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_UPI),
						Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS)));
		// query.limit(CommonConstants.TRANSACTION_HIT_COUNT);

		return mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

	}

}
