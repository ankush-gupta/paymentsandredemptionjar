package com.games.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.games.model.PaymentMaster;
import com.games.model.TransactionProcessMaster;
import com.games.model.WalletCashOfferMaster;
import com.games.util.CommonConstants;
import com.games.util.QueryConstants;

@Repository
public class PaymentOrderDaoImpl implements PaymentOrderDao {
	private static final Logger logs = LoggerFactory.getLogger(PaymentOrderDaoImpl.class);
	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Autowired
	@Qualifier("primaryDB")
	private JdbcTemplate jdbcTemplate;

	@Override
	public void updatePaymentOrder(String _id, int depositWalletcashAmount, String dealName, int dealAmount,
			int dealCoins) {
		Query query = new Query(Criteria.where("_id").is(_id));
		Update update;

		update = new Update();
		update.set("depositWalletCash", depositWalletcashAmount);
		update.set("dealName", dealName);
		update.set("dealAmount", dealAmount);
		update.set("dealCoins", dealCoins);
		mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);

	}

	@Override
	public void updatePaymentOrderWithRewardModeDetails(PaymentMaster paymentMaster) {
		Query query = new Query(Criteria.where("_id").is(paymentMaster.getId()));
		Update update;

		update = new Update();
		update.set("depositWalletCash", paymentMaster.getDepositWalletCash());
		update.set("rewardWalletCash", paymentMaster.getRewardWalletCash());
		update.set("rewardCoins", paymentMaster.getRewardCoins());
		update.set("cashback_status", paymentMaster.getCashback_status());

		mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);

	}

	@Override
	public boolean checkUserDealPaymentSuccess(String _id, long userId, String dealName) {

		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(Criteria.where("userId").is(userId),
				Criteria.where("paymentStatus").is(CommonConstants.TXN_SUCCESS_STATUS),
				Criteria.where("dealName").is(dealName), Criteria.where("_id").ne(_id)));
		return mongoTemplate.count(query, CommonConstants.PAYMENT_COLLECTION) > 0;

	}

	@Override
	public int getUserSuccPaymentOrderCount(long user_id) {
		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(Criteria.where("userId").is(user_id),
				Criteria.where("paymentStatus").is(CommonConstants.TXN_SUCCESS_STATUS)));

		return (int) mongoTemplate.count(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);
	}

	@Override
	public PaymentMaster getPaymentMasterByOrderId(String orderId) {
		// TODO Auto-generated method stub
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(orderId));
		return mongoTemplate.findOne(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);

	}

	@Override
	public boolean getTransactionProcessingState(String orderId) {
		TransactionProcessMaster transactionProcessMaster;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(orderId));
			long count = mongoTemplate.count(query, TransactionProcessMaster.class, "transactionProcessMaster");
			if (count > 0) {
				return false;
			} else {
				transactionProcessMaster = new TransactionProcessMaster();
				transactionProcessMaster.setId(orderId);
				transactionProcessMaster.setCreatedDate(new Date());
				mongoTemplate.insert(transactionProcessMaster, "transactionProcessMaster");
				return true;

			}

		} catch (Exception e) {
			e.printStackTrace();
			logs.error("#######duplicate record found error########" + e.getMessage());
		}

		return false;
	}

	@Override
	public WalletCashOfferMaster getWalletCashBackOfferByAmountAndMode(int planAmount, String userModeKeyWord) {

		try {
			String rewardMode = getPaymentModeByKeyWord(userModeKeyWord);

			return jdbcTemplate.query(QueryConstants.CASHBACK_OFFER_BY_PLAN_AMOUNT_REWARD_MODE,
					new Object[] { planAmount, "%_" + rewardMode + "_%", "%_ALL_%" },
					new ResultSetExtractor<WalletCashOfferMaster>() {

						@Override
						public WalletCashOfferMaster extractData(ResultSet rs)
								throws SQLException, DataAccessException {
							WalletCashOfferMaster walletCashOfferMaster;
							if (rs.next()) {
								walletCashOfferMaster = new WalletCashOfferMaster();
								walletCashOfferMaster.setOfferId(rs.getInt("id"));
								walletCashOfferMaster.setPlanAmount(rs.getInt("plan_amount"));
								walletCashOfferMaster.setDepositWalletCash(rs.getInt("deposit_wallet_cash"));
								walletCashOfferMaster.setRewardWalletCash(rs.getInt("reward_wallet_cash"));
								walletCashOfferMaster.setRewardCoins(rs.getInt("reward_coins"));
								return walletCashOfferMaster;
							}
							return null;
						}

					});

		} catch (Exception e) {
			e.printStackTrace();
			logs.error("Exception in getWalletCashBackOfferByAmountAndMode::" + e.getMessage());
		}

		return null;
	}

	private String getPaymentModeByKeyWord(String userModeKeyWord) {

		try {
			return jdbcTemplate.query(QueryConstants.GET_PAYMENT_MODE_BY_KEY_WORD, new Object[] { userModeKeyWord },
					new ResultSetExtractor<String>() {

						@Override
						public String extractData(ResultSet rs) throws SQLException, DataAccessException {
							if (rs.next()) {
								return rs.getString("payment_mode");
							}
							return "ALL";
						}

					});

		} catch (Exception e) {
			e.printStackTrace();
			logs.error("Exception in getPaymentModeByKeyWord::" + e.getMessage());
		}
		return "ALL";

	}

	@Override
	public boolean checkUserGotCashBackAmount_N_Previous_days(long userId, int dayInterval) {
		Query query;
		SimpleDateFormat sdf;
		Calendar calendar;
		try {
			sdf = new SimpleDateFormat("yyyy-MM-dd");
			calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, -dayInterval);

			query = new Query();
			query.addCriteria(Criteria.where("userId").is(userId).and("groupDate").gte(sdf.format(calendar.getTime()))
					.and("paymentStatus").is(CommonConstants.TXN_SUCCESS_STATUS).and("cashback_status").is(1));

			logs.info("checkUserGotCashBackAmount_N_Previous_days ::" + query);
			return mongoTemplate.count(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION) > 0;

		} catch (Exception e) {
			logs.error("Exception in checkUserGotCashBackAmount_N_Previous_days::" + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void maintainUserTotalAddMoney(long userId, int planAmount) {
		SimpleDateFormat sdf;
		Update update;
		Query query;
		try {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			query = new Query();
			query.addCriteria(Criteria.where("userId").is(userId));

			update = new Update();
			update.inc("total_plan_amount", planAmount);
			update.set("modified_date", sdf.format(new Date()));
			mongoTemplate.upsert(query, update, CommonConstants.USER_TOTAL_ADD_MONEY_COLLECTION);
		} catch (Exception e) {
			logs.error("Exception in maintainUserTotalAddMoney::" + e.getMessage());
		}

	}

}
