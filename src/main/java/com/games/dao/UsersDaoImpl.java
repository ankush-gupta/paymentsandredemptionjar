package com.games.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.games.model.FirstGameDto;
import com.games.model.ProUserHistoryMaster;
import com.games.model.ProUserMaster;
import com.games.model.UserMaster;
import com.games.util.CommonConstants;
import com.games.util.QueryConstants;

@Repository
public class UsersDaoImpl implements UsersDao {

	private static final Logger LOGS = LoggerFactory.getLogger(UsersDaoImpl.class);

	@Autowired
	@Qualifier("primaryDB")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Override
	public String getUserFcmToken(long userId) {

		return jdbcTemplate.query("SELECT channel_key FROM user_token WHERE user_id = ?", new Object[] { userId },
				new ResultSetExtractor<String>() {

					@Override
					public String extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getString("channel_key");
						}
						return "";

					}
				});

	}

	@Override
	public void savePreviousMaxOfferAmount(int userId, int offerAmount) {

		int previousAmount;
		try {
			previousAmount = jdbcTemplate.queryForObject(
					"SELECT prvs_max_offer_amount  FROM users where user_id=? limit 1", new Object[] { userId },
					Integer.class);
			if (previousAmount < offerAmount) {
				jdbcTemplate.update("update users set prvs_max_offer_amount=? where user_id=?",
						new Object[] { offerAmount, userId });
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGS.info("error ::" + e);
		}

	}

	@Override
	public void saveProUserDetails(ProUserHistoryMaster proUserHistoryMaster) {

		try {
			Calendar calendar;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			Query query;
			Update update;
			query = new Query();
			query.addCriteria(Criteria.where("userId").is(proUserHistoryMaster.getUserId()));
			update = new Update();
			update.set("added_date", proUserHistoryMaster.getAdded_date());
			update.set("activeDays", proUserHistoryMaster.getActiveDays());

			calendar = Calendar.getInstance();
			calendar.setTime(sdf.parse(proUserHistoryMaster.getAdded_date()));
			calendar.add(Calendar.DATE, proUserHistoryMaster.getActiveDays() - 1);

			update.set("record_expire_date", calendar.getTime());

			LOGS.info("query::" + query);
			LOGS.info("update::" + update);

			mongoTemplate.upsert(query, update, ProUserMaster.class, CommonConstants.PRO_USER_MASTER_COLLECTION);

			// saving history
			mongoTemplate.insert(proUserHistoryMaster, CommonConstants.PRO_USER_HISTORY_COLLECTION);

		} catch (Exception e) {
			LOGS.error("Error in saveProUserDetails: " + e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	public UserMaster getUserDetailsByUserId(long user_id) {
		try {
			return jdbcTemplate.queryForObject(QueryConstants.GET_USER_DETAILS_BY_USER_ID, new Object[] { user_id },
					new BeanPropertyRowMapper<UserMaster>(UserMaster.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/*
	 * @Override public void updateAllUserRecords() {
	 * 
	 * Query query; List<PaymentMaster> listMaster; ProUserHistoryMaster
	 * proUserHistoryMaster; Calendar calender;
	 * 
	 * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); try { query = new
	 * Query(); query.addCriteria( new
	 * Criteria().andOperator(Criteria.where("paymentStatus").is(CommonConstants.
	 * TXN_SUCCESS_STATUS))); listMaster = mongoTemplate.find(query,
	 * PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);
	 * 
	 * for (PaymentMaster paymentMaster : listMaster) {
	 * 
	 * System.out.println("======paymentMaster==========="+paymentMaster.getId());
	 * 
	 * if (paymentMaster.getPlanAmount() >= 10) { calender = Calendar.getInstance();
	 * proUserHistoryMaster = new ProUserHistoryMaster();
	 * proUserHistoryMaster.setUserId(paymentMaster.getUserId());
	 * proUserHistoryMaster.setAdded_date(sdf.format(paymentMaster.getCreatedDate())
	 * ); proUserHistoryMaster.setCreatedDate(calender.getTime());
	 * proUserHistoryMaster.setCrtLongDate(calender.getTimeInMillis());
	 * proUserHistoryMaster.setPlanAmount(paymentMaster.getPlanAmount());
	 * proUserHistoryMaster.setReferenceNo(paymentMaster.getId());
	 * proUserHistoryMaster.setTtype("ADD_MONEY");
	 * proUserHistoryMaster.setActiveDays(30);
	 * 
	 * saveProUserDetails(proUserHistoryMaster); }
	 * 
	 * }
	 * 
	 * System.out.println("========================**********====================");
	 * System.out.println("updated all records");
	 * System.out.println("========================**********====================");
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } }
	 * 
	 */

	@Override
	public void verifyAndSendReferalSuccessEvent(long uid, String source, int reward) {
		// check user is new user
		if (check_user_exists_in_new_users(uid)) {
			// check event_sent or not
			if (!checkReferalEventExists(uid)) {
				saveFirstGameStatus(uid, source, reward);
			}
		}

	}

	boolean checkReferalEventExists(long uid) {
		Query query = new Query(Criteria.where("uid").is(uid));
		return mongoTemplate.count(query, CommonConstants.FIRST_GAME_COLLECTION) > 0;
	}

	boolean check_user_exists_in_new_users(long uid) {
		Query query = new Query(Criteria.where("userId").is(uid));
		return mongoTemplate.count(query, CommonConstants.NEW_USER_COLLECTION) > 0;
	}

	private int saveFirstGameStatus(long uid, String source, int reward) {
		try {
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(CommonConstants.DATE_TIME_FORMAT);
			FirstGameDto firstGameDto = new FirstGameDto();
			firstGameDto.setUid(uid);
			firstGameDto.setStatus(1);
			firstGameDto.setSource(source);
			firstGameDto.setReward(reward);
			firstGameDto.setAdded_date(LocalDate.now().toString());
			firstGameDto.setCrt_date(dateTimeFormatter.format(LocalDateTime.now()));
			firstGameDto.setExpireAt(new Date());
			mongoTemplate.insert(firstGameDto, CommonConstants.FIRST_GAME_COLLECTION);
			return 1;
		} catch (Exception e) {
			LOGS.error("Error in First Game Event for: {}", uid);
			LOGS.error("Exception: {}", e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}

}
