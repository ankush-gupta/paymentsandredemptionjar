package com.games.dao;

import java.util.List;
import java.util.Map;

import com.games.model.PaymentMaster;

public interface PaymentDao {

	public PaymentMaster getPendingPayments();

	public List<PaymentMaster> getListPendingPayments(boolean statusReVerfication, String date_type);

	public Map<String, Object> checkPaymentStaus(String orderId);

	public int updatePaymentRecord(PaymentMaster paymentMaster, Map<String, Object> mapResponse);

	public List<PaymentMaster> getWebHookPayments();

	public String getUserChoosenPaymentMode(Map<String, Object> mapResponse);

	default public void checkPaymentFraudStatusCodeAndTrackInfo(long userId, Map<String, Object> mapResponse,List<String> fraudErrorCodes) {
		return;
	}

}
