package com.games.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.games.model.SingularEventMaster;
import com.games.util.AppCommonUtils;
import com.games.util.CommonConstants;

@Repository
public class SingularDaoImpl implements SingularDao {
	private static final Logger LOGS = LoggerFactory.getLogger(SingularDaoImpl.class);

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Override
	public void updateSingularEventsStatus(long userId, String eventName, int amount) {

		SingularEventMaster singularEventMaster;
		Query query;
		Update update;

		try {
			singularEventMaster = getSingularEventMasterByUserId(userId);

			if (singularEventMaster != null) {
				query = new Query(Criteria.where("_id").is(singularEventMaster.getId()));
				update = new Update();

				switch (eventName) {
				case "NewReg":
					if (singularEventMaster.getIs_new() == 0) {
						update.set("is_new", 1);
						update.set("otpVerifyDate", AppCommonUtils.getCurrentTime());
						update.set("is_new_status", 0);

						mongoTemplate.updateFirst(query, update, SingularEventMaster.class,
								CommonConstants.SINGULAR_EVENTS_COLLECTION);
					}

					break;
				case "Exist_Reg":

					if (singularEventMaster.getIs_new() == 0) {
						update.set("is_new", 2);
						update.set("otpVerifyDate", AppCommonUtils.getCurrentTime());
						update.set("is_new_status", 0);

						mongoTemplate.updateFirst(query, update, SingularEventMaster.class,
								CommonConstants.SINGULAR_EVENTS_COLLECTION);
					}
					break;
				case "CashPlay":

					if (singularEventMaster.getCagEvent() == 0 && singularEventMaster.getIs_new() == 1) {
						update.set("cagEvent", 1);
						update.set("cagCount", 1);
						update.set("addCagTime", AppCommonUtils.getCurrentTime());
						update.set("cagStatus", 0);
					} else {
						update.inc("cagCount", 1);
					}

					mongoTemplate.updateFirst(query, update, SingularEventMaster.class,
							CommonConstants.SINGULAR_EVENTS_COLLECTION);

					break;
				case "CoinPlay":

					if (singularEventMaster.getConEvent() == 0 && singularEventMaster.getIs_new() == 1) {
						update.set("conEvent", 1);
						update.set("conCount", 1);
						update.set("addConTime", AppCommonUtils.getCurrentTime());
						update.set("conStatus", 0);
					} else {
						update.inc("conCount", 1);
					}

					mongoTemplate.updateFirst(query, update, SingularEventMaster.class,
							CommonConstants.SINGULAR_EVENTS_COLLECTION);

					break;
				case "AddMo_New_Mid":

					if (singularEventMaster.getAddMid() == 0 && singularEventMaster.getIs_new() == 1) {
						update.set("addMid", 1);
						update.set("addMidTime", AppCommonUtils.getCurrentTime());
						update.set("addMidAmt", amount);
						update.set("addMidStatus", 0);

						if (singularEventMaster.getAddAll() == 0) {
							update.set("addAll", 1);
							update.set("addAllTime", AppCommonUtils.getCurrentTime());
							update.set("addAllStatus", 0);
						}

						mongoTemplate.updateFirst(query, update, SingularEventMaster.class,
								CommonConstants.SINGULAR_EVENTS_COLLECTION);

					}

					break;
				case "AddMo_New_Low":

					if (singularEventMaster.getAddLow() == 0 && singularEventMaster.getIs_new() == 1) {
						update.set("addLow", 1);
						update.set("addLowTime", AppCommonUtils.getCurrentTime());
						update.set("addLowAmt", amount);
						update.set("addLowStatus", 0);

						if (singularEventMaster.getAddAll() == 0) {
							update.set("addAll", 1);
							update.set("addAllTime", AppCommonUtils.getCurrentTime());
							update.set("addAllStatus", 0);
						}

						mongoTemplate.updateFirst(query, update, SingularEventMaster.class,
								CommonConstants.SINGULAR_EVENTS_COLLECTION);
					}

					break;

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGS.error(e.getMessage());
		}

	}

	SingularEventMaster getSingularEventMasterByUserId(long userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		query.limit(1);

		return mongoTemplate.findOne(query, SingularEventMaster.class, CommonConstants.SINGULAR_EVENTS_COLLECTION);

	}

}
