package com.games.dao;

import com.games.model.RawNotification;

public interface MongoCommonDao {
	
	public int saveNotificationData(RawNotification rawNotification, String mongo_collection);

}
