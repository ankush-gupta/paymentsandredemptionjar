package com.games.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.games.util.CommonConstants;

@Component
public class TransferFactoryImple implements TransferFactory {
	@Autowired
	@Qualifier(value = "paytmTransferDaoImpl")
	TransferDao paytmTransferDaoImpl;

	@Autowired
	@Qualifier(value = "upiTransferDaoImpl")
	TransferDao upiTransferDaoImpl;

	@Override
	public TransferDao TransferObjectByType(String transferType) {
		switch (transferType) {
		case CommonConstants.TRANSFER_TYPE_PAYTM:
			return paytmTransferDaoImpl;

		case CommonConstants.TRANSFER_TYPE_UPI:
			return upiTransferDaoImpl;

		}
		return null;
	}

}
