package com.games.dao;

import com.games.model.ProUserHistoryMaster;
import com.games.model.UserMaster;

public interface UsersDao {
	public String getUserFcmToken(long userId);

	public void savePreviousMaxOfferAmount(int userId, int offerAmount);
	
	public void saveProUserDetails(ProUserHistoryMaster proUserHistoryMaster);
	
	 public UserMaster getUserDetailsByUserId(long user_id);
	 
	 void verifyAndSendReferalSuccessEvent(long uid,String source,int reward);
	
	

}
