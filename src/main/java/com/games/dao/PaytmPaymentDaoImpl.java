package com.games.dao;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games.model.PaymentMaster;
import com.games.properties.PaymentGateWayProperties;
import com.games.util.AppCommonUtils;
import com.games.util.CommonConstants;
import com.mongodb.client.result.UpdateResult;

@Repository
public class PaytmPaymentDaoImpl implements PaymentDao {
	private static final Logger logs = LoggerFactory.getLogger(PaytmPaymentDaoImpl.class);

	@Autowired
	PaymentGateWayProperties paymentGateWayProperties;

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Autowired
	@Qualifier("trackRestTemplate")
	RestTemplate trackRestTemplate;

	@Override
	public PaymentMaster getPendingPayments() {

		int count;
		Query query;
		List<PaymentMaster> listMaster;
		Random random = new Random();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());

		try {

			query = new Query();
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_PAYTM),
							Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));

			count = (int) mongoTemplate.count(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);

			if (count > 0) {

				query = new Query();
				query.addCriteria(
						new Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_PAYTM),
								Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
								Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));

				query.limit(1);
				query.skip(new Long(random.nextInt(count)));

				listMaster = mongoTemplate.find(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);

				if (listMaster.size() > 0) {
					return listMaster.get(0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public Map<String, Object> checkPaymentStaus(String orderId) {

		Map<String, Object> map;
		TreeMap<String, String> paytmParams;
		JSONObject requestJson;
		String response;
		HttpHeaders headers;
		HttpEntity<String> requestEntity;
		ObjectMapper mapper;

		try {
			logs.info("PaytmPayment status api....paymentId:: " + orderId);

			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			paytmParams = new TreeMap<String, String>();
			paytmParams.put("MID", paymentGateWayProperties.getPaytmMerchantId());
			paytmParams.put("ORDERID", orderId);

			requestJson = new JSONObject(paytmParams);
			requestJson.put("CHECKSUMHASH",
					AppCommonUtils.getPaytmCheckSum(paymentGateWayProperties.getPaytmMerchantKey(), paytmParams));

			// requestEntity = new HttpEntity<String>("JsonData=" + requestJson.toString(),
			// headers);
			requestEntity = new HttpEntity<String>(requestJson.toString(), headers);

			logs.info("url:: " + paymentGateWayProperties.getPaytmApiBaseUrl()
					+ paymentGateWayProperties.getPaytmApiOrderStatus());
			logs.info("body:: " + requestJson);
			logs.info("header:: " + headers);

			response = trackRestTemplate.postForObject(
					paymentGateWayProperties.getPaytmApiBaseUrl() + paymentGateWayProperties.getPaytmApiOrderStatus(),
					requestEntity, String.class);

			logs.info("PaytmPayment response: " + response);

			mapper = new ObjectMapper();
			return mapper.readValue(response, new TypeReference<Map<String, Object>>() {
			});
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}

	@Override
	public int updatePaymentRecord(PaymentMaster paymentMaster, Map<String, Object> mapResponse) {

		logs.info("PaytmPayment updating payment record.... paymentId::" + paymentMaster.getId());

		Query query = new Query(Criteria.where("_id").is(paymentMaster.getId()));
		Update update;
		UpdateResult updateResult;
		int responseCode;

		Calendar calendar = Calendar.getInstance();
		update = new Update();
		update.set("modifiedDate", calendar.getTime());

		if (mapResponse != null && mapResponse.get("STATUS") != null) {
			switch (mapResponse.get("STATUS").toString()) {

			case "TXN_SUCCESS":

				responseCode = Integer.parseInt(mapResponse.get("RESPCODE").toString());
				switch (responseCode) {

				case 1:
					update.set("paymentStatus", CommonConstants.TXN_SUCCESS_STATUS).set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
					// add amount to his wallet
					if (updateResult.getMatchedCount() > 0) {
						return CommonConstants.TXN_SUCCESS_STATUS;
					}
					break;

				default:
					logs.info("Error not configured paytm:: " + mapResponse);
					break;
				}

				break;

			case "PENDING":
				// in process , wait for success or failure
				update.set("response", mapResponse);
				mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);

				break;

			case "TXN_FAILURE":

				/*
				 * 402 processing your request
				 */

				responseCode = Integer.parseInt(mapResponse.get("RESPCODE").toString());
				switch (responseCode) {

				case 334:
					// only for invalid order id
					// long diffMillsec = paymentMaster.getModifiedDate().getTime()-
					// paymentMaster.getCreatedDate().getTime();

					long diffMillsec = calendar.getTimeInMillis() - paymentMaster.getCrtLongDate();

					long min = diffMillsec / (60 * 1000);
					if (min >= 60) {
						logs.info(paymentMaster.getId() + "::id::stautus:302   currenttime::{},created time::{}",
								calendar.getTimeInMillis(), paymentMaster.getCrtLongDate());
						update.set("paymentStatus", CommonConstants.TXN_FAILURE_STATUS).set("response", mapResponse);
						updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
						if (updateResult.getMatchedCount() > 0) {
							return CommonConstants.TXN_FAILURE_STATUS;
						}

					} else {
						update.set("response", mapResponse);
						mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
					}
					break;
				case 235:
				case 316:
				case 202:
				case 227:
				case 372:
				case 401:
				case 232:
				case 229:
				case 156:
				case 196:
				case 153:
				case 158:
				case 159:
				case 161:
				case 295:
				case 501:
				case 810:
				case 832:
					update.set("paymentStatus", CommonConstants.TXN_FAILURE_STATUS).set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
					if (updateResult.getMatchedCount() > 0) {
						return CommonConstants.TXN_FAILURE_STATUS;
					}
					break;
				default:
					logs.info("Error not configured paytm:: " + mapResponse);
					update.set("response", mapResponse);
					mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
					break;

				}

				break;

			default:
				logs.info("Error not configured paytm:: " + mapResponse);
				break;

			}

		}

		return CommonConstants.TXN_PENDING_STATUS;
	}

	/*
	 * @Override public List<PaymentMaster> getListPendingPayments() {
	 * 
	 * int count; Query query; List<PaymentMaster> listMaster; Random random = new
	 * Random(); Calendar calendar = Calendar.getInstance();
	 * calendar.add(Calendar.MINUTE,
	 * -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());
	 * 
	 * try {
	 * 
	 * query = new Query(); query.addCriteria( new
	 * Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.
	 * PAYMENT_TYPE_PAYTM),
	 * Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
	 * Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));
	 * 
	 * count = (int) mongoTemplate.count(query, PaymentMaster.class,
	 * CommonConstants.PAYMENT_COLLECTION);
	 * 
	 * if (count > 0) {
	 * 
	 * query = new Query(); query.addCriteria( new
	 * Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.
	 * PAYMENT_TYPE_PAYTM),
	 * Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
	 * Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));
	 * 
	 * query.limit(CommonConstants.TRANSACTION_HIT_COUNT); query.skip(new
	 * Long(random.nextInt(count)));
	 * 
	 * listMaster = mongoTemplate.find(query, PaymentMaster.class,
	 * CommonConstants.PAYMENT_COLLECTION);
	 * 
	 * return listMaster; } } catch (Exception e) { e.printStackTrace(); } return
	 * new ArrayList<>();
	 * 
	 * }
	 */
	@Override
	public List<PaymentMaster> getListPendingPayments(boolean statusReVerfication, String date_type) {

		int count;
		Query query;
		List<PaymentMaster> listMaster;
		Random random = new Random();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());

		query = new Query();

		if (date_type.equals(CommonConstants.CURRENT_DAY_RECORDS)) {
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_PAYTM),
							Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("crtLongDate").lte(calendar.getTimeInMillis()),
							Criteria.where("response").exists(statusReVerfication),
							Criteria.where("groupDate").is(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))));

		} else if (date_type.equals(CommonConstants.PREVIOUS_DAY_RECORDS)) {
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_PAYTM),
							Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("crtLongDate").lte(calendar.getTimeInMillis()),
							Criteria.where("response").exists(statusReVerfication),
							Criteria.where("groupDate").lt(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))));

		} else {
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_PAYTM),
							Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("crtLongDate").lte(calendar.getTimeInMillis()),
							Criteria.where("response").exists(statusReVerfication)));

		}

		// query.limit(CommonConstants.TRANSACTION_HIT_COUNT);

		return mongoTemplate.find(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);

	}

	@Override
	public List<PaymentMaster> getWebHookPayments() {
		Query query;
		List<String> listOrderIds;

		Update update;
		UpdateResult updateResult;
		int responseCode;

		Calendar calendar = Calendar.getInstance();
		update = new Update();
		update.set("modifiedDate", calendar.getTime());

		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0");

		try {

			// calendar.add(Calendar.MINUTE,
			// -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());

			listOrderIds = new ArrayList<>();

			query = new Query();
			// need to add this condition 6 mint delay compulsary beacuase raise condition
			// will occure
			query.addCriteria(new Criteria().andOperator(Criteria.where("record_status").is(0)));

			// System.out.println("query###########" + query);

			for (HashMap hmap : mongoTemplate.find(query, HashMap.class,
					CommonConstants.PAYTM_PAYMENT_POSTBACK_COLLECTION)) {

				listOrderIds.add(hmap.get("ORDERID").toString());
			}

			if (listOrderIds.size() > 0) {
				// updating list of orderids fetched
				query = new Query();
				query.addCriteria(Criteria.where("ORDERID").in(listOrderIds));

				update = new Update();
				update.set("record_status", 1);

				updateResult = mongoTemplate.updateMulti(query, update,
						CommonConstants.PAYTM_PAYMENT_POSTBACK_COLLECTION);
				if (updateResult.getMatchedCount() > 0) {
					// get the payment masters

					query = new Query();
					query.addCriteria(new Criteria().andOperator(
							Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_PAYTM),
							Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("_id").in(listOrderIds)));
					return mongoTemplate.find(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ArrayList<PaymentMaster>();
	}

	@Override
	public String getUserChoosenPaymentMode(Map<String, Object> mapResponse) {
		try {
			if (mapResponse != null) {
				if (mapResponse.containsKey("PAYMENTMODE")) {
					return mapResponse.get("PAYMENTMODE").toString().trim();
				}
			}

		} catch (Exception e) {
			logs.error("Exception occured in PaytmPaymentDaoImpl(getUserChoosenPaymentMode)", e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public void checkPaymentFraudStatusCodeAndTrackInfo(long userId, Map<String, Object> mapResponse,
			List<String> fraudErrorCodes) {
		String errorCode;
		try {

			if (mapResponse != null && mapResponse.containsKey("RESPCODE")) {
				errorCode = mapResponse.get("RESPCODE").toString();
				for (String fraudCode : fraudErrorCodes) {
					if (fraudCode.equalsIgnoreCase(errorCode)) {
						trackFraudPaymentCount(userId);
						trackTotalFraudPaymentCount(userId);
					}
				}
			}
		} catch (Exception e) {
			logs.error("Exception occured in PaytmPaymentDaoImpl(checkPaymentFraudStatusCodeAndTrackInfo)",
					e.getMessage());
			e.printStackTrace();
		}
	}

	void trackFraudPaymentCount(long userId) {
		SimpleDateFormat sdf;
		try {
			sdf = new SimpleDateFormat("yyyy-MM-dd");
			Query query = new Query(Criteria.where("userId").is(userId).and("added_date").is(sdf.format(new Date())));
			Update update = new Update();
			update.inc("error_codes_count", 1);
			mongoTemplate.upsert(query, update, CommonConstants.PAYMENT_FRAUD_DAY_COUNT_COLLECTION);
		} catch (Exception e) {
			logs.error("Exception trackFraudPaymentCount: " + e.getMessage());
		}
	}

	void trackTotalFraudPaymentCount(long userId) {
		SimpleDateFormat sdf;
		try {
			sdf = new SimpleDateFormat(CommonConstants.DATE_TIME_FORMAT);
			Query query = new Query(Criteria.where("userId").is(userId));
			Update update = new Update();
			update.inc("error_codes_count", 1);
			update.set("updated", sdf.format(new Date()));
			mongoTemplate.upsert(query, update, CommonConstants.PAYMENT_TOTAL_FRAUD_COUNT_COLLECTION);
		} catch (Exception e) {
			logs.error("Exception trackTotalFraudPaymentCount: " + e.getMessage());
		}
	}

}
