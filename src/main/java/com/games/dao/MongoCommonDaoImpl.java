package com.games.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.games.model.NotificationMaster;
import com.games.model.RawNotification;

@Repository
public class MongoCommonDaoImpl implements MongoCommonDao {
	
	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;
	
	@Autowired
	UsersDao userDao;
	
	
	
	@Override
	public int saveNotificationData(RawNotification rawNotification, String mongo_collection) {
		NotificationMaster notificationMaster;
		Map<String, Object> mapDataTag;
		List<String> listFcmTokens;
		String fcmToken;
		try {

			mapDataTag = new HashMap<>();
			mapDataTag.put("landing", rawNotification.getLanding());
			mapDataTag.put("icon", rawNotification.getIcon());
			mapDataTag.put("title", rawNotification.getTitle());
			mapDataTag.put("body", rawNotification.getBody());
			mapDataTag.put("n_type", rawNotification.getN_type() + "");
			mapDataTag.put("tid", rawNotification.getTid() + "");
			mapDataTag.put("game_id", rawNotification.getGame_id() + "");

			fcmToken = userDao.getUserFcmToken(rawNotification.getUserId());

			if (fcmToken != null && !fcmToken.equals("")) {
				listFcmTokens = new ArrayList<>();
				listFcmTokens.add(fcmToken);

				notificationMaster = new NotificationMaster();
				notificationMaster.setFcm_tokens(listFcmTokens);
				notificationMaster.setData_tag(mapDataTag);
				notificationMaster.setTime_to_live(rawNotification.getTtl());
				mongoTemplate.insert(notificationMaster, mongo_collection);
			}

			return 1;
		} catch (Exception e) {
			e.printStackTrace(); 
		}

		return 0;
	}


}
