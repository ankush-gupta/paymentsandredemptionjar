package com.games.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.games.dao.procedures.UpdateCashWalletAndTrackTransactionsSp;
import com.games.model.UpdateWalletCashMaster;
import com.games.util.CommonConstants;
import com.games.util.QueryConstants;

@Repository

public class WalletTransactionDaoImpl implements WalletTransactionDao {
	private static final Logger LOGS = LoggerFactory.getLogger(WalletTransactionDaoImpl.class);

	@Autowired
	@Qualifier("primaryDB")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Override
	public boolean updateCashWallet(UpdateWalletCashMaster updateWalletCashMaster) {
		return (new UpdateCashWalletAndTrackTransactionsSp(jdbcTemplate).executeProcedure(updateWalletCashMaster)) > 0;

	}

	@Override
	public boolean decreaseGameRedeemedAmount(int userId, int gameRedeemAmount) {
		try {
			jdbcTemplate.update("update cash_wallet set game_redeemed=game_redeemed-? where user_id=? ",
					new Object[] { gameRedeemAmount, userId });
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean refundProWalletAmount(long userId, int proAmount) {
		jdbcTemplate.update(
				"update pro_users_wallet set pro_earned=pro_earned+?,pro_redeemed=pro_redeemed-?,modified=now() where user_id=?",
				new Object[] { proAmount, proAmount, userId });

		return true;
	}

	@Override
	public boolean refundSportsWalletAmount(long userId, int proAmount) {
		jdbcTemplate.update(
				"update sports_wallet set game_earned=game_earned+?,game_redeemed=game_redeemed-?,modified=now() where user_id=?",
				new Object[] { proAmount, proAmount, userId });

		return true;
	}

	@Override
	public int getTotalPaymentsAmount(long user_id) {
		return jdbcTemplate.query(QueryConstants.GET_TOTAL_PAYMENT_AMOUNT, new Object[] { user_id },
				new ResultSetExtractor<Integer>() {
					@Override
					public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getInt("paytm_deposit") + rs.getInt("paytm_redeemed");
						}
						return 0;
					}
				});
	}

	@Override
	public float getUserBalanceProWalletEarnings(long user_id) {
		return jdbcTemplate.query(QueryConstants.USER_PRO_BALANCE_EARNINGS, new Object[] { user_id },
				new ResultSetExtractor<Float>() {
					@Override
					public Float extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getFloat("pro_earned");
						}

						return 0f;
					}
				});
	}

	@Override
	public float getUserBalanceSportsWalletEarnings(long user_id) {
		return jdbcTemplate.query(QueryConstants.USER_SPORTS_BALANCE_EARNINGS, new Object[] { user_id },
				new ResultSetExtractor<Float>() {
					@Override
					public Float extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getFloat("game_earned");
						}

						return 0f;
					}
				});
	}

	@Override
	public float getUserBalanceGameEarnings(long user_id) {
		return jdbcTemplate.query(QueryConstants.USER_GAME_BALANCE_EARNINGS, new Object[] { user_id },
				new ResultSetExtractor<Float>() {
					@Override
					public Float extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getFloat("game_earned");
						}

						return 0f;
					}
				});
	}

	@Override
	public void updateUserBalanceTrackMaster(long user_id, String referenceNo, String walletType, int status) {
		Query query;
		double post_redeem_balance = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();
		try {
			query = new Query(Criteria.where("referenceNo").is(referenceNo));

			switch (walletType) {
			case "GAME":
				post_redeem_balance = getUserBalanceGameEarnings(user_id);
				break;
			case "PRO":
				post_redeem_balance = getUserBalanceProWalletEarnings(user_id);
				break;
			case "SPORTS":
				post_redeem_balance = getUserBalanceSportsWalletEarnings(user_id);
				break;

			}

			Update update;

			update = new Update();
			update.set("modifiedGroupDate", sdf.format(currentDate));
			update.set("modifiedDate", currentDate);
			update.set("post_redeem_balance", post_redeem_balance);
			update.set("transferStatus", status);

			mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_USER_BALANCE_TRACK_MASTER);

		} catch (Exception e) {
			e.printStackTrace();
			LOGS.error("Exception in updateUserBalanceTrackMaster::::" + e.getMessage());
		}
	}

}
