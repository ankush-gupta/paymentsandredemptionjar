package com.games.dao;

public interface SingularDao {

	public void updateSingularEventsStatus(long userId,String eventName,int amount);

}
