package com.games.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.games.util.CommonConstants;

@Repository
public class HotDealsDaoImpl implements HotDealsDao {

	private static final Logger LOGS = LoggerFactory.getLogger(HotDealsDaoImpl.class);

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Override
	public void removeUserHotDeals(long userId) {
		Query query;
		try {
			query = new Query(Criteria.where("userId").is(userId));
			mongoTemplate.remove(query, CommonConstants.USER_HOT_DETAILS_COLLECTION);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
