package com.games.dao.procedures;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.games.model.UpdateWalletCashMaster;
import com.games.util.ProcedureConstants;

public class UpdateCashWalletAndTrackTransactionsSp extends StoredProcedure {

	public UpdateCashWalletAndTrackTransactionsSp(JdbcTemplate jdbcTemplate) {

		super(jdbcTemplate, ProcedureConstants.UPDATE_CASH_WALLET_AND_TRACK_TRANSACTIONS_SP);

		declareParameter(new SqlParameter(ProcedureConstants.USER_ID, Types.INTEGER));
		declareParameter(new SqlParameter(ProcedureConstants.REFERENCE_NO, Types.VARCHAR));
		declareParameter(new SqlParameter(ProcedureConstants.GAME_AMOUNT, Types.INTEGER));
		declareParameter(new SqlParameter(ProcedureConstants.REWARD_AMOUNT, Types.INTEGER));

		declareParameter(new SqlParameter(ProcedureConstants.PAYTM_AMOUNT, Types.INTEGER));
		declareParameter(new SqlParameter(ProcedureConstants.GAME_COINS, Types.INTEGER));
		declareParameter(new SqlParameter(ProcedureConstants.REWARD_COINS, Types.INTEGER));
		declareParameter(new SqlParameter(ProcedureConstants.TRANS_TYPE, Types.VARCHAR));

		declareParameter(new SqlOutParameter(ProcedureConstants.TRANS_RESULT, Types.INTEGER));

		// now compile stored proc
		compile();

	}

	public int executeProcedure(UpdateWalletCashMaster updateWalletCashMaster) {

		Map inParameters = new HashMap<>();
		inParameters.put(ProcedureConstants.USER_ID, new Integer((int) updateWalletCashMaster.getUserId()));
		inParameters.put(ProcedureConstants.REFERENCE_NO, updateWalletCashMaster.getReferenceNo());
		inParameters.put(ProcedureConstants.GAME_AMOUNT, new Integer(updateWalletCashMaster.getGameAmount()));
		inParameters.put(ProcedureConstants.REWARD_AMOUNT, new Integer(updateWalletCashMaster.getRewardAmount()));

		inParameters.put(ProcedureConstants.PAYTM_AMOUNT, new Integer(updateWalletCashMaster.getPaytmAmount()));
		inParameters.put(ProcedureConstants.GAME_COINS, new Integer(updateWalletCashMaster.getGameCoins()));
		inParameters.put(ProcedureConstants.REWARD_COINS, new Integer(updateWalletCashMaster.getRewardCoins()));
		inParameters.put(ProcedureConstants.TRANS_TYPE, updateWalletCashMaster.gettType());

		// execting stored procedure
		Map result = execute(inParameters);

		if (result.containsKey("_result")) {
			return Integer.parseInt(result.get("_result").toString());
		}

		return 0;

	}

}
