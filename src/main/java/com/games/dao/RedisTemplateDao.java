package com.games.dao;

public interface RedisTemplateDao {

	int getCircularListValue(String key);

}
