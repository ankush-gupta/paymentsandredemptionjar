package com.games.dao;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.games.model.TransactionDto;
import com.games.util.AppCommonUtils;
import com.games.util.CommonConstants;
import com.mongodb.client.result.UpdateResult;

@Repository
public class TransactionsDaoImpl implements TransactionsDao {

	private static final Logger LOGS = LoggerFactory.getLogger(TransactionsDaoImpl.class);

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Override
	public long insertTransactions(TransactionDto dto, String mongoCollection) {
		try {
			dto.setAdded_date(AppCommonUtils.getCurrentDate());
			dto.setCreated(AppCommonUtils.getCurrentTime());
			Calendar calender = Calendar.getInstance();
			dto.setCreated_in_millis(calender.getTimeInMillis());
			mongoTemplate.insert(dto, mongoCollection);
			return dto.getUser_id();
		} catch (Exception e) {
			LOGS.error("Exception while saving transaction Data in collection: " + mongoCollection + " ::: "
					+ e.getMessage());
		}
		return 0;
	}

	/*
	 * @Override public long insertCashTransactions(TransactionDto dto) { // TODO
	 * Auto-generated method stub try { Calendar calender = Calendar.getInstance();
	 * dto.setAdded_date(AppCommonUtils.getCurrentDate());
	 * dto.setCreated(calender.getTime().toString()); mongoTemplate.insert(dto,
	 * CommonConstants.CASH_TRANSACTIONS); return dto.getUser_id(); } catch
	 * (Exception e) { LOGS.error("Exception while saving transaction Data: " +
	 * e.getMessage()); } return 0; }
	 */

	/*
	 * @Override public long insertTokenTransactions(TransactionDto dto) { // TODO
	 * Auto-generated method stub try { Calendar calender = Calendar.getInstance();
	 * dto.setAdded_date(AppCommonUtils.getCurrentDate());
	 * dto.setCreated(calender.getTime().toString()); mongoTemplate.insert(dto,
	 * CommonConstants.TOKEN_TRANSACTIONS); return dto.getUser_id(); } catch
	 * (Exception e) { LOGS.error("Exception while saving transaction Data: " +
	 * e.getMessage()); } return 0; }
	 */

	@Override
	public TransactionDto updateCashTransactions(TransactionDto transactionDto) {

		Update update;
		Calendar calender = Calendar.getInstance();

		Query query = new Query();
		query.addCriteria(Criteria.where("referenceNo").is(transactionDto.getReferenceNo()));

		if (transactionDto.getTransaction_subtitle() == null || transactionDto.getTransaction_subtitle().equals("")) {
			update = new Update().set("transaction_title", transactionDto.getTransaction_title())
					.set("added_date", transactionDto.getAdded_date())
					.set("created", calender.getTime().toString());
		} else {
			update = new Update().set("transaction_title", transactionDto.getTransaction_title())
					.set("transaction_subtitle", transactionDto.getTransaction_subtitle())
					.set("added_date", transactionDto.getAdded_date())
					.set("created", calender.getTime().toString());
		}

		UpdateResult updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.CASH_TRANSACTIONS);

		return transactionDto;
	}

}
