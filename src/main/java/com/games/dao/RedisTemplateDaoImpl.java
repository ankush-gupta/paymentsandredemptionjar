package com.games.dao;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RedisTemplateDaoImpl implements RedisTemplateDao {
	private static Logger logger = LoggerFactory.getLogger(RedisTemplateDaoImpl.class);
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> template;



	@Override
	public int getCircularListValue(String key) {
		String c_list_value;
		try {

			c_list_value = template.opsForList().rightPopAndLeftPush(key, key);

			if (c_list_value != null && !c_list_value.equals("")) {
				return Integer.parseInt(c_list_value);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in getCircularListValue::key:" + key);
		}
		return 0;
	}
}
