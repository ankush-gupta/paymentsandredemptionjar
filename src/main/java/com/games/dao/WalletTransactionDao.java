package com.games.dao;

import com.games.model.UpdateWalletCashMaster;

public interface WalletTransactionDao {

	public boolean updateCashWallet(UpdateWalletCashMaster updateWalletCashMaster);

	public boolean refundProWalletAmount(long userId, int proAmount);

	public boolean refundSportsWalletAmount(long userId, int proAmount);

	public boolean decreaseGameRedeemedAmount(int userId, int gameRedeemAmount);

	public int getTotalPaymentsAmount(long user_id);

	public float getUserBalanceProWalletEarnings(long user_id);

	public float getUserBalanceGameEarnings(long user_id);

	public float getUserBalanceSportsWalletEarnings(long user_id);

	public void updateUserBalanceTrackMaster(long user_id, String referenceNo, String walletType, int status);

}
