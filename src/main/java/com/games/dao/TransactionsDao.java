package com.games.dao;



import com.games.model.TransactionDto;


public interface TransactionsDao {
	
	TransactionDto updateCashTransactions(TransactionDto transactionDto);
	
	public long insertTransactions(TransactionDto dto, String mongoCollection) ;
	
	
}
