package com.games.dao;

import com.games.model.NotificationMaster;



public interface NotifiationDao {
	
	public NotificationMaster getNotificationMasterByCollection(String collectionName);
	
	public boolean updateNotificationRecord(String id,String collectionName);
	
	public void deleteNotificationRecord(String id,String collectionName); 
	

}
