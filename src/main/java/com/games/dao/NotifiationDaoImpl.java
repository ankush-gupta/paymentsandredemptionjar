package com.games.dao;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.games.model.NotificationMaster;
import com.mongodb.client.result.UpdateResult;



@Repository
public class NotifiationDaoImpl implements NotifiationDao {
	private static final Logger LOGS = LoggerFactory.getLogger(NotifiationDaoImpl.class);
	
	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;
	
	@Override
	public NotificationMaster getNotificationMasterByCollection(String collectionName) {
		Query query;
		//query=new Query(Criteria.where("status").exists(false).orOperator(Criteria.where("status").is("Pending").orOperator(Criteria.where("status").is(""))));
		
		
		query=new Query();
		
		query.addCriteria(
				new Criteria().orOperator(
						Criteria.where("status").exists(false),
				Criteria.where("status").exists(true).orOperator(Criteria.where("status").in(new Object[] {"","Pending"}) ) 
				)
				); 
		
		NotificationMaster notificationMaster= mongoTemplate.findOne(query, NotificationMaster.class, collectionName);
		if(notificationMaster!=null) {
			if(updateNotificationRecord(notificationMaster.getId(), collectionName)) {
				return notificationMaster;
			}
			
		}
		
		return null;
	}

	@Override
	public boolean updateNotificationRecord(String id, String collectionName) {
		Query query;
		Update update;
		UpdateResult updateResult;
		
		query = new Query(Criteria.where("_id").is(id)); 
		update = new Update().set("status", "Processing");
		updateResult = mongoTemplate.updateFirst(query, update,collectionName);
		if(updateResult.getMatchedCount()>0) {
			return true;
		}
		
		return false;
	}

	@Override
	public void deleteNotificationRecord(String id, String collectionName) {
		Query query;
		query = new Query(Criteria.where("_id").is(id)); 
		mongoTemplate.remove(query, collectionName);
	}
	
	
	
	
	


}
