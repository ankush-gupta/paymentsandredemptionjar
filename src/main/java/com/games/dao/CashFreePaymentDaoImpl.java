package com.games.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games.model.PaymentMaster;
import com.games.properties.PaymentGateWayProperties;
import com.games.util.CommonConstants;
import com.mongodb.client.result.UpdateResult;

@Repository
public class CashFreePaymentDaoImpl implements PaymentDao {
	private static final Logger logs = LoggerFactory.getLogger(CashFreePaymentDaoImpl.class);

	@Autowired
	PaymentGateWayProperties paymentGateWayProperties;

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Autowired
	@Qualifier("trackRestTemplate")
	RestTemplate trackRestTemplate;

	@Override
	public PaymentMaster getPendingPayments() {

		int count;
		Query query;
		List<PaymentMaster> listMaster;
		Random random = new Random();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());

		try {

			query = new Query();
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_CASHFREE),
							Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));

			count = (int) mongoTemplate.count(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);
			if (count > 0) {

				query = new Query();
				query.addCriteria(new Criteria().andOperator(
						Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_CASHFREE),
						Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
						Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));
				query.limit(1);
				query.skip(new Long(random.nextInt(count)));

				listMaster = mongoTemplate.find(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);

				if (listMaster.size() > 0) {
					return listMaster.get(0);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public Map<String, Object> checkPaymentStaus(String orderId) {

		logs.info("CashFreePayment status api.... paymentId::" + orderId);

		String response;
		Map<String, Object> map;
		HttpEntity<MultiValueMap<String, String>> requestEntity;
		HttpHeaders headers = new HttpHeaders();
		ObjectMapper mapper = new ObjectMapper();

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("appId", paymentGateWayProperties.getCashFreeAppId());
		params.add("secretKey", paymentGateWayProperties.getCashFreeSecretKey());
		params.add("orderId", orderId);

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		requestEntity = new HttpEntity<MultiValueMap<String, String>>(params, headers);

		logs.info("url:: " + paymentGateWayProperties.getPaytmApiBaseUrl()
				+ paymentGateWayProperties.getPaytmApiOrderStatus());
		logs.info("body:: " + headers);
		logs.info("header:: " + requestEntity);

		response = trackRestTemplate.postForObject(
				paymentGateWayProperties.getCashFreeApiBaseUrl() + paymentGateWayProperties.getCashFreeApiOrderStatus(),
				requestEntity, String.class);

		try {
			logs.info("response: " + response);

			return mapper.readValue(response, new TypeReference<Map<String, Object>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int updatePaymentRecord(PaymentMaster paymentMaster, Map<String, Object> mapResponse) {

		logs.info("CashFreePayment updating payment record.... paymentId::" + paymentMaster.getId());

		Query query = new Query(Criteria.where("_id").is(paymentMaster.getId()));
		Update update;
		UpdateResult updateResult;

		Calendar calendar = Calendar.getInstance();

		update = new Update();
		update.set("modifiedDate", calendar.getTime());

		if (mapResponse != null && mapResponse.get("status") != null) {

			switch (mapResponse.get("status").toString()) {

			case "OK":

				if (mapResponse.get("orderStatus") != null) {
					switch (mapResponse.get("orderStatus").toString()) {

					case "PAID":
						update.set("paymentStatus", CommonConstants.TXN_SUCCESS_STATUS).set("response", mapResponse);
						updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
						// add amount to his wallet
						if (updateResult.getMatchedCount() > 0) {
							return CommonConstants.TXN_SUCCESS_STATUS;

						}

						break;

					case "ACTIVE":

						if (mapResponse.get("txStatus") != null) {

							if (mapResponse.get("txStatus").toString().equals("FAILED")) {

								update.set("paymentStatus", CommonConstants.TXN_FAILURE_STATUS).set("response",
										mapResponse);
								updateResult = mongoTemplate.updateFirst(query, update,
										CommonConstants.PAYMENT_COLLECTION);
								if (updateResult.getMatchedCount() > 0) {
									return CommonConstants.TXN_FAILURE_STATUS;

								}

							} else if (mapResponse.get("txStatus").toString().equals("PENDING")) {

								// only for pending transactions
								long diffMillsec = paymentMaster.getModifiedDate().getTime()
										- paymentMaster.getCreatedDate().getTime();
								long min = diffMillsec / (60 * 1000);
								if (min >= 60) {
									update.set("paymentStatus", CommonConstants.TXN_FAILURE_STATUS).set("response",
											mapResponse);
									updateResult = mongoTemplate.updateFirst(query, update,
											CommonConstants.PAYMENT_COLLECTION);
									if (updateResult.getMatchedCount() > 0) {
										return CommonConstants.TXN_FAILURE_STATUS;
									}

								} else {
									update.set("response", mapResponse);
									mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
								}

							} else {
								// in process , wait for success or failure
								update.set("response", mapResponse);

								mongoTemplate.updateFirst(query, update, PaymentMaster.class,
										CommonConstants.PAYMENT_COLLECTION);
							}

						} else {
							update.set("paymentStatus", CommonConstants.TXN_FAILURE_STATUS).set("response",
									mapResponse);
							updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
							if (updateResult.getMatchedCount() > 0) {
								return CommonConstants.TXN_FAILURE_STATUS;

							}
						}

						break;

					case "PROCESSED":
						// in process , wait for success or failure
						update.set("response", mapResponse);

						mongoTemplate.updateFirst(query, update, PaymentMaster.class,
								CommonConstants.PAYMENT_COLLECTION);

						break;

					default:
						System.out.println("Error not configured cashfreee:: " + mapResponse);
						break;

					}
				}

				break;
			case "ERROR":
				if (mapResponse.get("reason").toString().equalsIgnoreCase("Order Id does not exist")) {
					update.set("paymentStatus", CommonConstants.TXN_FAILURE_STATUS).set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.PAYMENT_COLLECTION);
					if (updateResult.getMatchedCount() > 0) {
						return CommonConstants.TXN_FAILURE_STATUS;

					}
				} else {
					System.out.println("Error not configured cashfreee:: " + mapResponse);
				}
				break;
			default:
				System.out.println("Error not configured cashfreee:: " + mapResponse);
				break;
			}

		}
		return CommonConstants.TXN_PENDING_STATUS;
	}
	/*
	 * @Override public List<PaymentMaster> getListPendingPayments() { int count;
	 * Query query; List<PaymentMaster> listMaster; Random random = new Random();
	 * Calendar calendar = Calendar.getInstance(); calendar.add(Calendar.MINUTE,
	 * -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());
	 * 
	 * try {
	 * 
	 * query = new Query(); query.addCriteria( new
	 * Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.
	 * PAYMENT_TYPE_CASHFREE),
	 * Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
	 * Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));
	 * 
	 * count = (int) mongoTemplate.count(query, PaymentMaster.class,
	 * CommonConstants.PAYMENT_COLLECTION); if (count > 0) {
	 * 
	 * query = new Query(); query.addCriteria(new Criteria().andOperator(
	 * Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_CASHFREE),
	 * Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
	 * Criteria.where("crtLongDate").lte(calendar.getTimeInMillis())));
	 * query.limit(CommonConstants.TRANSACTION_HIT_COUNT); query.skip(new
	 * Long(random.nextInt(count)));
	 * 
	 * listMaster = mongoTemplate.find(query, PaymentMaster.class,
	 * CommonConstants.PAYMENT_COLLECTION);
	 * 
	 * return listMaster; }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * return new ArrayList<>();
	 * 
	 * }
	 */

	@Override
	public List<PaymentMaster> getListPendingPayments(boolean statusReVerfication,String date_type) {
		Query query;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());

		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_CASHFREE),
						Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
						Criteria.where("crtLongDate").lte(calendar.getTimeInMillis()),
						Criteria.where("response").exists(statusReVerfication)));

		// query.limit(CommonConstants.TRANSACTION_HIT_COUNT);

		return mongoTemplate.find(query, PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION);

	}

	@Override
	public List<PaymentMaster> getWebHookPayments() {
		/*
		 * Query query; List<String> listOrderIds;
		 * 
		 * Update update; UpdateResult updateResult; int responseCode;
		 * 
		 * Calendar calendar = Calendar.getInstance(); update = new Update();
		 * update.set("modifiedDate", calendar.getTime());
		 * 
		 * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0");
		 * 
		 * try {
		 * 
		 * calendar.add(Calendar.MINUTE,
		 * -paymentGateWayProperties.getPaymentStatusCheckMinutusDelay());
		 * 
		 * listOrderIds = new ArrayList<>();
		 * 
		 * query = new Query();
		 * 
		 * //need to add this condition 6 mint delay compulsary beacuase raise condition
		 * will occure query.addCriteria(new
		 * Criteria().andOperator(Criteria.where("record_status").is(0),
		 * Criteria.where("TXNDATETIME").lt(sdf.format(calendar.getTime()))));
		 * 
		 * for (HashMap hmap : mongoTemplate.find(query, HashMap.class,
		 * CommonConstants.CASHFREE_PAYMENT_POSTBACK_COLLECTION)) {
		 * 
		 * listOrderIds.add(hmap.get("orderId").toString()); }
		 * 
		 * if (listOrderIds.size() > 0) { // updating list of orderids fetched query =
		 * new Query(); query.addCriteria(Criteria.where("orderId").in(listOrderIds));
		 * 
		 * update = new Update(); update.set("record_status", 1);
		 * 
		 * updateResult = mongoTemplate.updateMulti(query, update,
		 * CommonConstants.CASHFREE_PAYMENT_POSTBACK_COLLECTION); if
		 * (updateResult.getMatchedCount() > 0) { // get the payment masters
		 * 
		 * query = new Query(); query.addCriteria(new Criteria().andOperator(
		 * Criteria.where("paymentType").is(CommonConstants.PAYMENT_TYPE_CASHFREE),
		 * Criteria.where("paymentStatus").is(CommonConstants.TXN_PENDING_STATUS),
		 * Criteria.where("_id").in(listOrderIds))); return mongoTemplate.find(query,
		 * PaymentMaster.class, CommonConstants.PAYMENT_COLLECTION); }
		 * 
		 * }
		 * 
		 * } catch (Exception e) { e.printStackTrace(); }
		 */

		return new ArrayList<PaymentMaster>();
	}
	
	@Override
	public String getUserChoosenPaymentMode(Map<String, Object> mapResponse) {
		try {

			if (mapResponse != null) {
				if (mapResponse.containsKey("paymentMode")) {
					return mapResponse.get("paymentMode").toString().trim();
				}
			}

		} catch (Exception e) {
			logs.error("Exception occured in CashFreePaymentDaoImpl(getUserChoosenPaymentMode)", e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

}
