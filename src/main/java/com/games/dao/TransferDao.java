package com.games.dao;

import java.util.List;
import java.util.Map;

import com.games.model.TransferMaster;

public interface TransferDao {

	public void transferAmount(TransferMaster transferMaster);

	public int updateTransferRecord(TransferMaster transferMaster, Map<String, Object> mapResponse);

	public TransferMaster getInitRecords();

	public TransferMaster getPendingRecords();

	public Map<String, Object> getTransferStaus(TransferMaster transferMaster);

	public List<TransferMaster> getListInitRecords();

	public List<TransferMaster> getListPendingRecords();

	default public TransferMaster getKycPendingRecords() {
		return null;
	}

	default public List<TransferMaster> getListKycPendingRecords() {
		return null;
	}

}
