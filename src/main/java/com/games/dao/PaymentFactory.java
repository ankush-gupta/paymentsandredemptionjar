package com.games.dao;

public interface PaymentFactory {
	public PaymentDao paymentObjectByType(String paymentType);
}
