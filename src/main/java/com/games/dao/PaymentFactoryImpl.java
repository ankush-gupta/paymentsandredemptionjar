package com.games.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.games.util.CommonConstants;

@Component
public class PaymentFactoryImpl implements PaymentFactory {

	@Autowired
	@Qualifier(value = "paytmPaymentDaoImpl")
	PaymentDao paytmPaymentDaoImpl;

	@Autowired
	@Qualifier(value = "cashFreePaymentDaoImpl")
	PaymentDao cashFreePaymentDaoImpl;

	@Override
	public PaymentDao paymentObjectByType(String paymentType) {
		switch (paymentType) {

		case CommonConstants.PAYMENT_TYPE_CASHFREE:
			return cashFreePaymentDaoImpl;

		case CommonConstants.PAYMENT_TYPE_PAYTM:
			return paytmPaymentDaoImpl;

		}
		return null;
	}

}
