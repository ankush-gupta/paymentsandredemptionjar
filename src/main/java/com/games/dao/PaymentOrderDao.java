package com.games.dao;

import com.games.model.PaymentMaster;
import com.games.model.WalletCashOfferMaster;

public interface PaymentOrderDao {

	public void updatePaymentOrder(String _id, int depositWalletcashAmount, String dealName, int dealAmount,
			int dealCoins);

	public void updatePaymentOrderWithRewardModeDetails(PaymentMaster paymentMaster);

	public boolean checkUserDealPaymentSuccess(String _id, long userId, String dealName);

	public int getUserSuccPaymentOrderCount(long user_id);

	public PaymentMaster getPaymentMasterByOrderId(String orderId);

	public boolean getTransactionProcessingState(String orderId);

	public WalletCashOfferMaster getWalletCashBackOfferByAmountAndMode(int planAmount, String userModeKeyWord);
	
	public boolean checkUserGotCashBackAmount_N_Previous_days(long userId, int dayInterval);
	
	public void maintainUserTotalAddMoney(long userId, int planAmount);

}
