package com.games.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games.model.TransferMaster;
import com.games.properties.ApplicationProperties;
import com.games.properties.TransferAmountProperties;
import com.games.util.CommonConstants;
import com.games.util.TransferPaytmUtils;
import com.mongodb.client.result.UpdateResult;

@Repository
public class PaytmTransferDaoImpl implements TransferDao {

	private static final Logger logs = LoggerFactory.getLogger(PaytmTransferDaoImpl.class);

	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	TransferAmountProperties transferAmountProperties;

	@Autowired
	@Qualifier("mongoTemplatePrimary")
	private MongoTemplate mongoTemplate;

	@Override
	public TransferMaster getInitRecords() {

		Query query;
		List<TransferMaster> listTransferMaster;

		query = new Query();

		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
						Criteria.where("transferStatus").is(CommonConstants.TXN_INIT_STATUS)));

		query.limit(1);

		listTransferMaster = mongoTemplate.find(query, TransferMaster.class);
		if (listTransferMaster.size() > 0) {
			return listTransferMaster.get(0);
		}

		return null;
	}

	@Override
	public TransferMaster getPendingRecords() {

		// logs.info("Paytm status check records");
		int count;
		Query query;
		List<TransferMaster> listTransferMaster;

		Random random = new Random();

		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
						Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
						Criteria.where("response").exists(false)));

		// logs.info("count query :: " + query);
		count = (int) mongoTemplate.count(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

		if (count > 0) {

			query = new Query();
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
							Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("response").exists(false)));
			query.limit(1);
			query.skip(new Long(random.nextInt(count)));

			// logs.info("actual query :: " + query);
			listTransferMaster = mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);
			// logs.info("records count :: " + listTransferMaster.size());
			if (listTransferMaster.size() > 0) {
				return listTransferMaster.get(0);
			}
		}

		return null;

	}

	@Override
	public void transferAmount(TransferMaster transferMaster) {

		Query query;
		Update update;
		UpdateResult updateResult;
		TransferPaytmUtils transferPaytmUtils;

		transferPaytmUtils = new TransferPaytmUtils(transferAmountProperties.getTransferPaytmMerchantKey(),
				transferAmountProperties.getTransferPaytmMerchantGuid(),
				transferAmountProperties.getTransferPaytmSalesWalletGuid(), "137.59.203.108",
				transferAmountProperties.getTransferPaytmBaseUrl()
						+ transferAmountProperties.getTransferPaytmSalesToUserCreditUri(),
				transferAmountProperties.getTransferPaytmBaseUrl()
						+ transferAmountProperties.getTransferPaytmCheckStatusUri());

		transferPaytmUtils.paytmTransferRequest(transferMaster.getId(), transferMaster.getMobileNo() + "",
				transferMaster.getTransferAmount());

		// updating the record first and send hit to the gateway
		query = new Query(Criteria.where("_id").is(transferMaster.getId()));
		update = new Update().set("transferStatus", CommonConstants.TXN_PENDING_STATUS);
		updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);

	}

	@Override
	public int updateTransferRecord(TransferMaster transferMaster, Map<String, Object> mapResponse) {
		Query query = new Query(Criteria.where("_id").is(transferMaster.getId()));
		Update update;
		UpdateResult updateResult;

		Calendar calender;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		calender = Calendar.getInstance();

		update = new Update();
		update.set("modifiedDate", calender.getTime());
		update.set("modifiedGroupDate", sdf.format(calender.getTime()));

		if (mapResponse != null && mapResponse.get("status") != null) {

			switch (mapResponse.get("status").toString()) {

			case "SUCCESS":
				switch (mapResponse.get("statusCode").toString()) {
				case "STUC_1000":
				case "STUC_1002":
				case "STUC_1003":
				case "STUC_1004":
				case "STUC_1005":
				case "STUC_1006":
				case "STUC_1008":
					// failure transactins , refund money
					update.set("transferStatus", CommonConstants.TXN_FAILURE_STATUS).set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);
					// revert money to his wallet

					return CommonConstants.TXN_FAILURE_STATUS;

				case "STUC_1009":
					// change status to init because this transaction not done
					update.set("transferStatus", CommonConstants.TXN_INIT_STATUS).set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);

					break;
				case "STUC_1007":
					// this condition will not generate, becuase we are checking the status only
					// already done transaction changed to intrasfer

					break;

				case "SS_001":
					// success
					if (mapResponse.get("response") != null) {

						Map<String, Object> txt = (Map<String, Object>) ((List<Object>) ((Map<String, Object>) mapResponse
								.get("response")).get("txnList")).get(0);
						switch (txt.get("status").toString()) {

						// init
						case "0":
							// do nothing just save response
							update.set("response", mapResponse);
							updateResult = mongoTemplate.updateFirst(query, update,
									CommonConstants.TRANSFER_COLLECTION);
							break;

						// success
						case "1":
							update.set("transferStatus", CommonConstants.TXN_SUCCESS_STATUS).set("response",
									mapResponse);
							updateResult = mongoTemplate.updateFirst(query, update,
									CommonConstants.TRANSFER_COLLECTION);
							if (updateResult.getMatchedCount() > 0) {
								return CommonConstants.TXN_SUCCESS_STATUS;
							}
							break;
						// failed
						case "2":
							update.set("transferStatus", CommonConstants.TXN_FAILURE_STATUS).set("response",
									mapResponse);
							updateResult = mongoTemplate.updateFirst(query, update,
									CommonConstants.TRANSFER_COLLECTION);
							if (updateResult.getMatchedCount() > 0) {
								return CommonConstants.TXN_FAILURE_STATUS;
							}

							break;
						// pending--in transfer
						case "3":
							update.set("response", mapResponse);
							updateResult = mongoTemplate.updateFirst(query, update,
									CommonConstants.TRANSFER_COLLECTION);

							break;

						}
					}

					break;
				default:
					System.out.println("status code not defined in paytmTransferdaoImple::");
					break;

				}

				break;

			case "FAIL":
				System.out.println("Paytm api faield::");
				break;

			case "FAILURE":
				switch (mapResponse.get("statusCode").toString()) {
				case "GE_1009":
					// failure transactins , refund money
					update.set("transferStatus", CommonConstants.TXN_FAILURE_STATUS).set("response", mapResponse);
					updateResult = mongoTemplate.updateFirst(query, update, CommonConstants.TRANSFER_COLLECTION);
					// revert money to his wallet
					if (updateResult.getMatchedCount() > 0) {
						return CommonConstants.TXN_FAILURE_STATUS;
					}
					break;

				}

				break;

			}

		}

		return CommonConstants.TXN_PENDING_STATUS;
	}

	@Override
	public Map<String, Object> getTransferStaus(TransferMaster transferMaster) {

		// logs.info("paytm status check ::");

		String response;
		TransferPaytmUtils transferPaytmUtils;

		try {

			transferPaytmUtils = new TransferPaytmUtils(transferAmountProperties.getTransferPaytmMerchantKey(),
					transferAmountProperties.getTransferPaytmMerchantGuid(),
					transferAmountProperties.getTransferPaytmSalesWalletGuid(), "103.248.83.248",
					transferAmountProperties.getTransferPaytmBaseUrl()
							+ transferAmountProperties.getTransferPaytmSalesToUserCreditUri(),
					transferAmountProperties.getTransferPaytmBaseUrl()
							+ transferAmountProperties.getTransferPaytmCheckStatusUri());

			response = transferPaytmUtils.paytmTransactionStatus(transferMaster.getId());
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				return mapper.readValue(response, new TypeReference<Map<String, Object>>() {
				});
			}

			/*
			 * request = new TreeMap<>(); request.put("requestType", "merchanttxnid");
			 * request.put("txnType", "SALES_TO_USER_CREDIT"); request.put("txnId",
			 * transferMaster.getId()); request.put("merchantGuid",
			 * transferAmountProperties.getTransferPaytmMerchantGuid());
			 * 
			 * paytmObject = new TreeMap<>(); paytmObject.put("request", request);
			 * paytmObject.put("operationType", "CHECK_TXN_STATUS");
			 * paytmObject.put("platformName", "PayTM");
			 * 
			 * headers = new HttpHeaders();
			 * headers.setContentType(MediaType.APPLICATION_JSON); headers.add("MID",
			 * transferAmountProperties.getTransferPaytmMerchantKey());
			 * 
			 * requestJson = new JSONObject(paytmObject);
			 * 
			 * 
			 * // need to check paytm check values map stirng correct or not
			 * headers.add("Checksumhash", AppCommonUtils
			 * .getPaytmCheckSum(transferAmountProperties.getTransferPaytmMerchantKey(),
			 * requestJson.toString()));
			 * 
			 * logs.info("request body:: " + requestJson); logs.info("headers:: " +
			 * headers);
			 * 
			 * logs.info("url:: " + transferAmountProperties.getTransferPaytmBaseUrl() +
			 * transferAmountProperties.getTransferPaytmCheckStatusUri());
			 * 
			 * requestEntity = new HttpEntity<String>(requestJson.toString(), headers);
			 * 
			 * // requestEntity = new HttpEntity<String>("JsonData=" +
			 * requestJson.toString(), // headers);
			 * 
			 * response =
			 * restTemplate.postForObject(transferAmountProperties.getTransferPaytmBaseUrl()
			 * + transferAmountProperties.getTransferPaytmCheckStatusUri(), requestEntity,
			 * String.class);
			 * 
			 * logs.info("response:: " + response);
			 * 
			 * try {
			 * 
			 * ObjectMapper mapper = new ObjectMapper(); return mapper.readValue(response,
			 * new TypeReference<Map<String, Object>>() { });
			 * 
			 * } catch (Exception e) { e.printStackTrace(); }
			 * 
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public TransferMaster getKycPendingRecords() {

		logs.info("Paytm kyc pending records");
		int count;
		Query query;
		List<TransferMaster> listTransferMaster;

		Random random = new Random();

		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
						Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
						Criteria.where("response").exists(true)));

		// logs.info("count Paytm kyc :: " + query);
		count = (int) mongoTemplate.count(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

		if (count > 0) {

			query = new Query();
			query.addCriteria(
					new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
							Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
							Criteria.where("response").exists(true)));
			query.limit(1);
			query.skip(new Long(random.nextInt(count)));

			// logs.info("actual query :: " + query);
			listTransferMaster = mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);
			// logs.info("records count :: " + listTransferMaster.size());
			if (listTransferMaster.size() > 0) {
				return listTransferMaster.get(0);
			}
		}

		return null;

	}

	@Override
	public List<TransferMaster> getListInitRecords() {
		Query query;
		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
						Criteria.where("transferStatus").is(CommonConstants.TXN_INIT_STATUS)));
		// query.limit(CommonConstants.TRANSACTION_HIT_COUNT);
		return mongoTemplate.find(query, TransferMaster.class);

	}

	@Override
	public List<TransferMaster> getListPendingRecords() {
		Query query;
		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
						Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
						Criteria.where("response").exists(false)));
		// query.limit(CommonConstants.TRANSACTION_HIT_COUNT);

		return mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

	}
	/*
	 * @Override public List<TransferMaster> getListKycPendingRecords() { int count;
	 * Query query; List<TransferMaster> listTransferMaster;
	 * 
	 * Random random = new Random();
	 * 
	 * query = new Query(); query.addCriteria( new
	 * Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.
	 * TRANSFER_TYPE_PAYTM),
	 * Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
	 * Criteria.where("response").exists(true))); count = (int)
	 * mongoTemplate.count(query, TransferMaster.class,
	 * CommonConstants.TRANSFER_COLLECTION);
	 * 
	 * if (count > 0) {
	 * 
	 * query = new Query(); query.addCriteria( new
	 * Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.
	 * TRANSFER_TYPE_PAYTM),
	 * Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
	 * Criteria.where("response").exists(true)));
	 * query.limit(CommonConstants.TRANSACTION_HIT_COUNT); query.skip(new
	 * Long(random.nextInt(count))); listTransferMaster = mongoTemplate.find(query,
	 * TransferMaster.class, CommonConstants.TRANSFER_COLLECTION); return
	 * listTransferMaster; }
	 * 
	 * return new ArrayList<>();
	 * 
	 * }
	 */

	@Override
	public List<TransferMaster> getListKycPendingRecords() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Query query;
		query = new Query();
		query.addCriteria(
				new Criteria().andOperator(Criteria.where("transferType").is(CommonConstants.TRANSFER_TYPE_PAYTM),
						Criteria.where("transferStatus").is(CommonConstants.TXN_PENDING_STATUS),
						Criteria.where("modifiedGroupDate").ne(sdf.format(new Date())),
						Criteria.where("response").exists(true)));
		query.limit(CommonConstants.TRANSACTION_HIT_COUNT);
		return mongoTemplate.find(query, TransferMaster.class, CommonConstants.TRANSFER_COLLECTION);

	}

}
